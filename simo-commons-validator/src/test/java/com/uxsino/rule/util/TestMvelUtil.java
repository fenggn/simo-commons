package com.uxsino.rule.util;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.mvel2.MVEL;
import org.mvel2.ParserContext;
import org.mvel2.integration.VariableResolverFactory;
import org.mvel2.integration.impl.MapVariableResolverFactory;

public class TestMvelUtil {

    // list 中 name='test' 的个数
    // findSize(list,name=='test')

    // list 的size

    // list 中第三行数据 中的 value = 2
    @Test
    public void test() throws IOException {
        File scriptFile = new File("src/test/java/com/uxsino/rule/util/mvel.el");
        VariableResolverFactory resolverFactory = new MapVariableResolverFactory();
        MVEL.evalFile(scriptFile, ParserContext.create(), resolverFactory);
        resolverFactory.createVariable("x", 10);
        resolverFactory.createVariable("y", 20);
        Object result = MVEL.eval("add(add(x,y),10)>200", resolverFactory);
        System.out.println(result);

        Map<String, Object> env = new HashMap<String, Object>();
        env.put("a", 100.3);
        env.put("b", 45);
        env.put("c", -199.100);
        VariableResolverFactory factory = new MapVariableResolverFactory(env);

        // Integer i = (Integer) MVEL.eval("x * y", factory);
        Object result2 = MVEL.eval("a +b ", factory);
        System.out.println(result2);
    }

}
