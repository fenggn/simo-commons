package com.uxsino.rule.constant;

/**
 * hadnler 所需要的key的常量
 * 
 *
 */
public class Keys {

    public static final String KEY_ADD = "add";

    public static final String KEY_UPDATE = "update";

    public static final String KEY_DELETE = "drop";

    public static final String KEY_INIT = "reload";

    public static final String KEY_ID = "id";

}
