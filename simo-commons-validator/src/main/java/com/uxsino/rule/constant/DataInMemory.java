package com.uxsino.rule.constant;

import java.util.concurrent.ConcurrentHashMap;

import com.alibaba.fastjson.JSONObject;

/**
 * 存储在内存中的其他项目数据<p>
 * 用于规则校验
 * 
 *
 */
public class DataInMemory {

    // 资源关系数据
    public static ConcurrentHashMap<String, JSONObject> neMap = new ConcurrentHashMap<String, JSONObject>();

    public static ConcurrentHashMap<String, JSONObject> indicatorMap = new ConcurrentHashMap<String, JSONObject>();

    public static ConcurrentHashMap<Long, String> domainMap = new ConcurrentHashMap<Long, String>();

}
