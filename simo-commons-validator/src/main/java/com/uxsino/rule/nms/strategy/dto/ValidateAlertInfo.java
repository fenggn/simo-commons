package com.uxsino.rule.nms.strategy.dto;

import java.util.Map;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValidateAlertInfo {

    private Long eventId;
    
    private Long flap;

    // msg -> {user ids}
    private Map<String, Set<Long>> sendInfo;

    private String objectId;

    // 告警 / 恢复 alert / recover
    private String type;

    // 来源 {POLLING}
    private String source;

    private String alertBrief;

    private String alertCode;

    // 告警级别
    private int level;

    private ValidateMsg msg;
}
