package com.uxsino.rule.nms.handler;

import com.uxsino.rule.nms.model.NmsCaller;
import com.uxsino.rule.nms.model.NmsReport;

public class NmsHandler {

    public static NmsReport nmsValidate(NmsCaller caller) {
        // TODO 开始验证逻辑前请进行数据完整性校验
        BaseStrategy sh = null;
        switch (caller.getRule().getCompareType()) {
        case string:
            sh = new StringStrategyHandler(caller);
            break;
        case sum:
            sh = new SumStrategyHandler(caller);
            break;
        case exists:
            sh = new ExistStrategyHandler(caller);
            break;
        case history:
            if (caller.getHistoryValue() == null) {
                return null;
            }
            sh = new HistoryStrategyHandler(caller);
            break;
        case range:
            sh = new RangeStrategyHandler(caller);
            break;
        case composition:
            sh = new CompositionStrategyHandler(caller);
            break;
        case change:
            sh = new ListStrategyHandler(caller);
            break;
        default:
            break;
        }
        return sh.holisticVerifyProcess();
    }
}
