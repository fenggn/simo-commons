package com.uxsino.rule.nms.handler;

import com.alibaba.fastjson.JSONObject;
import com.uxsino.rule.model.NumberCompareWay;
import com.uxsino.rule.nms.model.NmsCaller;
import com.uxsino.rule.nms.validate.NumberValidator;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * 
 */
@NoArgsConstructor
public class RangeStrategyHandler extends BaseStrategy {

    private static Logger logger = LoggerFactory.getLogger(RangeStrategyHandler.class);

    public RangeStrategyHandler(NmsCaller caller) {
        this.callData = caller;
    }

    @Override
    public boolean ruleMatch(JSONObject setting, String... value) {
        boolean warning = false;
        Double currentNumber = null;
        Double start = null;
        Double end = null;
        try {
            currentNumber = Double.valueOf(value[0]);
            start = setting.getDouble("begin");
            end = setting.getDouble("end");
        } catch (NumberFormatException e) {
            logger.warn("range rule check error! {}-Its not a number!",currentNumber);
        }
        NumberValidator validator = new NumberValidator(NumberCompareWay.RANGE, currentNumber, start, end);
        warning = validator.validate();
        return warning;
    }

    @Override
    public String createAlertRange(JSONObject setting) {
        String range = "";

        BigDecimal begin = null;
        BigDecimal end = null;
        try {
            begin = setting.getBigDecimal("begin");
            end = setting.getBigDecimal("end");
        } catch (Exception e) {
            logger.error("createAlertRange 中获取 begin/end 失败 setting-->{};StackTrace-->{}", setting, e);
        }
        if (begin != null && end != null) {
            range = "[" + begin + "," + end + "]";
        } else if (begin != null) {
            range = "≥ " + begin;
        } else if (end != null) {
            range = "< " + end;
        } else {
            range = "无法识别的阈值";
        }
        return range;
    }
}