package com.uxsino.rule.nms.model;


import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JSONType(includes = {"ruleId","flap","times","compName","alertCode"})
public class RedisFlapInfo{

    @JsonProperty("ruleId")
    @JSONField(name = "ruleId")
    private Long ruleId;

    @JsonProperty("flap")
    @JSONField(name = "flap")
    private Long flap;

    @JsonProperty("times")
    @JSONField(name = "times")
    private Long times;

    private String compName;

    private String alertCode;
    
}
