package com.uxsino.rule.nms.model;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SubRuleReport {
    Boolean alertStatus;
    String subAlertContent;
}
