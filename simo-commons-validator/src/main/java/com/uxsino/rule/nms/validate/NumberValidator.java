package com.uxsino.rule.nms.validate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.uxsino.rule.model.NumberCompareWay;

import java.text.DecimalFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class NumberValidator extends BaseValidate {
    private DecimalFormat df = new DecimalFormat("0.##");

    private static Logger logger = LoggerFactory.getLogger(NumberValidator.class);

    private NumberCompareWay compareWay;

    private Double value;

    private Double target;

    private Double start;

    private Double end;

    public NumberValidator(NumberCompareWay compareWay, Double value, Double target, Double start, Double end) {
        this.compareWay = compareWay;
        this.value = value;
        this.target = target;
        this.start = start;
        this.end = end;
    }

    public NumberValidator(NumberCompareWay compareWay, Double value, Double target) {
        this.compareWay = compareWay;
        this.value = value;
        this.target = target;
        this.start = null;
        this.end = null;
    }

    public NumberValidator(NumberCompareWay compareWay, Double value, Double start, Double end) {
        this.compareWay = compareWay;
        this.value = value;
        this.target = null;
        this.start = start;
        this.end = end;
    }

    @Override
    public boolean validate() {
        if (compareWay == null) {
            logger.error("Number validate -> CompareWay is null!");
            return false;
        }
        if (value == null) {
            logger.error("Number validate -> Value is null!");
            return false;
        }
        switch (compareWay) {
        case EQUALS:
            if (target == null) {
                logger.error("Number validate -> Target is null!");
                return false;
            }
            return value.equals(target);
        case NOT_EQUALS:
            if (target == null) {
                logger.error("Number validate -> Target is null!");
                return false;
            }
            return !value.equals(target);
        case GREATER:
            if (target == null) {
                logger.error("Number validate -> Target is null!");
                return false;
            }
            return value > target;
        case LESS:
            if (target == null) {
                logger.error("Number validate -> Target is null!");
                return false;
            }
            return value < target;
        case GREAT_EREQUALS:
            if (target == null) {
                logger.error("Number validate -> Target is null!");
                return false;
            }
            return value >= target;
        case LESS_EQUALS:
            if (target == null) {
                logger.error("Number validate -> Target is null!");
                return false;
            }
            return value <= target;
        case RANGE:
            return range(value, start, end);
        default:
            break;
        }
        return false;
    }

    /**
     * 
     * @param value 待检查值
     * @param start 开始值
     * @param end 结束值
     * @return 待检查值是否在此区间内（闭区间）
     */
    private boolean range(Double value, Double start, Double end) {
        boolean result = false;
        if (start == null && end == null) {
            logger.error("Number validate -> Start and End is null!");
            return result;
        }
        result = true;
        if (start != null) {
            result = (value >= start);
        }
        if (result && end != null) {
            result = (value < end);
        }
        return result;
    }
}
