package com.uxsino.rule.nms.model;

import com.uxsino.rule.nms.strategy.dto.RuleDto;

import java.util.Map;
import lombok.Data;

@Data
public class AnalysisResult {
    private Map<String, RuleDto> subRuleMap;
    private String simpleFormula;
}
