package com.uxsino.rule.nms.model;

//import com.uxsino.commons.model.RunStatus;
import com.uxsino.rule.nms.strategy.dto.ValidateAlertInfo;
import java.util.List;
//import java.util.Map;
import lombok.Data;

/**
 * 资源监控策略（network monitoring strategy）规则校验结果实体
 */
@Data
public class NmsReport {
    
    Integer indStatus;
    
    //是否产生告警
    Boolean alertStatus;

    List<ValidateAlertInfo> alertList;

    //Map<String, Map<String, RunStatus>> vmWareRunInfo;
    
    
    
    
}
