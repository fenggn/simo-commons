package com.uxsino.rule.nms.strategy.dto;

import com.uxsino.commons.model.NeClass;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValidateNeBaseInfo {

    // 网元ID
    private String id;

    private String type;

    private String version;

    private String name;

    private String ip;

    // 域
    private String domainId;

    // 值班人员
    private String duty;
    
    //资源类型
    private NeClass neClass;
}
