package com.uxsino.rule.nms.strategy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValidateMsg {

    // 指标名称（中文）
    private String indicator;

    // 存的是indicator的id(配置文件中的id)--指标名称（英文）
    private String indicatorId;

    // 指标的neClass
    private String indNeclass;

    // 部件类型名称 (分区)
    /*private String componentType;

    // 部件key值
    private String componentKey;*/

    // 检测时间
    private String time;

    // 子表达式序列名
    private String subKey;

    // 属性名称（英文）
    private String field;

    // 属性名称（中文）
    private String fieldText;

    // 规则ID
    private Long ruleId;

    // 属性配置ID
    private Long fieldConfId;

    private String currentValue;

    // 当前部件名称
    private String componentName;

    // 当前部件标识
    private String componentId;

    // 告警级别
    private int level;

    // 阈值范围
    private String range;

    // 综述
    private String content;

    // 组合策略公式
    private String formula;

    private ValidateNeBaseInfo ne;

}
