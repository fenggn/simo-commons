package com.uxsino.rule.nms.strategy.model;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.uxsino.rule.nms.model.SubRuleReport;
import com.uxsino.rule.nms.strategy.dto.RuleDto;
import com.uxsino.rule.nms.strategy.dto.ValidateMsg;

public interface CompositionSupport {
    public SubRuleReport support4Modular(JSONObject compData);
    public SubRuleReport support4Holistic();
    public boolean verifierUnit(JSONObject result, String currentComponentId, RuleDto rule,
        JSONObject setting, JSONArray historyArray, ValidateMsg msg, boolean isComposition);
}
