package com.uxsino.rule.nms.handler;

import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.uxsino.rule.model.ListCompareWay;
import com.uxsino.rule.nms.model.NmsCaller;
import lombok.NoArgsConstructor;

/**
 * 
 */
@NoArgsConstructor
public class ListStrategyHandler extends BaseStrategy {

    public ListStrategyHandler(NmsCaller caller) {
        this.callData = caller;
    }

    /**
     * 匹配校验逻辑并返回校验结果
     *
     * @param setting
     * @param value
     */
    @Override
    public boolean ruleMatch(JSONObject setting, String... value) {
        ListCompareWay compareWay = ListCompareWay.valueOf(setting.getString("compareWay"));
        List<String> currentList = StringUtils.isBlank(setting.getString("currentList")) ? Lists.newArrayList() : Lists
            .newArrayList(setting.getString("currentList").split(","));
        List<String> baseList = StringUtils.isBlank(setting.getString("baseList")) ? Lists.newArrayList() : Lists
            .newArrayList(setting.getString("baseList").split(","));
        Collections.sort(currentList);
        Collections.sort(baseList);
        return ListCompareWay.same.equals(compareWay) ? currentList.toString().equals(baseList.toString()) : !currentList
            .toString().equals(baseList.toString());
    }

    @Override
    public String createAlertRange(JSONObject setting) {
        return ListCompareWay.valueOf(setting.getString("compareWay")).getText();
    }

}