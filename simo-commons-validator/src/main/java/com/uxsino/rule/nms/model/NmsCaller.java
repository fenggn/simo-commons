package com.uxsino.rule.nms.model;

import com.alibaba.fastjson.JSONArray;
import com.uxsino.rule.model.ValidateType;
import com.uxsino.rule.nms.strategy.dto.IndicatorTableDto;
import com.uxsino.rule.nms.strategy.dto.RuleDto;
import com.uxsino.rule.nms.strategy.dto.ValidateNeBaseInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * 资源监控策略（network monitoring strategy）规则校验接口调用实体
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NmsCaller {
    
    private ValidateType validateType;
    
    // 检测时间: yyyy-MM-dd HH:mm:ss
    private String time;
    
    private RuleDto rule;
    //指标数据
    private JSONArray indValue;
    
    //historyValue
    private JSONArray historyValue;
    
    //指标信息
    private IndicatorTableDto indicator;

    private ValidateNeBaseInfo ne;

    //虚拟化监控状态信息
    private Map<String,Boolean> vmInfos;

    //部件数据
    private JSONArray comps;
    
}
