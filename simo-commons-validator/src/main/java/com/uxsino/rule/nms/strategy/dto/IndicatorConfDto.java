package com.uxsino.rule.nms.strategy.dto;

import lombok.Data;

/**
 * indicator与策略之间的关联关系表
 * 
 *
 */
@Data
public class IndicatorConfDto {

    private Long id;

    /** 频次ID */
    private String cornExpression;

    private String indicatorName;

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        IndicatorConfDto other = (IndicatorConfDto) obj;
        if (cornExpression == null) {
            if (other.cornExpression != null)
                return false;
        } else if (!cornExpression.equals(other.cornExpression))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (indicatorName == null) {
            if (other.indicatorName != null)
                return false;
        } else if (!indicatorName.equals(other.indicatorName))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((cornExpression == null) ? 0 : cornExpression.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((indicatorName == null) ? 0 : indicatorName.hashCode());
        return result;
    }

}
