package com.uxsino.rule.nms.strategy.dto;

import java.util.List;

import lombok.Data;

@Data
public class MonitorConfDto {

    private Long id;

    private String name;

    /** 适用的网元类型 */
    private String neClass;

    private String neIds;

    private List<IndicatorConfDto> indicatorConfs;

}
