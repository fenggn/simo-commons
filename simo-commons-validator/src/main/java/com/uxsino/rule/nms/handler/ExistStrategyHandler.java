package com.uxsino.rule.nms.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.uxsino.rule.model.ExistsCompareWay;
import com.uxsino.rule.nms.model.NmsCaller;
import com.uxsino.rule.nms.model.NmsReport;
import com.uxsino.rule.nms.strategy.dto.RuleDto;
import com.uxsino.rule.nms.strategy.dto.ValidateAlertInfo;
import com.uxsino.rule.nms.strategy.dto.ValidateMsg;
import com.uxsino.rule.nms.validate.ExistValidator;
import lombok.NoArgsConstructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 
 */
@NoArgsConstructor
public class ExistStrategyHandler extends BaseStrategy {

    public ExistStrategyHandler(NmsCaller caller) {
        this.callData = caller;
    }

    @Override
    public boolean ruleMatch(JSONObject setting, String... value) {
        boolean warning = false;
        String target = setting.getString("value");
        ExistsCompareWay compareWay = ExistsCompareWay.valueOf(setting.getString("compareWay"));
        ExistValidator validator = new ExistValidator(compareWay, value[0], target);
        warning = validator.validate();
        return warning;
    }

    @Override
    public String createAlertRange(JSONObject setting) {
        ExistsCompareWay compareWay = ExistsCompareWay.valueOf(setting.getString("compareWay"));
        return "[" + compareWay.getText() + "]";
    }

    @Override
    public NmsReport holisticVerifyProcess() {
        RuleDto rule = callData.getRule();
        JSONArray array = callData.getIndValue();
        ValidateMsg msg = getMsg();
        Map<String, String> componentMap = getComponents(rule);
        List<String> componentIds = Lists.newArrayList(componentMap.keySet());
        List<ValidateAlertInfo> alertInfoList = new ArrayList<>();
        List<String> existCompIds = Lists.newArrayList();
        for (Object dataObj : array) {
            JSONObject result = JSONObject.parseObject(dataObj.toString());// 当前检测数据
            String componentId = result.containsKey("identifier") ? result.getString("identifier") : "";
            if (!validateEnable(componentId, componentIds)) {// 校验使能
                continue;
            }
            existCompIds.add(componentId);
        }
        boolean alertStatus = false;
        for (Object obj : JSON.parseArray(rule.getSettings())) {
            JSONObject setting = JSON.parseObject(obj.toString());
            ExistsCompareWay compareWay = ExistsCompareWay.valueOf(setting.getString("compareWay"));
            for (String componentId : componentIds) {
                boolean warning = ExistsCompareWay.exists.equals(compareWay) ? existCompIds.contains(componentId)
                        : !existCompIds.contains(componentId);
                if (warning) {
                    alertStatus = true;
                }
                // 校验过程中的数据记录--用于本次校验完成之后的告警...
                msg.setComponentId(componentId);
                msg.setComponentName(componentMap.get(componentId));
                msg.setCurrentValue(compareWay.getText());
                ValidateAlertInfo alert = dealWithValidateResult(setting, warning, msg);
                alertInfoList.add(alert);
            }
        }
        NmsReport report = new NmsReport();
        report.setIndStatus(alertStatus ? hit : not_hit);
        report.setAlertStatus(alertStatus);
        report.setAlertList(alertInfoList);
        return report;
    }

}