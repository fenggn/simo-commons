package com.uxsino.rule.nms.handler;

import com.alibaba.fastjson.JSONObject;
import com.uxsino.rule.model.StringCompareWay;
import com.uxsino.rule.nms.model.NmsCaller;
import com.uxsino.rule.nms.validate.StringValidator;

import lombok.NoArgsConstructor;

/**
 * 
 */
@NoArgsConstructor
public class StringStrategyHandler extends BaseStrategy {


    public StringStrategyHandler(NmsCaller caller) {
        this.callData = caller;
    }

    @Override
    public boolean ruleMatch(JSONObject setting, String... value) {
        StringCompareWay compareWay = StringCompareWay.valueOf(setting.getString("compareWay"));
        String target = setting.getString("value");
        StringValidator validator = new StringValidator(compareWay, value[0], target);
        return validator.validate();
    }

    @Override
    public String createAlertRange(JSONObject setting) {
        String target = setting.getString("value");
        StringCompareWay compareWay = StringCompareWay.valueOf(setting.getString("compareWay"));
        return "[" + compareWay.getText() + "\"" + target + "\"]";
    }
    
}