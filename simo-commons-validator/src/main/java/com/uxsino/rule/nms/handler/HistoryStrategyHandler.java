package com.uxsino.rule.nms.handler;

import com.alibaba.fastjson.JSONObject;
import com.uxsino.commons.utils.StringUtils;
import com.uxsino.rule.model.HistoryCompareWay;
import com.uxsino.rule.nms.model.NmsCaller;
import com.uxsino.rule.nms.validate.ChangeValidator;
import lombok.NoArgsConstructor;

/**
 * 
 */
@NoArgsConstructor
public class HistoryStrategyHandler extends BaseStrategy {

    public HistoryStrategyHandler(NmsCaller caller) {
        this.callData = caller;
    }

    /**
     * 匹配校验逻辑并返回校验结果
     *
     * @param setting
     * @param value
     */

    @Override
    public boolean ruleMatch(JSONObject setting, String... value) {
        boolean warning = false;
        String currentValue = value[0];
        String historyValue = value[1];
        String target = setting.getString("target_value");// 指标属性设置中的目标值
        String source = setting.getString("inital_value");// 指标属性设置中的初始值
        HistoryCompareWay compareWay = HistoryCompareWay.valueOf(setting.getString("compareWay"));
        ChangeValidator validator = new ChangeValidator(compareWay, historyValue, currentValue, source, target);
        warning = validator.validate();

        return warning;

    }

    @Override
    public String createAlertRange(JSONObject setting) {
        String range = "";
        String target = setting.getString("target_value");// 指标属性设置中的目标值
        String inital = setting.getString("inital_value");// 指标属性设置中的初始值
        switch (HistoryCompareWay.valueOf(setting.getString("compareWay"))) {
        case same:
            range = "[不改变]";
            break;
        case change:
            if (StringUtils.isNotEmpty(inital) && StringUtils.isNotEmpty(target)) {// 两个值都不为空字符串
                range = "[\"" + inital + "\"-->\"" + target + "\"]";
            } else if (StringUtils.isNotEmpty(inital) && !StringUtils.isNotEmpty(target)) {// 目标值为空字符串
                range = "[\"" + inital + "\"-->其他值]";
            } else if (!StringUtils.isNotEmpty(inital) && !StringUtils.isNotEmpty(target)) {// 初始值目标值都为空字符串
                range = "[改变]";
            } else if (!StringUtils.isNotEmpty(inital) && StringUtils.isNotEmpty(target)) {// 初始值为空字符串
                range = "[其他值-->\"" + target + "\"]";
            }
            break;
        default:
            range = "阈值范围异常";
            break;
        }
        return range;
    }

}