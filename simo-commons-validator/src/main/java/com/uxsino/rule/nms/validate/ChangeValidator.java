package com.uxsino.rule.nms.validate;

import com.uxsino.commons.utils.StringUtils;
import com.uxsino.rule.model.HistoryCompareWay;

public class ChangeValidator extends BaseValidate {
    private HistoryCompareWay compareWay;

    private String historyValue;

    private String value;

    private String source;

    private String target;

    public ChangeValidator(HistoryCompareWay compareWay, String historyValue, String currentValue, String source,
        String target) {
        this.compareWay = compareWay;
        this.historyValue = historyValue;
        this.value = currentValue;
        this.source = source;
        this.target = target;
    }

    public ChangeValidator(HistoryCompareWay compareWay, String historyValue, String currentValue) {
        this.compareWay = compareWay;
        this.historyValue = historyValue;
        this.value = currentValue;
    }

    @Override
    public boolean validate() {
        switch (compareWay) {
        case same:
            return value.equals(historyValue);
        case change:
            if (StringUtils.isNotEmpty(source) && StringUtils.isNotEmpty(target)) {
                return target.equals(value) && source.equals(historyValue);
            } else if (StringUtils.isNotEmpty(source) && !StringUtils.isNotEmpty(target)) {
                return source.equals(historyValue) && !value.equals(historyValue);
            } else if (!StringUtils.isNotEmpty(source) && StringUtils.isNotEmpty(target)) {
                return target.equals(value) && !value.equals(historyValue);
            } else {
                return !value.equals(historyValue);
            }
        default:
            return false;
        }
    }

}
