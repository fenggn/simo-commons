package com.uxsino.rule.nms.strategy.dto;

import com.alibaba.fastjson.JSONArray;
import com.uxsino.rule.model.SqlBox;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IndicatorTableDto {
    private Long id;

    private String name;

    private String label;

    /*private String componentType;

    private String componentKey;*/

    private String componentNameFormula;// 表达式，标识子资源名称

    private JSONArray fields;

    private String indicatorType;

    private Boolean noKey;

    public IndicatorTableDto indId(Long id) {
        this.id = id;
        return this;
    }

    public IndicatorTableDto name(String name) {
        this.name = name;
        return this;
    }

    public IndicatorTableDto label(String label) {
        this.label = label;
        return this;
    }

    public IndicatorTableDto compNameFormula(String componentNameFormula) {
        this.componentNameFormula = componentNameFormula;
        return this;
    }

    public IndicatorTableDto fields(JSONArray fields) {
        this.fields = fields;
        return this;
    }

    public IndicatorTableDto indicatorType(String indicatorType) {
        this.indicatorType = indicatorType;
        return this;
    }

    public IndicatorTableDto noKey(Boolean noKey) {
        this.noKey = noKey;
        return this;
    }

    public SqlBox allIndicatorSql() {
        StringBuffer sb = new StringBuffer();
        sb.append("select new com.uxsino.rule.nms.strategy.dto.IndicatorTableDto ");
        sb.append("(a.id as id, a.name as name, a.label as label, "/*+"a.componentType as componentType, a.componentKey as componentKey, "*/
                + "a.componentNameFormula as componentNameFormula, a.fields as fields) ");
        sb.append("from IndicatorTable where 1=1");
        SqlBox ret = new SqlBox();
        ret.setSql(sb.toString());
        return ret;
    }
}
