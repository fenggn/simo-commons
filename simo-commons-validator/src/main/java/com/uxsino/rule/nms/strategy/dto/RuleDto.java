package com.uxsino.rule.nms.strategy.dto;

import com.uxsino.commons.model.NeClass;
import com.uxsino.rule.model.CompareType;
import com.uxsino.rule.model.CompsRange;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RuleDto {

    private Long id;

    // 策略的id monitorConf
    private Long strategyId;

    // 策略名称 monitorconfName
    private String strategyName;

    private NeClass neClass;

    private String neIds;

    private Long igId;

    private String indicatorName;

    private CompareType compareType;

    private CompsRange compsRange;

    // 属性配置ID
    private Long fieldConfId;

    private String fieldName;

    private String fieldText;

    /**设置列表: 阈值范围[begin,end] , level , eventId*/
    private String settings;

    private String neComps;

}
