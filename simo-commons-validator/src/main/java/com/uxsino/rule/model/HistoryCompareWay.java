package com.uxsino.rule.model;

public enum HistoryCompareWay {
                               same(8,"一致","=="),
                               change(9,"不一致","!=");

    private int value;

    private String text;

    private String sign;

    private HistoryCompareWay(int value, String text, String sign) {
        this.value = value;
        this.text = text;
        this.sign = sign;

    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public String getSign() {
        return sign;
    }

    public static HistoryCompareWay getCompareWay(String compareWay) {
        for (HistoryCompareWay way : HistoryCompareWay.values()) {
            if (way.toString().equals(compareWay)) {
                return way;
            }
        }
        return null;
    }

}
