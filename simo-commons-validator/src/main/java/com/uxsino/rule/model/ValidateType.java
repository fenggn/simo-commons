package com.uxsino.rule.model;

public enum ValidateType {
    
    INDICATOR_PATROL(1,"指标巡检"),
    RECOVERY(2,"故障自愈");

    private int value;

    private String text;

    private ValidateType(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }
}
