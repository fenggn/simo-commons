package com.uxsino.rule.model;

public enum ExistsCompareWay {
                              exists(1,"存在","exists"),
                              notexists(2,"不存在","notexists");

    private int value;

    private String text;

    private String sign;

    private ExistsCompareWay(int value, String text, String sign) {
        this.value = value;
        this.text = text;
        this.sign = sign;

    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public String getSign() {
        return sign;
    }

    public static ExistsCompareWay getCompareWay(String compareWay) {
        for (ExistsCompareWay way : ExistsCompareWay.values()) {
            if (way.toString().equals(compareWay)) {
                return way;
            }
        }
        return null;
    }

}
