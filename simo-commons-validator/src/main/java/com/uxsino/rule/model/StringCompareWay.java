package com.uxsino.rule.model;

public enum StringCompareWay {
                              CONTAINS(1,"包含","contains"),
                              NOT_CONTAINS(2,"不包含","contains"),
                              START_WITH(3,"开始于","start with"),
                              END_WITH(4,"结束于","end with"),
                              EQUALS(6,"等于","=="),
                              NOT_EQUALS(7,"不等于","!=");
    // regexper(5,"正则匹配","regex");

    private int value;

    private String text;

    private String sign;

    private StringCompareWay(int value, String text, String sign) {
        this.value = value;
        this.text = text;
        this.sign = sign;

    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public String getSign() {
        return sign;
    }

    public static StringCompareWay getCompareWay(String compareWay) {
        for (StringCompareWay way : StringCompareWay.values()) {
            if (way.toString().equals(compareWay)) {
                return way;
            }
        }
        return null;
    }

}
