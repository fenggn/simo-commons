package com.uxsino.rule.model;

public enum CompareType {
                         range(1,"阈值检测"),
                         string(2,"字符串检测"),
                         history(3,"历史数据对比"),
                         sum(4,"数量检测"),
                         exists(5,"存在性检测"),
                         composition(6,"组合策略检测"),
                         change(7,"变更检测");

    private int value;

    private String text;

    private CompareType(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

}
