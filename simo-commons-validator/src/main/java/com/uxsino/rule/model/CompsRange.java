package com.uxsino.rule.model;

public enum CompsRange {
    selected(1,"已选部件"),
    all(2,"不限");

    private int value;

    private String text;

    private CompsRange(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

}
