package com.uxsino.rule.model;

public enum DefaultTemple {
    
    DEF_ALERT_TPL("【${资源名称} ${IP地址}】的【${指标名称}】在${告警时间}产生【${告警级别}】级别的告警，内容为：【${部件名称}】的【${属性名称}】${比较方式}异常，当前值：${采集值}，阈值：${阈值}"),
    DEF_RECOVER_TPL("【${资源名称} ${IP地址}】的【${指标名称}】在${告警时间}已恢复，内容为：【${部件名称}】的【${属性名称}】${比较方式}正常，当前值：${采集值}，阈值：${阈值}"),
    DEF_UNUSABLE_TPL("【${资源名称} ${IP地址}】不可用"),
    DEF_USEABLE_TPL("【${资源名称} ${IP地址}】恢复可用"),
    DEF_COMP_CHANGE_TPL("【${资源名称} ${IP地址}】的【${指标名称}】在${告警时间}部件变更"),
    DEF_COMP_SAME_TPL("【${资源名称} ${IP地址}】的【${指标名称}】在${告警时间}部件未变更"),

    /*链路默认告警模板*/
    DEF_LINK_ALERT_TPL("${链路名称}【${指标名称}】：${采集值}，阈值范围：${阈值范围}"),
    DEF_LINK_UNUSABLE_TPL("${链路名称}【链路${指标名称}】：不可用"),
    DEF_LINK_USEABLE_TPL("${链路名称}【链路${指标名称}】：恢复可用"),

    /*ip告警模板*/
    DEF_IP_ALERT_TPL("【局域网（${局域网}）-子网（${子网}）】中，IP（${IP地址}）在（${告警时间}）时间产生了【（${告警级别}）】级别的告警，告警内容为【${告警类型}】"),
    DEF_IP_RECOVER_TPL("【局域网（${局域网}）-子网（${子网}）】中，IP（${IP地址}）在（${告警时间}）时间内，${恢复信息}"),
    /*ip冲突*/
    DEF_IP_CONFLICT_TPL("，当前MAC地址：（${采集值}），基准MAC地址：（${阈值}）"),
    /*上联设备/端口变化*/
    DEF_IP_SOURCE_TPL("，当前${变化类型}：（${采集值}），基准${变化类型}：（${阈值}）"),
    /*子网使用率*/
    DEF_IP_SUBNET_USAGE_TPL("【局域网（${局域网}）-子网（${子网}）】在（${告警时间}）时间产生了【（${告警级别}）】级别的告警，告警内容为${告警类型}超过阈值，当前值：（${采集值}），阈值：（${阈值}）"),
    DEF_IP_SUBNET_USAGE_RECOVER_TPL("【局域网（${局域网}）-子网（${子网}）】在（${告警时间}）时间内，${恢复信息}");

    private String text;
    
    DefaultTemple(String text) {
        this.text = text;
    }
    
    public String getText() {
        return text;
    }
}
