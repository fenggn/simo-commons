package com.uxsino.rule.model;

public enum NumberCompareWay {
                              EQUALS(1,"等于","=="),
                              NOT_EQUALS(2,"不等于","!="),
                              GREATER(3,"大于",">"),
                              LESS(4,"小于","<"),
                              GREAT_EREQUALS(5,"大于等于",">="),
                              LESS_EQUALS(6,"小于等于","<="),
                              RANGE(7,"范围","range");

    private int value;

    private String text;

    private String sign;

    private NumberCompareWay(int value, String text, String sign) {
        this.value = value;
        this.text = text;
        this.sign = sign;

    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public String getSign() {
        return sign;
    }

    public static NumberCompareWay getCompareWay(String compareWay) {
        for (NumberCompareWay way : NumberCompareWay.values()) {
            if (way.toString().equals(compareWay)) {
                return way;
            }
        }
        return null;
    }

}
