package com.uxsino.rule.model;

import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 用于所有需要同时返回sql语句和sql执行参数
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SqlBox {
    
    private String sql;
    
    private Map<String, Object> params;
}
