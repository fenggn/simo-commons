package com.uxsino.rule.util.function;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.runtime.function.AbstractVariadicFunction;
import com.googlecode.aviator.runtime.type.AviatorBoolean;
import com.googlecode.aviator.runtime.type.AviatorObject;

/**
 * 
 * 
 *
 */
public class CompareFunctionCustom extends AbstractVariadicFunction {

    @Override
    public String getName() {
        return "compare";
    }

    @Override
    public AviatorObject variadicCall(Map<String, Object> env, AviatorObject... args) {
        JSONObject obj = (JSONObject) args[0].getValue(env);
        String expression = (String) args[1].getValue(env);
        return AviatorBoolean.valueOf((boolean) AviatorEvaluator.exec(expression, obj));
    }

}
