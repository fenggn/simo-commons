package com.uxsino.rule.util;

import com.uxsino.rule.template.model.IntervalType;

public class CornConstructorUtil {

    public static String corn(int number, IntervalType interval) {
        String corn = "";
        switch (interval) {
        case minute:
            if (0 < number && number <= 59) {
                corn = "0 0/" + number + " * * * ?";
            }

            break;
        case hour:
            if (0 < number && number <= 23) {
                corn = "0 0 0/" + number + " * * ?";
            }
            break;
        case day:
            if (0 < number) {
                corn = "0 0 0 1/" + number + " * ?";
            }
            break;

        default:
            break;
        }

        return corn;
    }

}
