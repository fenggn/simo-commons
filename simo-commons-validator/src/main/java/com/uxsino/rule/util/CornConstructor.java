package com.uxsino.rule.util;

import com.uxsino.rule.template.model.IntervalType;

public class CornConstructor {

    public static String corn(int number, IntervalType interval) {
        String corn = "";
        switch (interval) {
        case minute:
            if (0 < number && number <= 59) {
                corn = "0 0/" + number + " * * * ? *";
            }

            break;
        case hour:
            corn = "0 0 0/" + number + " * * ? *";
            break;
        case day:

            break;

        default:
            break;
        }

        return corn;
    }

}
