package com.uxsino.rule.util;

import java.util.Map;

import com.googlecode.aviator.runtime.function.AbstractVariadicFunction;
import com.googlecode.aviator.runtime.function.FunctionUtils;
import com.googlecode.aviator.runtime.type.AviatorDouble;
import com.googlecode.aviator.runtime.type.AviatorObject;

public class CustomAdd extends AbstractVariadicFunction {

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return "test";
    }

    @Override
    public AviatorObject variadicCall(Map<String, Object> env, AviatorObject... args) {
        Number left = FunctionUtils.getNumberValue(args[0], env);
        Number right = FunctionUtils.getNumberValue(args[1], env);
        return new AviatorDouble(left.doubleValue() + right.doubleValue());
    }

}
