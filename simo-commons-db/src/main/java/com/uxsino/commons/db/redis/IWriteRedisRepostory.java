package com.uxsino.commons.db.redis;

public interface IWriteRedisRepostory<T> extends IRedisRepostory<T> {

    void put(String key, T t);

    /**
     * 
     * @param key
     * @param t
     * @param expire 过期时间,如果为-1,则不设置过期时间
     */
    void put(String key, T t, long expire);

    void remove(String key);

    void empty();

}
