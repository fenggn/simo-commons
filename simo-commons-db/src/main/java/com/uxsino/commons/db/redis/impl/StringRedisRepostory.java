package com.uxsino.commons.db.redis.impl;

import com.alibaba.fastjson.TypeReference;
import org.springframework.stereotype.Component;

import com.uxsino.commons.db.redis.RedisKeys;

@Component
public class StringRedisRepostory extends AbstractRedisRepostory<String> {

    @Override
    public String getRedisKey() {
        return RedisKeys.PATTEN_KEY_STRING;
    }

    @Override
    protected TypeReference<String> getSerializeClass() {
        return new TypeReference<String>(){};
    }
}
