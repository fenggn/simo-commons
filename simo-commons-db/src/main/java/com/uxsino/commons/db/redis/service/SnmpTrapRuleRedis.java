package com.uxsino.commons.db.redis.service;

import org.springframework.stereotype.Component;
import com.uxsino.commons.db.redis.RedisKeys;
import com.uxsino.commons.db.redis.impl.AbstracStringtReadRedisRepostory;

@Component
public class SnmpTrapRuleRedis extends AbstracStringtReadRedisRepostory {

    @Override
    public String getRedisKey() {
        return RedisKeys.PATTEN_KEY_SNMPTRAP_RULE;
    }
}
