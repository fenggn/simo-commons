package com.uxsino.commons.db.model;

public enum IntervalType {

                          minute(1,"分钟"),
                          hour(2,"小时"),
                          day(3,"天"),
                          week(4,"周")

    ;

    private String text;

    private int value;

    private IntervalType(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public int getValue() {
        return value;
    }

}
