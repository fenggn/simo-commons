package com.uxsino.commons.db.redis.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.uxsino.commons.db.redis.IReadRedisRepostory;

public abstract class AbstracStringtReadRedisRepostory implements IReadRedisRepostory<String> {

    @Autowired
    private HashOperations<String, String, String> hashOperations;

    public JSONObject getObj(String key) {
        String json = this.get(key);
        if (StringUtils.isEmpty(json)) {
            return null;
        }
        return JSONObject.parseObject(json);
    }

    @Override
    public String get(String key) {
        return hashOperations.get(getRedisKey(), key);
    }

    @Override
    public List<String> getAll() {
        return hashOperations.values(getRedisKey());
    }

    @Override
    public Set<String> getKeys() {
        return hashOperations.keys(getRedisKey());
    }

    @Override
    public boolean isKeyExists(String key) {
        return hashOperations.hasKey(getRedisKey(), key);
    }

    @Override
    public long count() {
        return hashOperations.size(getRedisKey());
    }

}
