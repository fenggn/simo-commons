package com.uxsino.commons.db.model;

import java.util.Date;
import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @description SIMO消息
 * @date 2019年6月4日
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SimoMsg {

    /**
     * 消息来源
     */
    private String msgFrom;

    /**
     * 消息类别
     */
    private String msgType;

    /**
     * 最新消息时间
     */
    private Date msgTime;

    /**
     * 消息分组id，用来标识属于同一个消息，类似于消息分组
     */
    private String hashKey;

    /**
     * 消息的唯一标识
     */
    private String uid;

    /**
     * 消息内容，可以是字符串，可以jsonb
     */
    private String content;

    /**
     * 接收消息人： {"email": ["a@uxsino.com","b@uxsino.com"], "sms": ["13800138001","13800138002"], "socket": [1,2,3]}
     */
    private JSONObject receiver;

    /**
     * 扩展参数，用于前端渲染、跳转：{"level":2, "alertType":"Alert", "alertId":1, "isAvailability":false}
     */
    private JSONObject extParam;

}
