package com.uxsino.commons.db.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.uxsino.commons.db.repository.ICustomRepository;

public class CustomRepositoryUtils {

    public static final Logger logger = LoggerFactory.getLogger(CustomRepositoryUtils.class);

    public static void excuteSqlFile(String sqls, ICustomRepository<?, ?> dao) {
        try {
            String[] sqlStatements = sqls.split(";");
            for (String sqlStatement : sqlStatements) {
                if (!StringUtils.isEmpty(sqlStatement)) {
                    dao.updateBySQL(sqlStatement + ";", null);
                }
            }
        } catch (Exception e) {
            logger.error("sql excute fail!", e);
        }
    }

}
