package com.uxsino.commons.db.redis.impl;

import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class SimoRedisService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 条件查询key
     * @param pattern 查询字符串
     *              * ：通配任意多个字符
     *              ? ：通配单个字符
     *              [] ：通配括号内的某一个字符
     * @return
     */
    public Set<String> keys(String pattern) {
        return stringRedisTemplate.keys(pattern);
    }

    /**
     * 条件删除key
     * @param pattern 查询字符串
     *              *：通配任意多个字符
     *              ?：通配单个字符
     *              []：通配括号内的某一个字符
     * @return
     */
    public void emptyByPattern(String pattern) {
        Set<String> keySet = keys(pattern);
        keySet.stream().forEach(redisKey -> stringRedisTemplate.delete(redisKey));
    }

    /**
     * 删除某个key
     * @param redisKey
     */
    public void deleteRedisKey(String redisKey) {
        stringRedisTemplate.delete(redisKey);
    }

}
