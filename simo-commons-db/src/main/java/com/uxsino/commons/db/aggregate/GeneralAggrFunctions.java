package com.uxsino.commons.db.aggregate;

/**
 * general aggregate functions used in {@link Cube}
 * 
 *
 */
public class GeneralAggrFunctions {

    public static class Count implements IAggregateFunction {

        @Override
        public String getName() {
            return "COUNT";
        }
    }

    public static class Avg implements IAggregateFunction {

        @Override
        public String getName() {
            return "AVG";
        }
    }

    public static class CountDistinct implements IAggregateFunction {

        @Override
        public String getName() {
            return "COUNT";
        }

        @Override
        public String toSql(String fieldExpr) {
            return "COUNT(DISTINCT " + fieldExpr + ")";
        }
    }

    public static class Max implements IAggregateFunction {

        @Override
        public String getName() {
            return "MAX";
        }
    }

    public static class Min implements IAggregateFunction {

        @Override
        public String getName() {
            return "MIN";
        }
    }

    public static class Sum implements IAggregateFunction {

        @Override
        public String getName() {
            return "SUM";
        }
    }

    public static Count count = new Count();

    public static Avg avg = new Avg();

    public static CountDistinct countDistinct = new CountDistinct();

    public static Max max = new Max();

    public static Min min = new Min();

    public static Sum sum = new Sum();
}
