package com.uxsino.commons.db.aggregate;

public class SubSelectDimension implements IDimension {

    protected String sql;

    protected String primaryKey;

    protected SubSelectDimension() {

    }

    public SubSelectDimension(String sql, String primaryKey) {
        this.sql = sql;
        this.primaryKey = primaryKey;
    }

    @Override
    public String getSrcExpr() {
        return sql;
    }

    @Override
    public boolean isSimpleTable() {
        return false;
    }

    @Override
    public String getPrimaryKey() {
        return primaryKey;
    }

}
