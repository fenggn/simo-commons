/**
 * 
 */
package com.uxsino.commons.db.base;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 描述：基本的实体类
 * 
 * 时间：2017年6月20日
 */
@SuppressWarnings("serial")
@Data
@MappedSuperclass
@EntityListeners(value = { AuditingEntityListener.class })
public class BaseEntity implements Serializable {
    @CreatedDate
    private Date createTime;

    @LastModifiedDate
    private Date updateTime;
}
