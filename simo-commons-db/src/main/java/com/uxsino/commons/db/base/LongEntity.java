/**
 * 
 */
package com.uxsino.commons.db.base;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 描述：整型ID 基础实体
 * 
 * 时间：2017年6月20日
 */
@SuppressWarnings("serial")
@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper = false)
public class LongEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
}
