package com.uxsino.commons.db.redis;

public class RedisKeys {

    public final static String PATTEN_KEY_STRING = "SIMO.SIMPLE.STRING";

    public final static String PATTEN_KEY_SITEUSER = "SIMO.OBJECT.SITEUSER";

    public final static String PATTEN_KEY_OO = "OBJECT.MAPPING";

    public final static String PATTEN_KEY_EVENTTEMPLATEINFO = "SIMO.OBJECT.EVENTTEMPLATEINFO";

    public final static String PATTEN_KEY_ALERTSTATUS = "SIMO.OBJECT.ALERT.STATUS";

    public final static String PATTEN_KEY_INDICATORCACHE = "SIMO.OBJECT.INDICATORCACHE";

    public final static String PATTEN_KEY_INDICATORFIELD = "SIMO.OBJECT.INDICATORFIELD";

    public final static String PATTEN_KEY_FLAPINFO = "SIMO.OBJECT.FLAP.INFO";

    public final static String PATTEN_KEY_NEINDVAL = "SIMO.OBJECT.NEINDVAL";

    public final static String SIMO_USERS_AUTHORITY = "SIMO.USERS.AUTHORITY";

    public final static String PATTEN_KEY_SYSLOG_RULE = "SIMO.OBJECT.SYSLOG.RULE";

    public final static String PATTEN_KEY_SNMPTRAP_RULE = "SIMO.OBJECT.SNMPTRAP.RULE";

}
