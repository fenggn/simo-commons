/**
 * 
 */
package com.uxsino.commons.db.base;

import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 描述：
 * 
 * 时间：2017年6月26日
 */
public class EntityListener {

    Log log = LogFactory.getLog(EntityListener.class);

    @PrePersist
    void onPrePersist(Object o) {
    }

    @PostPersist
    void onPostPersist(Object o) {
    }

    @PostLoad
    void onPostLoad(Object o) {
    }

    @PreUpdate
    void onPreUpdate(Object o) {
    }

    @PostUpdate
    void onPostUpdate(Object o) {
    }

    @PreRemove
    void onPreRemove(Object o) {
    }

    @PostRemove
    void onPostRemove(Object o) {
    }
}
