package com.uxsino.commons.db.model;

import java.util.List;
import lombok.Data;

/**
 * 事件模板 </p>
 * eg. 在事件发生时的 [全天/时间段], 连续发生[1/^]次之后, 以[邮件/短信/系统]方式, 通知 [指定人员/指定角色/巡检值班人员]   </p>
 * 系统通知:默认通知给当前的操作者 [告警列表会插入数据,根据权限进行查看,就不关联到人员了] 系统通知 与 插入告警 是不同的事件
 * 
 *
 */
@Data
public class AlertStrategy {

    private Long id;

    private Byte status;

    private List<AlertStrategyItme> itmes;

}
