package com.uxsino.commons.db.queue.dao;

import com.uxsino.commons.db.queue.entity.DBQ;
import com.uxsino.commons.db.repository.ICustomRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

/**
 *
 * 仅支持jpql, 原生sql不提倡使用。
 *
 * @ClassName DBQDao
 * @Description TODO
 * @Author <a href="mailto:royrxc@gmail.com">Ran</a>
 * @Daate 2019/12/14 11:07
 **/

public interface DBQDao extends ICustomRepository<DBQ, Long> {

//    @Modifying
//    @Transactional
//    @Query(value = "delete from DBQ where queueName = :queueName")
//    int deleteByQueueName(@Param("queueName") String queueName);



    @Modifying
    @Transactional
    int deleteByQueueNameAndIdIn(String queueName, Collection<Long> ids);


    @Modifying
    @Transactional
    @Query(value = "delete from DBQ where id IN :ids")
    int deleteByIdIn(@Param("ids") Collection<Long> ids);

    default List<DBQ> poll(String queueName, int size){
        return findByQueueNameOrderByIdAsc(queueName, PageRequest.of(0, size)).getContent();
    }

    Page<DBQ> findByQueueNameOrderByIdAsc(String queueName, Pageable page);


    @Modifying
    @Transactional
    default List<DBQ> push(Collection<DBQ> datas){
        return this.saveAll(datas);
    }


    Long countByQueueName(String queueName);






}
