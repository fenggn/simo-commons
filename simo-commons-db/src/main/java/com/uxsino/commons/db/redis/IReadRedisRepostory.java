package com.uxsino.commons.db.redis;

import java.util.List;
import java.util.Set;

public interface IReadRedisRepostory<T> extends IRedisRepostory<T> {

    T get(String key);

    List<T> getAll();

    Set<String> getKeys();

    boolean isKeyExists(String key);

    long count();

}
