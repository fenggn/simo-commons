/**
 * 
 */
package com.uxsino.commons.db.base;

import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 描述：范围对象 Embeddable
 * 
 * 时间：2017年6月20日
 * @param <T>
 */
@Data
@MappedSuperclass
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class Range<T> {
    private T rangeFrom;

    private T rangeTo;

    public static <T> Range<T> of(T from, T to) {
        return new Range<T>(from, to);
    }
}
