package com.uxsino.commons.db.redis.service;

import org.springframework.stereotype.Component;
import com.uxsino.commons.db.redis.RedisKeys;
import com.uxsino.commons.db.redis.impl.AbstracStringtReadRedisRepostory;

/**
 * 描述：获取事件模板
 * 
 * 时间：2017年8月16日
 */
@Component
public class EventTemplateRedis extends AbstracStringtReadRedisRepostory {

    @Override
    public String getRedisKey() {
        return RedisKeys.PATTEN_KEY_EVENTTEMPLATEINFO;
    }
}
