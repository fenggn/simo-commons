package com.uxsino.commons.db.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.uxsino.commons.db.repository.impl.CustomRepositoryImpl;

@Configuration
@EnableJpaRepositories(repositoryBaseClass = CustomRepositoryImpl.class)
public class JpaDataConfig {

}
