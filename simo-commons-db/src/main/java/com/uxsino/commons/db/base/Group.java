/**
 * 
 */
package com.uxsino.commons.db.base;

import javax.persistence.MappedSuperclass;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 描述：
 * 
 * 时间：2017年6月21日
 */
@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = false)
@MappedSuperclass
public class Group extends LongEntity {
    private Long parentId;

    private String name;

    private Integer status;// 1为正常状态， 0 表示已经删除

    // 看看是否有其他的公用属性
}
