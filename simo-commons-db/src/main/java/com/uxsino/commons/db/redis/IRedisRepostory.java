package com.uxsino.commons.db.redis;

public interface IRedisRepostory<T> {

    String getRedisKey();

}
