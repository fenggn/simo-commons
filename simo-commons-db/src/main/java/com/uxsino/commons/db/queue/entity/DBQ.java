package com.uxsino.commons.db.queue.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName DBQ 队列
 * @Description TODO
 * @Author <a href="mailto:royrxc@gmail.com">Ran</a>
 * @Daate 2019/12/14 10:41
 **/
@Entity
@Data
@Table(name = "simo_queue_${spring.application.name:core}",
        indexes = {
        @Index(/*name = "idx_simo_queue_${spring.application.name:#{core}}_query_time", */columnList = "queueName,time")
})
public class DBQ implements Serializable {

    public DBQ(String queueName, String data){
        this.queueName = queueName;
        this.data = data;
        this.time = new Date();
    }

    public DBQ(){}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    /**
     * 数据
     */
    @Column(columnDefinition = "text")
    private String data;

    /**
     * 队列名称
     */
    private String queueName;


    /**
     * 入队时间
     */
    @Column(nullable = false, columnDefinition = "timestamp default CURRENT_TIMESTAMP")
    private Date time;


}
