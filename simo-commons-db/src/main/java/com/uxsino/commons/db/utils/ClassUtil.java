package com.uxsino.commons.db.utils;

import java.lang.reflect.Field;
import javax.persistence.Column;

/**
 * 
 * @description 类工具
 * @date 2018年8月2日
 */
public class ClassUtil {

    /**
     * 对象非空字段验空
     * @param obj 对象
     * @return
     * @throws IllegalAccessException 
     * @throws IllegalArgumentException 
     */
    public static boolean checkNullField(Object obj) throws IllegalArgumentException, IllegalAccessException {
        if (obj == null) {
            return false;
        }

        Field[] fields = obj.getClass().getDeclaredFields();
        if (fields == null || fields.length == 0) {
            return false;
        }
        for (Field field : fields) {
            field.setAccessible(true);
            Column column = field.getAnnotation(Column.class);
            if (column != null && !column.nullable() && field.get(obj) == null) {
                return true;
            }
        }
        return false;
    }

}
