package com.uxsino.commons.db.strategy;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.core.env.Environment;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;

/**
 * @ClassName NameStrategy, table name for el expression
 * @Description TODO
 * @Author <a href="mailto:royrxc@gmail.com">Ran</a>
 * @Daate 2019/12/19 14:54
 **/
@Component
public class NameStrategy extends SpringPhysicalNamingStrategy {
    private final StandardEvaluationContext context = new StandardEvaluationContext();

    @Autowired
    Environment env;

    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        if((name.getText().contains("#") || name.getText().contains("$")) && name.getText().contains("{") && name.getText().contains("}")){
            String nm = env.resolvePlaceholders(name.getText());
            return Identifier.toIdentifier(nm);
        }
        return super.toPhysicalTableName(name, jdbcEnvironment);
    }


}
