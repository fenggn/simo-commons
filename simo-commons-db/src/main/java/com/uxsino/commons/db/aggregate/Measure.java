package com.uxsino.commons.db.aggregate;

import com.alibaba.druid.util.StringUtils;

public class Measure {
    public String name;

    public String columnExpr;

    public IAggregateFunction aggrFunc;

    public int precision;

    public String toSql() {
        return (aggrFunc == null ? columnExpr
                : ("round(CAST(" + aggrFunc.toSql(columnExpr) + " as numeric), " + precision + ")"))
                + (StringUtils.isEmpty(name) ? "" : " as " + name);
    }
}