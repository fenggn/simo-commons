package com.uxsino.commons.db.aggregate;

/**
 * dimension in an olap like {@link Cube}
 * 
 *
 */
public interface IDimension {

    /**
     * a table name of sub select expr: '(SELECT ... FROM ...)'
     * @return
     */
    public String getSrcExpr();

    /**
     * if the source is a simple table, not a sub select statement
     * @return
     */
    public boolean isSimpleTable();

    /**
     * the key field name
     * @return
     */
    public String getPrimaryKey();

}
