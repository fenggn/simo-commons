/**
 * 
 */
package com.uxsino.commons.db.criteria;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.uxsino.commons.db.model.PageModel;
import com.uxsino.commons.db.repository.ICustomRepository;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 描述：
 * 
 * 时间：2017年6月28日
 */
@Data
@EqualsAndHashCode(callSuper = false)
public abstract class Criteria<T> extends PageModel {
    private static final Log LOG = LogFactory.getLog(Criteria.class);

    public abstract String createQuery();

    public abstract String createCount();

    private Class<T> cls;

    private boolean pagination = true;

    private boolean nativeQuery = false;

    private String sortField;

    private Boolean asc = Boolean.FALSE;// 是否升序排列

    public static String escapeLike(String likeKeyWord) {
        if (likeKeyWord == null) {
            return "%%";
        } else {
            return "%" + escape(likeKeyWord) + "%";
        }
    }

    public static String escape(String likeKeyWord) {
        if (likeKeyWord == null) {
            return "";
        } else {
            return (likeKeyWord.replaceAll("_", "\\\\_").replaceAll("\\[", "\\\\[").replaceAll("\\]", "\\\\]")
                .replaceAll("\\^", "\\\\^").replaceAll("%", "\\\\%"));
        }
    }

    public Map<String, Object> param() {
        return new HashMap<String, Object>();
    }

    public Criteria(Class<T> cls) {
        this.cls = cls;
    }

    /**
     * 下划线命名转驼峰
     * @param str
     * @return
     */
    public static String underlineToHump(String str) {
        Pattern linePattern = Pattern.compile("_(\\w)");
        str = str.toLowerCase();
        Matcher matcher = linePattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    /**
     * 驼峰命名转下划线命名
     * @param fieldName
     * @return
     */
    public static String humpToUnderline(String fieldName) {
        Pattern humpPattern = Pattern.compile("[A-Z]");
        Matcher matcher = humpPattern.matcher(fieldName);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    public Long count(ICustomRepository<?, ?> dao) {
        Long count = 0L;
        if (nativeQuery) {
            count = dao.countBySql(this.createCount(), this.param());
        } else {
            count = dao.countByJPQL(this.createCount(), this.param());
        }
        this.setCount(count);
        return this.getCount();
    }

    @SuppressWarnings("unchecked")
    public Criteria<T> doSearch(ICustomRepository<?, ?> dao) {
        if (!this.pagination) {
            this.setPageSize(Integer.MAX_VALUE);
        }
        try {
            Lists.<Runnable>newArrayList(()->{
                this.count(dao);
            },()->{
                List<T> datas;
                if (this.pagination) {
                    if (nativeQuery) {
                        datas = (List<T>) dao.nativeQuery(this.createQuery(), cls, this.param(), this.getCurrentNo(),
                                this.getPageSize());
                    } else {
                        datas = (List<T>) dao.findByJPQL(this.createQuery(), this, this.param(), cls);
                    }
                } else {
                    if (nativeQuery) {
                        datas = (List<T>) dao.nativeQuery(this.createQuery(), cls, this.param());
                    } else {
                        datas = (List<T>) dao.findByJPQL(this.createQuery(), this.param(), cls);
                    }
                }
                this.setObject(datas);
            }).parallelStream().forEach(run->run.run());
        } catch (Exception e) {
            LOG.error("Criteria doSearch (...)执行失败,JPQL语句:" + this.createQuery() + "，错误：" + "", e);
        }
        return this;
    }

    public PageModel parsePageModel() {
        PageModel pageModel = new PageModel();
        pageModel.setCount(this.getCount());
        pageModel.setCurrentNo(this.getCurrentNo());
        pageModel.setPageSize(this.getPageSize());
        pageModel.setTotalPages(this.getTotalPages());
        pageModel.setObject(this.getObject());
        return pageModel;
    }

}
