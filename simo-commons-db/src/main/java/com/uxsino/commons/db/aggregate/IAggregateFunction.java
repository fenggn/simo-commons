package com.uxsino.commons.db.aggregate;

public interface IAggregateFunction {
    public String getName();

    default public String toSql(String fieldExpr) {
        return getName() + "(" + fieldExpr + ")";
    }
}
