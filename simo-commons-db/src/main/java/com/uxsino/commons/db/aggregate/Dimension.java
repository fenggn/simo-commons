package com.uxsino.commons.db.aggregate;

/**
 * 
 * a dimension use simple table. see {@link IDimension}, {@link Cube}
 *
 */
public class Dimension implements IDimension {

    public String tableName;

    public String primaryKey;

    public Dimension() {

    }

    public Dimension(String tableName, String primaryKey) {
        this.tableName = tableName;
        this.primaryKey = primaryKey;
    }

    @Override
    public String getSrcExpr() {
        return tableName;
    }

    @Override
    public boolean isSimpleTable() {
        return true;
    }

    @Override
    public String getPrimaryKey() {
        return primaryKey;
    }

}
