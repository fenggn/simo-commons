package com.uxsino.commons.db.model;

import lombok.Data;

@Data
public class AlertStrategyItme {

    private Long id;

    private Long strategyId;

    /** 通知的方式:邮件、短信 */
    private String notifyWay;

    private Byte status;

    private String userIds;

    /** 发送的时机 */
    private Occasion occasion;

    private IntervalType intervalTimeUnit;

    /** 间隔多少分钟发一次 */
    private Integer intervalTime;

    private SendTimeWay sendTime;

    private String sendTimeInfo;

}
