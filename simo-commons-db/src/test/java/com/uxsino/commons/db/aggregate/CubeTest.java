package com.uxsino.commons.db.aggregate;

import org.junit.Test;

public class CubeTest {

	private Cube cube = new Cube("simo_business_request");

	@Test
	public void testCreateAggrSql() {
		Dimension d1 = new Dimension();
		d1.tableName = "simo_business_request_relation";
		d1.primaryKey = "request_type_id";

		Dimension d2 = new Dimension();
		d2.tableName = "simo_business_request_type";
		d2.primaryKey = "url";
		cube.addDimension("uri", d2);
		cube.addDimension("simo_business_request_type", "id", "LEFT", d1, "br_type");
		
		Measure m1 = new Measure();
		
		m1.aggrFunc=GeneralAggrFunctions.count;
		m1.columnExpr="simo_business_request.id";
		m1.name="id_count";
		
		Measure m2 = new Measure();
		m2.aggrFunc=null;
		m2.columnExpr="simo_business_request_relation.business_type_id";
		m2.name="b_id";
		
		cube.addMeasure("m2", m2);
		cube.addMeasure("id_count", m1);
		String sql = cube.createAggrSql("1=1", "simo_business_request_relation.business_type_id");
		System.out.println("\ntestCube\n");
		System.out.println(sql);
	}


}
