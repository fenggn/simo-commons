package com.uxsino.authority.lib.annotation;

import java.lang.annotation.*;

/**
 * 权限注解 用于检查权限 规定访问权限
 * @Author: jane
 * @Date:2019/8/30 13:58
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Permission {

    // 必填项 模块页面ID menu.json 中的“id”
    String[] value() default {};

    // 需要校验的权限
    String[] permission() default {};

    // 菜单名称
    String[] text() default {};
}
