package com.uxsino.authority.lib.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 菜单权限
 * @Author: jane
 * @Date: 2019/9/4 15:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuPermission {
    //菜单ID
    private String id;
    // 操作权限
    private String permission;
}
