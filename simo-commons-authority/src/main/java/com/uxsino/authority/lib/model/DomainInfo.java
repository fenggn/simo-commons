package com.uxsino.authority.lib.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 域信息
 * 包含唯一标识和名称
 * @Author: jane
 * @Date: 2019/9/18 17:27
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DomainInfo {
    private Long id;
    private String name;
    private String region;
    private String regionCode;
}
