package com.uxsino.authority.lib.util;

import com.uxsino.authority.lib.handler.AuthorityHandler;
import com.uxsino.authority.lib.model.DomainInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 其他模块获取用户域权限数据集
 * @Author: jane
 * @Date: 2019/9/12 15:43
 */
@Component
public class DomainUtils {

    @Autowired
    private AuthorityHandler authorityHandler;

    public List<Long> getUserDomainIds(HttpSession session) {
        return new ArrayList<>(authorityHandler.getDomainIds(session));
    }

    public List<Long> getUserDomainIds(Long uid) {
        return new ArrayList<>(authorityHandler.getDomainIds(uid));
    }

    public List<DomainInfo> getUserDomainInfos(HttpSession session) {
        return new ArrayList<>(authorityHandler.getUserDepartmentInfos(session));
    }

    public List<DomainInfo> getUserDomainInfos(Long uid) {
        return new ArrayList<>(authorityHandler.getUserDepartmentInfos(uid));
    }

    public List<DomainInfo> getAllDomainInfos() {
        return new ArrayList<>(authorityHandler.getAllDepartmentInfos());
    }

    public DomainInfo getDepartmentInfo(List<DomainInfo> infoList, Long id) {
        if (infoList != null && !infoList.isEmpty()) {
            for (DomainInfo info : infoList) {
                if (info.getId().equals(id)) {
                    return info;
                }
            }
        }
        return null;
    }

    public List<DomainInfo> getDepartmentInfos(List<Long> idIn) {
        List<DomainInfo> list = new ArrayList<>();
        Set<DomainInfo> infoSet = authorityHandler.getAllDepartmentInfos();
        if (infoSet != null && !infoSet.isEmpty()) {
            for (DomainInfo info : infoSet) {
                if (idIn == null || idIn.isEmpty() || idIn.contains(info.getId())) {
                    list.add(info);
                }
            }
        }
        return list;
    }

    public List<DomainInfo> getDepartmentInfos(List<DomainInfo> infoSet, String name) {
        List<DomainInfo> list = new ArrayList<>();
        if (infoSet != null && !infoSet.isEmpty()) {
            for (DomainInfo info : infoSet) {
                if (info.getName().equals(name)) {
                    list.add(info);
                }
            }
        }
        return list;
    }
}
