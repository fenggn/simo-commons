package com.uxsino.simo.selector;

import com.uxsino.commons.logicSelector.PropOperator;
import com.uxsino.commons.logicSelector.SelectorContext;
import com.uxsino.simo.networkentity.EntityClass;
import com.uxsino.simo.networkentity.EntityDomain;
import com.uxsino.simo.networkentity.EntityInfo;

public class EntitySelectorContext extends SelectorContext<EntityInfo> {

    private EntityDomain domain;

    public EntitySelectorContext(EntityDomain domain) {
        super(EntityInfo.class, EntitySelectorContext.class);
        this.domain = domain;

    }

    public static class OpIsa implements PropOperator {

        @Override
        public boolean accept(SelectorContext<?> context, Object object, java.lang.String valueString) {
            assert context.getClass() == EntitySelectorContext.class;
            EntitySelectorContext ctxt = (EntitySelectorContext) context;
            EntityClass src = ctxt.getObject().getEntityClass();
            if (src == null) {
                return false;
            }
            EntityClass dest = ctxt.domain.getEntityClassById(valueString);

            if (dest != null) {
                return ((EntityClass) src).isTypeOf(dest);
            }
            return false;
        }

    }

    static {
        registerContextType(EntitySelectorContext.class);
        registerOperator(EntitySelectorContext.class, "isa", new OpIsa());
    }
}
