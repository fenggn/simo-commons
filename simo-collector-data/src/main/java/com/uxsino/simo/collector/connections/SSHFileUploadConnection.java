package com.uxsino.simo.collector.connections;

import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;
import com.uxsino.simo.connections.exception.SimoConnectionException;
import com.uxsino.simo.connections.exception.SimoQueryException;
import com.uxsino.simo.connections.target.TCPTarget;

public class SSHFileUploadConnection extends AbstractSFTPConnection<TCPTarget> {
    private static Logger logger = LoggerFactory.getLogger(SSHFileUploadConnection.class);

    public SSHFileUploadConnection() {
        super();
    }

    @Override
    public int connect(TCPTarget target) {
        return super.connect(target);
    }

    @Override
    public int supportedFeatures() {
        return F_UPLOAD;
    }

    @Override
    public boolean upload(String remotePath, String fileName, InputStream fileToUpload) {
        if (remotePath != null) {
            try {
                if (!Strings.isNullOrEmpty(remotePath)) {
                    channelSftp.cd(remotePath);
                }
                channelSftp.put(fileToUpload, fileName, ChannelSftp.OVERWRITE);
            } catch (SftpException e) {
                logger.error("error uploading file {} to {}", fileName, remotePath, e);
                return false;
            }

        }
        return true;
    }

    @Override
    public InputStream download(String remotePath, String fileName) throws SimoConnectionException {
        return null;
    }

    @Override
    public Object execCmd(Object cmdPattern) throws SimoQueryException, SimoConnectionException {
        return null;
    }

}
