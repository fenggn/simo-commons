package com.uxsino.simo.collector.connections;

import com.uxsino.simo.connections.exception.SimoConnectionException;
import com.uxsino.simo.connections.target.WMITarget;
import com.uxsino.simo.connections.target.WmiDirTarget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @ClassName DirWmiConnection
 * @Description TODO
 * @Author <a href="mailto:royrxc@gmail.com">Ran</a>
 * @Daate 2019/12/17 16:04
 **/
public class DirWmiConnection extends WMIConnection {
    private Logger LOG = LoggerFactory.getLogger(DirWmiConnection.class);


    @Override
    public int connect(WMITarget target) {
        return super.connect(target);
    }

    @Override
    public Object buildCmd(String cmdPattern, Map<String, String> args) {
        WmiDirTarget target = (WmiDirTarget) this.target;
        String drive = target.drive();
        String path = target.path();

        switch (cmdPattern){
            case "files":
                return super.buildCmd("SELECT * FROM CIM_DataFile WHERE Drive=\""+drive+"\" and Path=\""+path+"\"", args);
            default:
                LOG.warn("cmd not found {}", cmdPattern);
                return null;
        }
    }

    /**
     * support cmd: files
     * @param cmdPattern
     * @return
     * @throws SimoConnectionException
     */
    @Override
    public Object execCmd(Object cmdPattern) throws SimoConnectionException {
        return super.execCmd(cmdPattern);
    }
}
