package com.uxsino.simo.collector.simulate;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class SimulateUtil {

    private static Logger log = LoggerFactory.getLogger(SimulateUtil.class);

    public static boolean snmp = false;

    public static boolean ind = false;

    public static boolean recording = false;

    public static String simulationFilePath = "simo_files\\simulate";

    public static Map<String, Properties> snmpData = new HashMap<>();

    public static Map<String, Object> indData = new HashMap<>();

    public static void recordIndValue(String neclass, String indName, String protocolName, Object value) {
        JSONObject result = new JSONObject();
        result.put("cls", value.getClass().getName());
        result.put("value", JSON.toJSONString(value));
        String dirPaths = simulationFilePath + File.separator + "ind" + File.separator + neclass + File.separator
                + indName;
        File dir = new File(dirPaths);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String filePath = dirPaths + File.separator + protocolName;
        File recordFile = new File(filePath);
        if (!recordFile.exists()) {
            try {
                recordFile.createNewFile();
            } catch (IOException e) {
                log.error("文件{}创建失败!{}", filePath, e.getMessage());
                return;
            }
        }
        FileOutputStream outSTr = null;
        try {
            outSTr = new FileOutputStream(recordFile);
        } catch (FileNotFoundException e) {
            log.error("文件{}不存在!{}", filePath, e.getMessage());
            return;
        }
        BufferedOutputStream buff = new BufferedOutputStream(outSTr);
        try {
            buff.write(result.toJSONString().getBytes());
            buff.flush();
            buff.close();
        } catch (IOException e) {
            log.error("文件{}写入失败!{}", filePath, e.getMessage());
        }
    }

    public static boolean dirExists(String fileName) {
        File flie = new File(fileName);
        if (flie.exists() && flie.isDirectory()) {
            return true;
        }
        return false;
    }

    public static Object simulateInd(String fileName) {
        String jsonStr = null;
        String encoding = "UTF-8";
        File file = new File(fileName);
        if (!file.exists() || !file.isFile()) {
            return null;
        }
        Long filelength = file.length();
        byte[] filecontent = new byte[filelength.intValue()];
        try {
            FileInputStream in = new FileInputStream(file);
            in.read(filecontent);
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            jsonStr = new String(filecontent, encoding);
        } catch (UnsupportedEncodingException e) {
            log.error("The OS does not support {}", encoding);
        }
        if (jsonStr != null) {
            JSONObject json = JSON.parseObject(jsonStr);
            try {
                return JSON.parseObject(json.getString("value"), Class.forName(json.getString("cls")));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
