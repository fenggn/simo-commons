package com.uxsino.simo.collector.connections;

import java.io.File;
import java.util.Map;

import com.uxsino.simo.collector.simulate.SimulateUtil;
import com.uxsino.simo.connections.AbstractConnection;
import com.uxsino.simo.connections.exception.SimoQueryException;
import com.uxsino.simo.connections.target.SimulatedTarget;
import com.uxsino.simo.connections.target.SimulatedTarget.QueryPattern;

/**
 * Make connection to the {@link SimulatedTarget}
 * @author once
 *
 */
public class SimulatedConnection extends AbstractConnection<SimulatedTarget> {

    @Override
    public int connect(SimulatedTarget _target) {
        super.connect(_target);
        connected = false;
        state = 0;
        if (null != _target && _target instanceof SimulatedTarget) {
            target = _target;
            connected = SimulateUtil.dirExists(
                SimulateUtil.simulationFilePath + File.separator + "ind" + File.separator + target.getNeClassId());
        }
        if (connected) {
            state = 1;
        }
        return state;
    }

    @Override
    public Object execCmd(Object cmdPattern) throws SimoQueryException {
        Object result = null;
        QueryPattern cmd = (QueryPattern) cmdPattern;
        result = SimulateUtil.simulateInd(SimulateUtil.simulationFilePath + File.separator + "ind" + File.separator
                + target.getNeClassId() + File.separator + cmd.getInd() + File.separator + cmd.getProtocolName());
        return result;
    }

    @Override
    public Object buildCmd(String cmdPattern, Map<String, String> args) {
        SimulatedTarget.QueryPattern queryPattern = new SimulatedTarget.QueryPattern();
        queryPattern.setProtocolName(target.getProtocolName());
        queryPattern.setInd(args.get("ind"));
        return queryPattern;
    }

    @Override
    public int close() {
        connected = false;
        super.close();
        return state;
    }
}
