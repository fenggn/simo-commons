package com.uxsino.simo.collector.connections;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;

import com.uxsino.simo.connections.exception.SimoConnectionException;
import com.uxsino.simo.connections.exception.SimoQueryException;
import com.uxsino.simo.connections.target.IPMISSHTarget;
import com.uxsino.simo.connections.target.SSHTarget;

public class IPMISConnection extends SSHConnection<IPMISSHTarget> {
    // private static final Logger logger = LoggerFactory.getLogger(IPMISConnection.class);

    private static final String REPLACEMENT = "REPLACEMENT";

    private String commandPrefix;

    public IPMISConnection() {
        super();
    }

    @Override
    public int connect(SSHTarget target) {
        IPMISSHTarget ipmiTarget = (IPMISSHTarget) target;
        if (ipmiTarget.getTargetPassword() == null) {
            commandPrefix = ipmiTarget.commandPrefix;
        } else {
            commandPrefix = ipmiTarget.commandPrefix + " -P " + ipmiTarget.getTargetPassword();
        }
        return super.connect(target);
    }

    @Override
    public Object execCmd(Object cmdPattern) throws SimoQueryException, SimoConnectionException {
        String cmdStr = (String) cmdPattern;
        if (cmdStr.contains(REPLACEMENT)) {
            cmdStr = cmdStr.replaceAll(REPLACEMENT, commandPrefix);
        }
        return super.execCmd(cmdStr);
    }

}
