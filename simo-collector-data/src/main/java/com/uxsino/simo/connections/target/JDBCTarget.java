package com.uxsino.simo.connections.target;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class JDBCTarget extends TCPTarget {
    // private static Logger logger = LoggerFactory.getLogger(TCPTarget.class);

    public String dbName;

    public String connectType;

    public String type;
}
