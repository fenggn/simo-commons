package com.uxsino.simo.connections;

import java.util.Map;

import com.uxsino.simo.connections.target.AbstractTarget;

/**
 * a connection does nothing
 * 
 *
 */
public class NoneConnection extends AbstractConnection<NoneConnection.NoneTarget> {

    public final static class NoneTarget extends AbstractTarget {
        public NoneTarget() {
        }
    }

    @Override
    public int connect(NoneTarget target) {
        super.connect(target);
        connected = true;
        return 0;
    }

    @Override
    public Object execCmd(Object cmd) {
        return null;
    }

    @Override
    public Object buildCmd(String cmdPattern, Map<String, String> args) {
        return cmdPattern;
    }

    @Override
    public int close() {
        connected = false;
        super.close();
        return 0;
    }
}
