package com.uxsino.simo.connections.target;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public class CDPSTarget extends TCPTarget {

    public String type;

    public String mgrIp;

    public String userAcc;

    public String userName;

    public String userEmail;
 
    public String userConcat;

    public String iProtocol;

}
