package com.uxsino.simo.connections;

import java.util.Map;

import com.uxsino.simo.connections.exception.SimoConnectionException;
import com.uxsino.simo.connections.exception.SimoQueryException;

/**
 * 
 * connect/execute to a network entity via various protocols
 *
 * @param <TargetType>
 */
public interface IConnection<TargetType> {

    public String getStateName(int stateValue);

    public void setConnected(boolean connected);

    public boolean isConnected();

    public int getState();

    public int connect(TargetType target);

    /**
     * Takes in the result of {@link buildCmd}, of which type depends on the connection, 
     * get the execution result.
     * .
     * @param cmdPattern
     * @return an instance of {@link ParametrizedResultDto} with the command return as field {@code result}
     *  and the parameter map as field {@code parameters}
     * @throws SimoConnectionException 
     * @throws SimoQueryException 
     */
    public Object execCmd(Object cmdPattern) throws SimoQueryException, SimoConnectionException;

    /**
     * Take the cmd pattern string and pre-process it to conform to the execute format
     *  
     *  
     * @param cmdPattern the command pattern, parameters('%%xxx%%' see {@link QueryParameter}) are replaced
     * before command building
     * @param args
     * @return the formatted object.
     * @throws SimoQueryException 
     */
    public Object buildCmd(String cmdPattern, Map<String, String> args) throws SimoQueryException;

    public int close();

    /**
     * test if the connection with given target is available when Connected(no close)
     * @param cmdString
     * @param resStart
     * @return true if test ok
     */
    public boolean testWithConnected(String cmdString, String resStart);
    
    /**
     * test if the connection with given target is available(connection and close)
     * @param target
     * @param cmdString
     * @param resStart
     * @return
     */
    public boolean connectAndTest(TargetType target, String cmdString, String resStart);
}
