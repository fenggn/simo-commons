package com.uxsino.simo.connections.target;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.uxsino.commons.logicSelector.Prop;
import com.uxsino.commons.utils.DESUtil;
import com.uxsino.commons.utils.config.ConfigProp;

@EqualsAndHashCode(callSuper = true)
@Data
public class IBMMQTarget extends AbstractTarget {
    static Logger logger = LoggerFactory.getLogger(IBMMQTarget.class);

    @ConfigProp(name = "host")
    @Prop(name = "host")
    public String host;

    @ConfigProp(name = "port")
    @Prop(name = "port")
    public int port;

    @ConfigProp(name = "channel")
    @Prop(name = "channel")
    public String channel;

    @ConfigProp(name = "queueManagerName")
    @Prop(name = "queueManagerName")
    public String queueManagerName;

    private String password;

    public Boolean passwordEncrypted = true;

    private String username;

    public String getPassword() {
        try {
            return passwordEncrypted ? DESUtil.decrypt(password) : password;
        } catch (Exception e) {
            logger.error("密码解密失败.", e);
            return null;
        }
    }
}
