package com.uxsino.simo.connections.target;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.uxsino.commons.logicSelector.PropProxy;

public interface ITarget {

    @PropProxy
    default String getProperty(String propName) {
        return null;
    }

    @JsonAnySetter
    default void setProperty(String propName, String value) {
    }

}
