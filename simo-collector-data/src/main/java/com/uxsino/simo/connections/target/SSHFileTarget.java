package com.uxsino.simo.connections.target;

import com.uxsino.commons.logicSelector.Prop;
import com.uxsino.commons.utils.config.ConfigProp;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SSHFileTarget extends TCPTarget {

    public SSHFileTarget() {
    }

    // paths should be separated by ','
    // file path points to the target file
    @ConfigProp(name = "files")
    @Prop(name = "files")
    public String files;

}
