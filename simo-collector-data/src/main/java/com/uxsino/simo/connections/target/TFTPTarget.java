package com.uxsino.simo.connections.target;

public class TFTPTarget extends TCPTarget {
    /**使用的模式*/
    public int model = 1;

    /**远程文件(文件路径)*/
    public String remoteFilePath;

    /**本地保存的文件名*/
    public String localFileName;

}
