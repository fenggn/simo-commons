package com.uxsino.simo.connections.target;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.uxsino.commons.utils.DESUtil;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public class IPMISSHTarget extends SSHTarget {

    private static final Logger logger = LoggerFactory.getLogger(IPMISSHTarget.class);

    private String targetPassword;

    public String commandPrefix;

    public String getTargetPassword() {
        if (targetPassword == null || targetPassword.trim().length() == 0) {
            return null;
        }
        try {
            return passwordEncrypted ? DESUtil.decrypt(targetPassword) : targetPassword;
        } catch (Exception e) {
            logger.error("密码解密失败.", e);
            return null;
        }
    }

    public void setTargetPassword(String pass) {
        this.targetPassword = pass;
    }

}
