package com.uxsino.simo.connections.target;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class JMXTarget extends TCPTarget {

    private String type;

    private String version = "12c";

    private String protocol = "t3";

    private String jndiroot = "/jndi/";

    private String mserver = "weblogic.management.mbeanservers.domainruntime";

    private String protocolProviderPackages = "weblogic.management.remote";
}
