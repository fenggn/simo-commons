package com.uxsino.simo.connections.target;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class WebsphereTarget extends TCPTarget {
    private String deployType;

    private String isSecurity;

    private String appDmgrIp;

    private int appDmgrPort;

    private String keyStorePath;

    private String trustStorePath;

    private String keyStorePassword;

    private String trustStorePassword;
}
