package com.uxsino.simo.connections;

import com.uxsino.commons.model.ITProtocol;
import com.uxsino.commons.model.NeClass;

public class ConnectionTestUtil {

    public static ConnTestModel getSign4ConnectionTest(String protocolName, String classId, String version) {
        if (protocolName.equals(ITProtocol.HTTP.toString().toLowerCase())
                && classId.equals(NeClass.tomcat.toString())) {
            // String tomcatVersion = neObj.getString("version");
            int ver = Integer.parseInt(version);
            if (ver == 5 || ver == 6) {
                return new ConnTestModel("/manager/list", "startsWith:OK - ");
            } else if (ver > 6 && ver < 10) {
                return new ConnTestModel("/manager/text/list", "startsWith:OK - ");
            }
        } else if (protocolName.equals(ITProtocol.HTTP.toString().toLowerCase())
                && classId.equals(NeClass.apache.toString())) {
            return new ConnTestModel("/server-status", "indexOf:Apache Server Status");
        } else if (protocolName.equals(ITProtocol.HTTP.toString().toLowerCase())
                && classId.equals(NeClass.elasticsearch.toString())) {
            return new ConnTestModel("/_nodes/stats/http", "startsWith:{\"_nodes\"");
        } else if (protocolName.equals(ITProtocol.HTTP.toString().toLowerCase())
                && classId.equals(NeClass.nginx.toString())) {
            return new ConnTestModel("/nginx_status", "startsWith:Active connections:");
        } else if (protocolName.equals(ITProtocol.HTTP_CAS.toString().toLowerCase())
                && classId.equals(NeClass.cas.toString())) {
            return new ConnTestModel("{\"relativeURL\":\"/cas/casrs/host/\"}", "indexOf:standalone=\"yes\"") ;
        } else if (protocolName.equals(ITProtocol.REDIS.toString().toLowerCase())) {
            if (classId.equals(NeClass.redis.toString())) {
                return new ConnTestModel("info", "indexOf:# Server");
            } else if (classId.equals(NeClass.redisCluster.toString())) {
                return new ConnTestModel("clusterInfo", "indexOf:cluster_state");
            }
        } else if (protocolName.equals(ITProtocol.SSH.toString().toLowerCase())
                && classId.equals(NeClass.orclRac.toString())) {
            return new ConnTestModel("orclRac:#commandPath#crs_stat -t", "Name");
        } else if (protocolName.equals(NeClass.mongodb.toString())) {
            return new ConnTestModel(
                "{serverStatus: 1, dur: 0, extra_info:0, globalLock:0, locks:0, opLatencies:0, opcounters:0, opcountersRepl:0, rangeDeleter:0, repl:0, security:0, storageEngine:0, wiredTiger:0, writeBacksQueued:0, mem:0, metrics:0, tcmalloc:0}",
                "version");
        } else if (protocolName.equals(ITProtocol.SMIS.toString().toLowerCase())
                && classId.equals(NeClass.diskArray.toString())) {
            return new ConnTestModel(
                "CIM_StoragePool:ElementName,OperationalStatus,Primordial,RemainingManagedSpace,TotalManagedSpace",
                null);
        } else if (protocolName.equals(ITProtocol.SNMP_COMPONENT.toString().toLowerCase())) {
            if (classId.equals(NeClass.iis.toString())) {
                return new ConnTestModel("1.3.6.1.4.1.311.1.7.3.1.18.0", null);
            } else if (classId.equals(NeClass.diskArray.toString())) {
                return new ConnTestModel("1.3.6.1.2.1.1.2.0", null);
            }
        } else if (protocolName.equals(ITProtocol.SNMP.toString().toLowerCase())) {
            return new ConnTestModel("1.3.6.1.2.1.1.2.0", null);
        }
        return new ConnTestModel();
    }
}
