package com.uxsino.simo.connections.exception;

/**
 *  网元机器不能连接的异常
 */
public class SimoConnectionException extends Exception {

    private static final long serialVersionUID = 8171751566805795739L;

    public SimoConnectionException(String message) {
        super(message);
    }

    public SimoConnectionException(Throwable cause) {
        super(cause);
    }
}
