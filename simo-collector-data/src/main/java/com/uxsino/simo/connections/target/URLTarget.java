package com.uxsino.simo.connections.target;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.uxsino.commons.logicSelector.Prop;
import com.uxsino.commons.utils.DESUtil;
import com.uxsino.commons.utils.config.ConfigProp;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public class URLTarget extends AbstractTarget {

    private static Logger logger = LoggerFactory.getLogger(URLTarget.class);

    @ConfigProp(name = "timeout")
    @Prop(name = "timeout")
    private int timeout;

    private String password;

    public Boolean passwordEncrypted = true;

    private String username;

    private String url;

    private String requestType;

    public String getPassword() {
        try {
            return passwordEncrypted ? DESUtil.decrypt(password) : password;
        } catch (Exception e) {
            logger.error("密码解密失败.", e);
            return null;
        }
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setRequestType(String type) {
        this.requestType = type;
    }

    public String getRequestType() {
        return requestType;
    }
}
