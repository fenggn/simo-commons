package com.uxsino.simo.connections.target;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public class FTPTarget extends TCPTarget {

    public String account;

}
