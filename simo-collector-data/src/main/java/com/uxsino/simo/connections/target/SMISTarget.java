package com.uxsino.simo.connections.target;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SMISTarget extends TCPTarget {
    @SuppressWarnings("unused")
    private static Logger logger = LoggerFactory.getLogger(TCPTarget.class);

    private String scheme; // http or https

    private String namespace;

}
