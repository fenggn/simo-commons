package com.uxsino.simo.connections.target;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class VMWareTarget extends TCPTarget {

    private String type;

    private String domain = "";

    private String installPath;

    private String uuid;

    private int retryTimes = 1;
}
