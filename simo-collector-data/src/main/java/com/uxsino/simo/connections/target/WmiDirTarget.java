package com.uxsino.simo.connections.target;

import com.google.common.base.Strings;
import lombok.Data;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 目录监控协议，基于{@link WMITarget}
 * @ClassName DirTarget
 * @Description TODO
 * @Author <a href="mailto:royrxc@gmail.com">Ran</a>
 * @Daate 2019/12/17 15:48
 **/
@Data
public class WmiDirTarget extends WMITarget{
    /**
     * include driver name eg: c:
     * include path special eg: \Programe Files
     * directory necessory
     */
    private String dir;


    public String drive(){
        Matcher matcher = Pattern.compile("[a-z A-Z]:").matcher(this.dir);

        if(matcher.find()){
            return matcher.group();
        }
        return null;
    }

    public String path(){
        Matcher matcher = Pattern.compile("[a-z A-Z]:").matcher(this.dir);
        String path = "\\";
        if(matcher.find()){
            path = matcher.replaceFirst("");
            path = Strings.isNullOrEmpty(path)?"\\":path;
        }
        return path.replaceAll("\\\\", "\\\\\\\\");
    }

    /*public static void main(String[] args) {
        WmiDirTarget target = new WmiDirTarget();
        target.dir = "D:\\src\\";
        System.out.println(target.path());
        System.out.println(target.drive());
    }*/

}
