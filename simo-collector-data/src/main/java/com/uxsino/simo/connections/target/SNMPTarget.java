package com.uxsino.simo.connections.target;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.uxsino.commons.logicSelector.Prop;
import com.uxsino.commons.utils.DESUtil;
import com.uxsino.commons.utils.config.ConfigProp;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SNMPTarget extends TCPTarget {
    private static Logger logger = LoggerFactory.getLogger(SNMPTarget.class);

    private String readCommunity;

    private String writeCommunity;

    private int snmpVersion;

    private String user;

    private int securityLevel;

    private String authProtocol;

    private String authPassword;

    private String privProtocol;

    private String privPassword;

    private String contextEngineId;

    private String runModel;//执行命令方式  cmd 或 snmp4j

    @ConfigProp(name = "timeout")
    @Prop(name = "timeout")
    private int timeout;

    @ConfigProp(name = "retries")
    @Prop(name = "retries")
    private int retries;

    public String getReadCommunity() {
        if (!passwordEncrypted) {
            return readCommunity;
        }
        try {
            return DESUtil.decrypt(readCommunity);
        } catch (Exception e) {
            logger.error("密码解密失败！", e);
            return null;
        }
    }

    public String getWriteCommunity() {
        if (!passwordEncrypted) {
            return writeCommunity;
        }
        try {
            return DESUtil.decrypt(writeCommunity);
        } catch (Exception e) {
            logger.error("密码解密失败！", e);
            return null;
        }
    }

    public String getAuthPassword() {
        if (!passwordEncrypted) {
            return authPassword;
        }
        try {
            return DESUtil.decrypt(authPassword);
        } catch (Exception e) {
            logger.error("密码解密失败！", e);
            return null;
        }
    }

    public String getPrivPassword() {
        if (!passwordEncrypted) {
            return privPassword;
        }
        try {
            return DESUtil.decrypt(privPassword);
        } catch (Exception e) {
            logger.error("密码解密失败！", e);
            return null;
        }
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getRetries() {
        return retries;
    }

    public void setRetries(int retries) {
        this.retries = retries;
    }

}
