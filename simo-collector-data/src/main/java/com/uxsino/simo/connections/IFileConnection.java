package com.uxsino.simo.connections;

import java.io.InputStream;

import com.uxsino.simo.connections.exception.SimoConnectionException;

/**
 * a {@link IConnection} support file transport
 * 
 *
 */
public interface IFileConnection<TargetType> {

    // bit flag for features supported by this connection

    public static final int F_UPLOAD = 0x01;

    public static final int F_DOWNLOAD = 0x02;

    public static final int F_REMOVE = 0x04;

    public static final int F_LIST = 0x08;

    // can set working directory
    public static final int F_WORKING_DIR = 0x10;

    public static final int F_RENAME = 0x20;

    public int supportedFeatures();

    public boolean upload(String remotePath, String fileName, InputStream fileToUpload);

    public InputStream download(String remotePath, String fileName) throws SimoConnectionException;

    /**
     * connect for file transport
     * @param target connection target
     * @return true for success
     */
    public void connectForFileTrans(TargetType target);
}
