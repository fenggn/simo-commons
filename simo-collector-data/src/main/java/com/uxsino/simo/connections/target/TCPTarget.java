package com.uxsino.simo.connections.target;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.uxsino.commons.logicSelector.Prop;
import com.uxsino.commons.utils.DESUtil;
import com.uxsino.commons.utils.config.ConfigProp;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public class TCPTarget extends AbstractTarget {
    static Logger logger = LoggerFactory.getLogger(TCPTarget.class);

    @ConfigProp(name = "host")
    @Prop(name = "host")
    public String host;

    @ConfigProp(name = "port")
    @Prop(name = "port")
    public int port;

    public String type;
    
    public String max_connected_times;
    
    private String password;

    public Boolean passwordEncrypted = true;

    private String username;

    public String getPassword() {
        try {
            return passwordEncrypted ? DESUtil.decrypt(password) : password;
        } catch (Exception e) {
            logger.error("密码解密失败.", e);
            return null;
        }
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

	public String getMax_connected_times() {
		return max_connected_times;
	}

	public void setMax_connected_times(String max_connected_times) {
		this.max_connected_times = max_connected_times;
	}

    
}
