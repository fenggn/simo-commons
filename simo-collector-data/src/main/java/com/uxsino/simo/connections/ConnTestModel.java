package com.uxsino.simo.connections;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConnTestModel {
    private String cmd;

    private String resStart;
}
