package com.uxsino.simo.connections.target;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public abstract class AbstractTarget implements ITarget {

    private Map<String, String> properties;

    public AbstractTarget() {
        properties = new HashMap<>();

    }

    @JsonAnyGetter
    private Map<String, String> getProperties() {
        return properties;
    }

    @Override
    public String getProperty(String propName) {
        return properties.get(propName);
    }

    @Override
    public void setProperty(String propName, String value) {
        properties.put(propName, value);
    }

}
