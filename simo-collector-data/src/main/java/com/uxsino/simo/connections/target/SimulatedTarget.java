package com.uxsino.simo.connections.target;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * This is the wrapper class providing all the simulation data available to the system.
 * It defines the format queries are recorded and provides the mechanism for looping replay.
 * 
 *
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SimulatedTarget extends AbstractTarget {

    private String host;

    private String neClassId;

    private String protocolName;

    /**
     * The wrapper of the protocol and command pair to get query returns
     * 
     */
    @Data
    public static class QueryPattern {
        @JSONField(name = "protocolName", ordinal = 1)
        private String protocolName;

        @JSONField(name = "ind", ordinal = 2)
        private String ind;
    }
}
