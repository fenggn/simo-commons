package com.uxsino.simo.connections.target;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.uxsino.commons.logicSelector.Prop;
import com.uxsino.commons.utils.config.ConfigProp;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class WsphereTarget extends TCPTarget {
    static Logger logger = LoggerFactory.getLogger(WsphereTarget.class);

    @ConfigProp(name = "profileType")
    @Prop(name = "profileType")
    public String profileType;

    @ConfigProp(name = "node")
    @Prop(name = "node")
    public String node;

    @ConfigProp(name = "server")
    @Prop(name = "server")
    public String server;
}
