package com.uxsino.simo.connections.exception;

/**
 *  Collector Query不能正确获取Query的结果抛出的隐藏
 *  其他的异常也统一转换为SimoQueryException异常
 */
public class SimoQueryException extends Exception {

    private static final long serialVersionUID = -841410311434634774L;

    public SimoQueryException(String message) {
        super(message);
    }

    public SimoQueryException(Throwable cause) {
        super(cause);
    }
}
