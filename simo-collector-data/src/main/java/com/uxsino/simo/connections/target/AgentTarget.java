package com.uxsino.simo.connections.target;

/**
 * @ClassName AgentTarget
 * @Description TODO
 * @Author <a href="mailto:royrxc@gmail.com">Ran</a>
 * @Daate 2019/10/25 11:17
 **/
public class AgentTarget extends TCPTarget {
    public enum FETCH_TYPE {
        PUSH,
        FETCH;
    }

    public enum ROUTE_TYPE{
        DIRECT,//直连
        ROUTE;//路由，通过第三方程序递交执行
    }

    private FETCH_TYPE fetchType;

    private ROUTE_TYPE execType;//
    private String routerUrl;//数据路由
    private String routeUser;//路由用户
    private String routePwd;//路由密码
    private String encoding;//编码方式

    private int timeout;// 超时时间，获取数据的超时时间。

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public ROUTE_TYPE getExecType() {
        return execType;
    }

    public void setExecType(ROUTE_TYPE execType) {
        this.execType = execType;
    }

    public String getRouteUser() {
        return routeUser;
    }

    public void setRouteUser(String routeUser) {
        this.routeUser = routeUser;
    }

    public String getRoutePwd() {
        return routePwd;
    }

    public void setRoutePwd(String routePwd) {
        this.routePwd = routePwd;
    }

    public String getRouterUrl() {
        return routerUrl;
    }

    public void setRouterUrl(String routerUrl) {
        this.routerUrl = routerUrl;
    }

    public FETCH_TYPE getFetchType() {
        return fetchType;
    }
    public void setFetchType(FETCH_TYPE fetchType) {
        this.fetchType = fetchType;
    }
}
