package com.uxsino.simo.connections.target;

import com.uxsino.commons.logicSelector.Prop;
import com.uxsino.commons.utils.config.ConfigProp;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * This keeps track of essential information to create and maintain
 * {@link RestfulConnection} to the entity at given url. <br />
 * Each field is commented by the corresponding value in the existing example of
 * 北统三期 transactions.
 * 
 * 
 *
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class HttpTarget extends TCPTarget {

    public static final String HTTPS = "https";

    /**
     * For 北统三期: "https"
     */
    @ConfigProp(name = "connectProtocol")
    @Prop(name = "connectProtocol")
    public String connectProtocol;

    /**
     * For 北统三期: "/goku/rest/v1.5"
     */
    @ConfigProp(name = "basePath")
    @Prop(name = "basePath")
    public String basePath;

    /**
     * For 北统三期: "application/json;charset=utf-8"
     */
    @ConfigProp(name = "contentType")
    @Prop(name = "contentType")
    public String contentType;

    /**
     * For 北统三期: "192.166.154.9"
     *//*
         * @ConfigProp(name = "clientIP")
         * 
         * @Prop(name = "clientIP") public String clientIP;
         */
    /**
     * For 北统三期: "HttpClient"
     */
    @ConfigProp(name = "userAgent")
    @Prop(name = "userAgent")
    public String userAgent;

    /**
     * the authorization token timeout limit in seconds
     */
    @ConfigProp(name = "tokenTimeoutSeconds")
    @Prop(name = "tokenTimeoutSeconds")
    public long tokenTimeoutSeconds = 300;

}
