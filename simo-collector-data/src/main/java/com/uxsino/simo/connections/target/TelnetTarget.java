package com.uxsino.simo.connections.target;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class TelnetTarget extends TCPTarget {
    static Logger logger = LoggerFactory.getLogger(TelnetTarget.class);

    /*
     * The next four items should have default values put outside of the program ?
     *  e.g. in application.properties, via annotations
     */
    /**用户名提示符*/
    private String loginPrompt = "login:";

    /** 密码提示符 */
    private String passwordPrompt = "Password:";

    /** 命令提示符 */
    private String prompt = "$";

    /**登录失败关键字*/
    private String failPrompt = "incorrect";
}
