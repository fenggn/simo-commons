package com.uxsino.simo.connections.target;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class IPMITarget extends TCPTarget {

    private String level;// 特权级别

    private String authtype;// 认证类型

    private String KGValue;// KG值

    private String interf;// 接口类型 lan or lanplus

}
