package com.uxsino.simo.connections.target;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class WMITarget extends TCPTarget {

    private String ip;

    @SuppressWarnings("unused")
    private static Logger logger = LoggerFactory.getLogger(WMITarget.class);

    private String type;

    private String domain = "";
}
