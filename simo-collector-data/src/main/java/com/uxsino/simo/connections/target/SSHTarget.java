package com.uxsino.simo.connections.target;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.uxsino.commons.logicSelector.Prop;
import com.uxsino.commons.utils.config.ConfigProp;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SSHTarget extends TCPTarget {
    @SuppressWarnings("unused")
    private static Logger logger = LoggerFactory.getLogger(TCPTarget.class);

    @ConfigProp(name = "commandPath")
    @Prop(name = "commandPath")
    public String commandPath;

    @ConfigProp(name = "timeout")
    @Prop(name = "timeout")
    private int timeout;
    public SSHTarget() {
    }

}
