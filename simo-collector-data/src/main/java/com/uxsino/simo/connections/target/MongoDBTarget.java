package com.uxsino.simo.connections.target;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.apache.commons.lang3.StringUtils;

@EqualsAndHashCode(callSuper = true)
@Data
public class MongoDBTarget extends TCPTarget {

    private String dbName;

    public String type;

    public String getDbName() {
        if (StringUtils.isEmpty(dbName)) {
            dbName = "admin";
        }
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }
}
