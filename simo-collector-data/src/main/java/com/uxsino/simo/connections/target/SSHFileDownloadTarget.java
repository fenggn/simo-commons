package com.uxsino.simo.connections.target;

import com.uxsino.commons.logicSelector.Prop;
import com.uxsino.commons.utils.config.ConfigProp;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SSHFileDownloadTarget extends TCPTarget {

    public SSHFileDownloadTarget() {
    }

    // paths should be separated by ','
    // number of paths should match number of filenames
    @ConfigProp(name = "filePaths")
    @Prop(name = "filePaths")
    public String filePaths;

    // file names should be separated by ','
    @ConfigProp(name = "fileNames")
    @Prop(name = "fileNames")
    public String fileNames;

}
