package com.uxsino.simo.connections.target;

import com.uxsino.simo.connections.target.AbstractTarget;
import com.uxsino.simo.connections.target.ITarget;

/**
 * 
 * empty target for internal protocols see {@link ITarget}
 *
 */
public class EmptyTarget extends AbstractTarget {

}
