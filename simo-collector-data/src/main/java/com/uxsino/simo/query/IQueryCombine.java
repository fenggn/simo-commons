package com.uxsino.simo.query;

import java.util.List;

import com.uxsino.commons.utils.config.PropElement;
import com.uxsino.reactorq.model.IndicatorValue;
import com.uxsino.simo.indicator.Indicator;
import com.uxsino.simo.utils.ConfigLoadingContext;

/**
 * combine multiple query to one result 
 *
 * @param <ResultIndicatorType>
 * @param <ValueType>
 */
public interface IQueryCombine<ResultIndicatorType extends Indicator, ValueType> extends IQueryGroup {
    ResultIndicatorType getResultIndicator();

    List<QueryTemplate> getQueries();

    void addQuery(QueryTemplate query);

    ValueType apply(QueryContext ctxt, List<IndicatorValue> lists);

    /**
     * load property for the query
     * @param element the PropElement of the configuration
     * @param lctxt configuration loading context to trace loading info
     */
    void loadProp(PropElement element, ConfigLoadingContext lctxt);

}
