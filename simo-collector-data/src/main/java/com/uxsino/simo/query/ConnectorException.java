package com.uxsino.simo.query;

public class ConnectorException extends Exception {

    /**
     * serialVerson id auto generated
     */
    private static final long serialVersionUID = -3365594931486613771L;

    public ConnectorException(String message) {
        super(message);
    }

}
