package com.uxsino.simo.query;

import java.util.concurrent.ConcurrentMap;

import com.uxsino.simo.connections.IConnection;
import com.uxsino.simo.connections.target.ITarget;

public class ConnectionPool {

    @SuppressWarnings("unused")
    private ConcurrentMap<ITarget, IConnection<? extends ITarget>> connections;

    /**
     * get an connection by target
     * @param target
     * @param key
     */
    public IConnection<? extends ITarget> getConnection(ITarget target) {
        return null;
    }

    /**
     * all connections required by a key is no longer needed
     * @param key
     */
    public void closeKey(int key) {

    }
}
