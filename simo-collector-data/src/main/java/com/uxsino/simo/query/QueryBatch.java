package com.uxsino.simo.query;

import java.util.LinkedList;
import java.util.List;

import com.uxsino.simo.indicator.Indicator;
import com.uxsino.simo.networkentity.EntityInfo;

public class QueryBatch {

    private LinkedList<QueryTemplate> queryTemplates = new LinkedList<>();

    public EntityInfo neInfo;

    public String protocolName;

    public QueryBatch(EntityInfo ne, String protocolName) {
        neInfo = ne;
        this.protocolName = protocolName;
    }

    public void addQueryTemplate(QueryTemplate qt) {
        queryTemplates.add(qt);
    }

    // public Iterator<QueryTemplate> getQueryIterator() {
    // return queryTemplates.iterator();
    // }

    // get a shallow copy of query list
    @SuppressWarnings("unchecked")
    public List<QueryTemplate> dumpQueries() {
        return (List<QueryTemplate>) queryTemplates.clone();
    }

    public boolean queriesIndicator(EntityInfo ne, Indicator ind) {

        if (!neInfo.equals(ne))
            return false;
        for (QueryTemplate qt : queryTemplates) {
            if (qt.retractsIndicator(ind))
                return true;
        }
        return false;
    }

    public <T> T getTargetOfProtocol() {
        return ProtocolManager.createTargetFromJson(neInfo.protocols.get(protocolName), protocolName);
    }

    public String getTargetJson() {
        return neInfo.protocols.get(protocolName);
    }
}
