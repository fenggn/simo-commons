package com.uxsino.simo.query;

import java.util.function.Consumer;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.uxsino.commons.utils.debug.SourceTracable;
import com.uxsino.simo.indicator.Indicator;

/*
 * represents a group of queries that will be selected/used together
 */
@JsonInclude(Include.NON_NULL)
public interface IQueryGroup extends SourceTracable {

    public static final String QUERY_TYPE_QUERY = "query";

    public static final String QUERY_TYPE_COMBINE = "combine";

    void foreach(Consumer<QueryTemplate> action);

    /**
     * how many sub query it consists
     * @return
     */
    int count();

    boolean isCustom();

    /**
     * does this query retract given indicator
     * @param indicator to retract
     * @return true if it reatracts given indicator
     */
    default boolean retractsIndicator(Indicator indicator) {
        return retractsIndicator(indicator, null);
    }

    /**
     * does this query retract given indicator
     * @param indicator to retract
     * @return true if it reatracts given indicator
     */
    boolean retractsIndicator(Indicator indicator, String fieldName);

    /**
     * apply action to each indicator this query retracts 
     * @param action
     */
    void forEachIndicator(Consumer<Indicator> action);

    /**
     * type of this query
     * @return
     */
    @JsonProperty("query_type")
    @JSONField(name = "query_type")
    String getQueryType();

    /**
     * default priority of a query. see {@link getPriority}
     */
    public static final int DEFAULT_PRIORITY = 0;

    /**
     * get priority of this query.
     * when more than one query can solve the same indicator, the one with high priority will be chosen first
     * @return
     */
    int getPriority();
}
