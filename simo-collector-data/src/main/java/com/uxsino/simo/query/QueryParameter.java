package com.uxsino.simo.query;

import java.util.function.BiConsumer;

import org.apache.commons.collections4.keyvalue.MultiKey;

import com.uxsino.commons.utils.config.ConfigProp;
import com.uxsino.reactorq.model.INDICATOR_TYPE;
import com.uxsino.simo.networkentity.EntityInfo;

public interface QueryParameter {
    public static final int ESCAPE_NONE = 0;

    public static final int ESCAPE_HTML = 1;

    public static final int ESCAPE_SQL = 2;

    public static final int ESCAPE_URL = 3;

    String getName();

    void setName(String name);

    INDICATOR_TYPE getType();

    // INDICATOR_QUERY_STATUS getQueryStatus(QueryContext ctxt, EntityInfo ne);

    Object getValue(QueryContext ctxt, EntityInfo ne);

    void getV(EntityInfo entity, QueryContext ctxt, BiConsumer<MultiKey<String>, Object> addValue);

    void setNullAs(String s);

    /**
     * in what kind of code should the parameter value be escaped
     * @return
     */
    int getEscaping();

    void setEscaping(int escaping);

    @ConfigProp(name = "escaping")
    default void setEscapingByName(String escapingName) {
        switch (escapingName.toLowerCase()) {
        case "html":
            setEscaping(ESCAPE_HTML);
            break;
        case "sql":
            setEscaping(ESCAPE_SQL);
        case "none":
        default:
            setEscaping(ESCAPE_NONE);
        }
    }

    /***
     * 
     * used in parameter expanding. two parameter with list value will be expanded
     * into Cartesian products, unless they have the same key name. for example:
     * {1,2,3} - {a,b} --> {[1,a],[1,b],[2,a],[2,b],[3,a],[3,b]}
     * if they are marshaled with same key:
     * {[k1,v1],[k2,v2]} - {[k1,w1],[k2,w2]} --> {[k1,v1,w1],[k2,v2,w2]}
     * 
     * @return a string represents the key, null if not grouped.
     */
    String getGroupingKey();

}
