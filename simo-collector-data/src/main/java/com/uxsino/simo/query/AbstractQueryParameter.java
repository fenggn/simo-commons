package com.uxsino.simo.query;

public abstract class AbstractQueryParameter implements QueryParameter {

    private String name;

    protected String nullAs = "";

    private int escaping = ESCAPE_NONE;

    @Override
    public void setNullAs(String s) {

        nullAs = s;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getEscaping() {
        return escaping;
    }

    @Override
    public void setEscaping(int escaping) {
        this.escaping = escaping;
    }
}
