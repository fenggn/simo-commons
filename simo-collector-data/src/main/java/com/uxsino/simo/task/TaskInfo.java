package com.uxsino.simo.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.uxsino.reactorq.model.SCHEDULE_TYPE;

// to define a patrol task
public class TaskInfo {

    public static enum TASK_SOURCE {
                                    PATROL,
                                    POLICY,
                                    ONDEMAND,
                                    OTHER
    }

    @JsonProperty("id")
    @JSONField(name = "id")
    private String taskId;

    public String title;

    @JsonProperty("src")
    @JSONField(name = "src")
    public TASK_SOURCE taskSource;

    // do not create the container to improve performance while in most case
    // there are no arguments for a task.
    private Map<String, String> args = null;

    @JsonProperty("interval")
    @JSONField(name = "interval")
    public long scheduleInterval; // whether FIXED_DELAY or FIXED_RATE

    public String cronExpr; // CRON

    @JsonProperty("type")
    @JSONField(name = "type")
    public SCHEDULE_TYPE scheduleType;

    // whether this task should be running
    public boolean active = false;

    @JsonProperty("release_time")
    @JSONField(name = "release_time")
    public long releaseTime;

    @JsonProperty("items")
    @JSONField(name = "items")
    private List<TaskItem> taskItems;

    public TaskInfo() {
        taskItems = new ArrayList<>();
        scheduleType = SCHEDULE_TYPE.CRON;
    }

    public TaskInfo(TASK_SOURCE taskSource, String taskId) {
        this.taskId = taskId;
        this.taskSource = taskSource;
        scheduleType = SCHEDULE_TYPE.CRON;
        taskItems = new ArrayList<>();
    }

    /**
     * copy constructor
     * @param from
     */
    public TaskInfo(TaskInfo from) {
        this.active = from.active;
        this.cronExpr = from.cronExpr;
        this.releaseTime = from.releaseTime;
        this.scheduleInterval = from.scheduleInterval;
        this.scheduleType = from.scheduleType;
        this.taskId = from.taskId;
        this.taskItems = new ArrayList<>(from.taskItems);
        this.title = from.title;
        this.taskSource = from.taskSource;

        if (from.args != null) {
            this.args = new HashMap<>(from.args);
        }
    }

    public void addTaskItem(TaskItem item) {
        taskItems.add(item);
    }

    public String getTaskId() {
        return taskId;
    }

    public List<TaskItem> getTaskItems() {
        return taskItems;
    }

    public String getArg(String name) {
        if (args == null)
            return null;

        return args.get(name);
    }

    public String getArg(String name, String defaultValue) {
        String s = getArg(name);
        return s == null ? defaultValue : s;
    }

    public void setArg(String name, String value) {
        if (args == null) {
            args = new HashMap<>();
        }

        args.put(name, value);
    }

    public boolean hasItem(String neId, String indName) {
        for (TaskItem item : taskItems) {
            if (item.entityId.equals(neId) && item.indicatorName.equals(indName)){
                return true;
            }
        }
        return false;
    }
}
