package com.uxsino.simo.task;

import java.util.function.Consumer;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.uxsino.reactorq.model.IndicatorValue;

public class TaskRequest {

    @JsonProperty("seq")
    @JSONField(name = "seq")
    public String sequenceId;

    @JsonIgnore
    @JSONField(serialize = false)
    public TaskInfo task;

    public String queryId;

    @JsonIgnore
    @JSONField(serialize = false)
    public Consumer<IndicatorValue> valueAction;

    public Runnable onCompleteAction;

    public TaskRequest(String sequenceId, TaskInfo task, Consumer<IndicatorValue> receiver, Runnable completeAction) {
        this.sequenceId = sequenceId;
        this.task = task;
        this.valueAction = receiver;
        this.onCompleteAction = completeAction;
    }

    @JsonProperty("taskId")
    @JSONField(name = "taskId")
    public String getTaskId() {
        return task.getTaskId();
    }
}
