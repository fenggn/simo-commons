package com.uxsino.simo.task;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class TaskItem {

    @JsonProperty("ne")
    @JSONField(name = "ne")
    public String entityId;

    @JsonProperty("ind")
    @JSONField(name = "ind")
    public String indicatorName;

    public TaskItem() {

    }

    @Override
    public String toString() {
        return entityId + "." + indicatorName;
    }
}
