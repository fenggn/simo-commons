package com.uxsino.simo.task;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.uxsino.commons.logicSelector.SelectorGroup;
import com.uxsino.reactorq.model.PolicyItem;
import com.uxsino.reactorq.model.SCHEDULE_TYPE;
import com.uxsino.simo.networkentity.EntityInfo;

public class PatrolPolicy {

    // Selector for network entity: SelectorGroup<EntityInfo> {@ link
    // SelectorGroup}
    // @JsonIgnore
    @JsonProperty("selector")
    @JSONField(name = "selector")
    private SelectorGroup<EntityInfo> selector;

    public List<PolicyItem> items = new ArrayList<PolicyItem>();

    public void addItem(String indicatorName, SCHEDULE_TYPE scheduleType, long scheduleInterval, String cronExpr) {
        PolicyItem item = new PolicyItem();
        item.cronExpr = cronExpr;
        item.indicatorName = indicatorName;
        item.scheduleInterval = scheduleInterval;
        item.scheduleType = scheduleType;
        items.add(item);
    }

    @JsonIgnore
    @JSONField(serialize = false)
    public SelectorGroup<EntityInfo> getSelector() {
        return selector;
    }

    @JsonIgnore
    @JSONField(serialize = false)
    public void setSelector(SelectorGroup<EntityInfo> selector) {
        this.selector = selector;
    }

}
