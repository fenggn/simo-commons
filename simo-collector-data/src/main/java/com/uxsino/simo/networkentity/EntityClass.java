package com.uxsino.simo.networkentity;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.uxsino.commons.logicSelector.PropProxy;

public class EntityClass {
    private static Logger logger = LoggerFactory.getLogger(EntityClass.class);

    public String id;

    @JsonProperty("release_time")
    @JSONField(name = "release_time")
    public long releaseTime;

    @JsonIgnore
    @JSONField(serialize = false)

    protected EntityClass parent;

    private Map<String, String> properties;

    public EntityClass() {
        properties = new HashMap<>();
    }

    public EntityClass(String classId) {
        this();
        this.id = classId;
    }

    @JsonIgnore
    @JSONField(serialize = false)
    public void setParent(EntityClass parent) {
        if (parent != null && parent.id == id) {
            logger.error("loop in class tree");
            this.parent = null;
        }
        this.parent = parent;
    }

    public EntityClass getParent() {
        return parent;
    }

    @JsonProperty("parent_id")
    @JSONField(name = "parent_id")
    public String getParentId() {
        return parent == null ? null : parent.id;
    }

    public boolean isTypeOf(EntityClass cls) {
        if (id.equals(cls.id))
            return true;
        if (parent != null)
            return parent.isTypeOf(cls);
        return false;
    }

    @JsonAnyGetter
    private Map<String, String> getProperties() {
        return properties;
    }

    @PropProxy
    public String getProperty(String propName) {
        return properties.get(propName);
    }

    @JsonAnySetter
    public void setProperty(String propName, String value) {
        properties.put(propName, value);
    }

    public long getReleaseTime() {
        return releaseTime;
    }
}
