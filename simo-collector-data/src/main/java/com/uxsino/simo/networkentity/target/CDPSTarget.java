package com.uxsino.simo.networkentity.target;

import com.uxsino.simo.connections.target.TCPTarget;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public class CDPSTarget extends TCPTarget {

    public String type;

    public String targetIP;

    public String userAcc;

    public String userName;

    public String userEmail;

    public String userConcat;

    public String iProtocol;

}
