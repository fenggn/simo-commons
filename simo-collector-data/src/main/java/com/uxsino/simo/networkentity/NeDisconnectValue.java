package com.uxsino.simo.networkentity;

import com.uxsino.reactorq.commons.IFlowItem;

public class NeDisconnectValue implements IFlowItem {

    public String entityId;

    public String msg;

    public boolean connected;

    @Override
    public boolean isInternal() {
        return true;
    }

    /*@Override
    public boolean isEnded() {
        return true;
    }*/

    @Override
    public STATUS getStatus() {
        return STATUS.END_ERROR;
    }

}
