package com.uxsino.simo.indicator;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;

import com.uxsino.reactorq.model.IndicatorValue;
import com.uxsino.simo.networkentity.EntityDomain;
import com.uxsino.simo.networkentity.EntityInfo;

public class NSIndicatorDepo {

    private IndicatorNamespace indicatorNamespace;

    private EntityDomain domain;

    private ConcurrentMap<Pair<String, String>, IndicatorValue> values = new ConcurrentHashMap<>();

    public NSIndicatorDepo(IndicatorNamespace ns, EntityDomain domain) {
        this.indicatorNamespace = ns;
        this.domain = domain;
    }

    public IndicatorValue getIndicatorValue(String entityId, Indicator indicator) {
        return getIndicatorValue(entityId, indicator.name);
    }

    public IndicatorValue getIndicatorValue(String entityId, String indicatorName) {
        return values.get(Pair.of(entityId, indicatorName));
    }

    public void setIndicatorValue(IndicatorValue v) {

        values.put(Pair.of(v.entityId, v.indicatorName), v);
    }

    // public List<String> getNetworkEntityIds() {
    // return new ArrayList<String>(neDepos.keySet());
    // }

    public boolean hasIndicatorValue(String entityId, String indicatorName) {
        return values.containsKey(Pair.of(entityId, indicatorName));
    }

    public EntityDomain getDomain() {
        return domain;
    }

    public IndicatorNamespace getIndicatorNamespace() {
        return indicatorNamespace;
    }

    public void clearIndicator(EntityInfo ne, Indicator indicator) {
    }

    public List<IndicatorValue> dumpAllValues() {
        return values.values().stream().collect(Collectors.toList());
    }

    public List<IndicatorValue> dumpNEValues(String entityId) {
        return values.values().stream().filter(v -> v.entityId.equals(entityId)).collect(Collectors.toList());
    }
}
