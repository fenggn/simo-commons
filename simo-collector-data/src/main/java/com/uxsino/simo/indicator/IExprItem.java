package com.uxsino.simo.indicator;

import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface IExprItem {
    void setFormula(String formula);

    String getFormula();

    @JsonIgnore
    Object getCompiled();

    @JsonIgnore

    void setCompiled(Object compiled);

    @JsonIgnore
    void setReferredVariables(@SuppressWarnings("rawtypes") Map<String, Class> refs);

    @SuppressWarnings("rawtypes")
    @JsonIgnore
    Map<String, Class> getReferredVariables();

    @JsonIgnore
    default Set<String> getReferredNames() {
        @SuppressWarnings("rawtypes")
        Map<String, Class> refs = getReferredVariables();

        if (refs == null)
            return null;

        return refs.keySet();
    }

    void disable();

    boolean isDisabled();
}
