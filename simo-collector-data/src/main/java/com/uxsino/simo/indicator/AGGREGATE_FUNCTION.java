package com.uxsino.simo.indicator;

public enum AGGREGATE_FUNCTION {
                                SUM,
                                AVG,
                                MAX,
                                MIN,
                                COUNT,
                                COUNT_ALL, // including null
                                CUSTOM // 自定义
}
