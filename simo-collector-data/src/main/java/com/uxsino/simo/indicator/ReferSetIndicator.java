package com.uxsino.simo.indicator;

import com.uxsino.reactorq.model.INDICATOR_TYPE;
import com.uxsino.simo.networkentity.EntityInfo;
import com.uxsino.simo.query.QueryContext;

/**
 * 
 * a pseudo indicator that triggers all referred indicator to be queried, but no return value for itself.
 * define in config: <refer-set name="xxx" refers="ind_a,ind_b"/>
 *
 */
public class ReferSetIndicator extends IndicatorWithRefer implements INoneQueryIndicator {

    public ReferSetIndicator(String indicatorName) {
        super(indicatorName);
    }

    @Override
    public INDICATOR_TYPE getIndicatorType() {
        return INDICATOR_TYPE.NONE;
    }

    @Override
    public void doPostQuery(EntityInfo entity, QueryContext ctxt, Object value) {

    }

}
