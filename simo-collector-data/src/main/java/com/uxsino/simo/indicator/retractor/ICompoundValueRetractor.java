package com.uxsino.simo.indicator.retractor;

public interface ICompoundValueRetractor {
    boolean retractsField(String fieldName);
}
