package com.uxsino.simo.indicator.retractor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.uxsino.commons.utils.config.ConfigProp;

public class CSVListRetractor extends TextListRetractor {

    @ConfigProp(name = "column_delimeter", trim = false)
    public String columnDelimeter;

    public CSVListRetractor() {
        super();
        columnDelimeter = ",";
    }

    @Override
    protected String[] splitRow(String row) {
        String[] r = row.split(columnDelimeter);
        return r;
    }

    @Override
    protected String getTable(String buf) {
        buf = super.getTable(buf);
        if (" ".equals(columnDelimeter)) {
            Pattern pattern = Pattern.compile("  +");
            Matcher m = pattern.matcher(buf);
            buf = m.replaceAll(" ");
            pattern = Pattern.compile("\n-/\\+");
            m = pattern.matcher(buf);
            buf = m.replaceAll("");
            pattern = Pattern.compile("^ ");
            m = pattern.matcher(buf);
            buf = m.replaceAll("");
            pattern = Pattern.compile("\n ");
            m = pattern.matcher(buf);
            buf = m.replaceAll("\n");
        }
        return buf;
    }

}
