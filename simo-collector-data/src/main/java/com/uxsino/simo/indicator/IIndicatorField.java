package com.uxsino.simo.indicator;

import java.util.Map;

import com.uxsino.commons.utils.config.ConfigProp;
import com.uxsino.reactorq.model.FieldType;

public interface IIndicatorField {

    String getName();

    String getLabel();

    @ConfigProp(name = "label")
    void setLabel(String label);

    FieldType getFieldType();

    @ConfigProp(name = "type")
    void setFieldType(FieldType fieldType);

    String getUnit();

    @ConfigProp(name = "unit")
    void setUnit(String unit);

    String getFormat();

    @ConfigProp(name = "format")
    void setFormat(String format);

    String getWithoutrule();

    @ConfigProp(name = "withoutrule")
    void setWithoutrule(String withoutrule);

    Map<String, String> getDesc();

    @ConfigProp(name = "desc")
    void setDesc(Map<String, String> desc);

    String getTag();

    @ConfigProp(name = "tag")
    void setTag(String tag);

}
