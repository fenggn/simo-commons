package com.uxsino.simo.indicator;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.uxsino.commons.utils.config.ConfigProp;
import com.uxsino.reactorq.model.INDICATOR_TYPE;
import com.uxsino.simo.networkentity.EntityInfo;
import com.uxsino.simo.query.QueryContext;

public class ListIndicator extends CompoundIndicator {

    @ConfigProp(name = "filter", subElement = true)
    @JsonProperty("filter")
    @JSONField(name = "filter")
    private String filterExpression = null;

    public ListIndicator(String indicatorName) {
        super(indicatorName);
    }

    @Override
    @JsonProperty("type")
    @JSONField(name = "type")
    public INDICATOR_TYPE getIndicatorType() {
        return INDICATOR_TYPE.LIST;
    }

    public String getFilterExpression() {
        return filterExpression;
    }

    @Override
    public void doPostQuery(EntityInfo entity, QueryContext ctxt, Object value) {

        @SuppressWarnings("unchecked")
        List<Map<String, Object>> lv = (List<Map<String, Object>>) value;

        if (lv == null) {
            return;
        }

        for (IIndicatorField field : this.getFieldsCollection()) {
            lv.forEach(row -> row.putIfAbsent(field.getName(), null));
        }
    }
}
