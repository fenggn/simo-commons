package com.uxsino.simo.indicator.retractor;

import com.uxsino.reactorq.model.INDICATOR_TYPE;
import com.uxsino.simo.networkentity.EntityInfo;
import com.uxsino.simo.query.QueryContext;
import com.uxsino.simo.query.QueryTemplate;

/**
 * @ClassName SNMPOrigonRetractor
 * @Description TODO
 * @Author <a href="mailto:royrxc@gmail.com">Ran</a>
 * @Daate 2019/11/16 15:20
 **/
public class OriginRetractor extends SimpleValueRetractor {
    public OriginRetractor() {
        super(INDICATOR_TYPE.UNKNOWN);
    }

    @Override
    public Object doRetract(EntityInfo entity, QueryContext ctxt, QueryTemplate qt, Object obj) {
        return obj;
    }
}
