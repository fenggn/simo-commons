package com.uxsino.simo.indicator.retractor;

        import com.uxsino.commons.utils.StringUtils;
        import com.uxsino.commons.utils.config.ConfigProp;

public class TuxedoInfoRetractor extends TextListRetractor {

    @ConfigProp(name = "delimiter", trim = false)
    public String delimiter;

    @ConfigProp(name = "column_begin", trim = false)
    public String columnBegin;

    public TuxedoInfoRetractor() {
        super();
    }

    @Override
    protected String[] splitRow(String row) {
        String[] r = row.split(delimiter);
        return r;
    }

    protected String getTable(String buf) {
        String[] bufs = buf.split("\\n{1,}");
        String str = "";
        Integer num = 0;
        if (StringUtils.isNotBlank(columnBegin)){
            num = Integer.parseInt(columnBegin);
        }
        for (int i = num; i < bufs.length; i++) {
            str += bufs[i].replaceAll("\\(|\\)", "").replaceAll("\\s+", " ") + "\n";
        }
        return str;
    }

}
