package com.uxsino.simo.indicator.retractor;

import com.uxsino.commons.utils.config.ConfigProp;
import com.uxsino.simo.networkentity.EntityInfo;
import com.uxsino.simo.query.QueryContext;
import com.uxsino.simo.query.QueryTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class SNMPListValueRetractor extends ListValueRetractor {
    @ConfigProp(name = "exclude_value")
    private String excludeValue;

    @ConfigProp(name = "max_value")
    private String maxValue;

    @ConfigProp(name = "min_value")
    private String minValue;

    @SuppressWarnings("unchecked")
    @Override
    public Object doRetract(EntityInfo entity, QueryContext ctxt, QueryTemplate qt, Object obj) {

        ArrayList<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
        if (obj == null) {
            return records;
        }
        Map<String, Map<String, String>> map = (Map<String, Map<String, String>>) obj;
        // 严格规定，cmd中的oid才能被使用
        int size = 0;
        ColumnEntry tempEntry = null;
        for (ColumnEntry entry : columnEntries) {
            if (map.containsKey(entry.index)) {
                Map<String, String> indexMap = map.get(entry.index);// field
                                                                    // 对应的oid
                if (indexMap.size() > size) {
                    size = indexMap.size();
                    tempEntry = entry;
                }
            }
        }

        if (tempEntry == null) {
            return records;
        }

        Map<String, String> maxMap = map.get(tempEntry.index);// field 对应的oid
        int valueSize = maxMap.size();
        int index = 0;
        for (Entry<String, String> maxEntry : maxMap.entrySet()) {
            index++;
            String key = maxEntry.getKey().trim();
            String sign = key.replace(tempEntry.index + ".", "");// index
            Map<String, Object> temp = new HashMap<String, Object>();
            boolean emptyValue = true;
            for (ColumnEntry entry : columnEntries) {
                Object value = null;
                if (entry.equals(tempEntry)) {
                    value = tempEntry.retractor.retract(entity, ctxt, qt, maxEntry.getValue());
                } else {
                    if (map.containsKey(entry.index)) {
                        Map<String, String> indexMap = map.get(entry.index);// field
                                                                            // 对应的oid
                        if (indexMap.size() == 1) { // get类型
                            for (Entry<String, String> entry_value : indexMap.entrySet()) {
                                value = entry.retractor.retract(entity, ctxt, qt, entry_value.getValue());
                            }
                        } else {
                            if (entry.index.endsWith(".")) {

                                value = entry.retractor.retract(entity, ctxt, qt, indexMap.get(entry.index + sign));
                            } else {
                                value = entry.retractor.retract(entity, ctxt, qt, indexMap.get(entry.index + "." + sign));
                            }
                        }
                    }
                }
                if (maxValue != null) {
                    try {
                        if (Double.parseDouble(maxValue) < Double.parseDouble(value.toString())) {
                            emptyValue = true;
                            break;
                        }
                    } catch (Exception e) {

                    }
                }
                if (minValue != null) {
                    try {
                        if (Double.parseDouble(minValue) > Double.parseDouble(value.toString())) {
                            emptyValue = true;
                            break;
                        }
                    } catch (Exception e) {

                    }
                }
                if (excludeValue == null || (value != null && !excludeValue.equals(value.toString()))) {
                    emptyValue = false;
                }
                temp.put(entry.field.getName(), value);
            }
            if (temp.isEmpty()) {
                continue;
            }
            if (!emptyValue || (records.isEmpty() && valueSize == index)) {
                // temp = (Map<String, Object>) ctxt.getExprEvaluator().evalExpr(temp, "");
                records.add(temp);
            }
        }
        return records;
    }

}
