// package com.uxsino.simo.indicator.expression.groovy;
//
// import java.util.Map;
//
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
//
// import com.uxsino.simo.indicator.expression.ExprEvaluator;
// import com.uxsino.simo.networkentity.EntityInfo;
//
// import groovy.lang.GroovyShell;
// import groovy.lang.MissingPropertyException;
//
// public class ExprEvaluatorGroovy extends ExprEvaluator {
// private final static Logger logger = LoggerFactory.getLogger(ExprEvaluatorGroovy.class);
//
// private IndicatorBinding binding;
//
// private GroovyShell shell;
//
// public ExprEvaluatorGroovy() {
// binding = new IndicatorBinding();
// shell = new GroovyShell(binding);
// }
//
//
// @Override
// public Object evalExpr( Map<String, Object> currentItem, String expr) {
// binding.setNE(depo);
// binding.setListItem(currentItem);
// Object r = null;
// try {
// r = shell.evaluate(expr);
// } catch (MissingPropertyException e) {
// logger.error("Property {} missing for expression: {}", e.getProperty(), expr);
// } catch (Exception e) {
// logger.error("表达式[" + expr + "]计算失败，数据为：{}.\n{}", currentItem.toString(), e.getMessage());
// }
//
// return r;
//
// }
//
// }
