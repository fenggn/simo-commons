package com.uxsino.simo.indicator;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.uxsino.commons.utils.config.ConfigProp;
import com.uxsino.reactorq.model.FieldType;

public interface ICompound {
    @JsonIgnore
    Iterator<Map.Entry<String, IIndicatorField>> getFieldIterator();

    @JsonIgnore
    int getFieldsCount();

    IIndicatorField getField(String fieldName);

    default boolean hasField(String fieldName) {
        return getField(fieldName) != null;
    }

    @JsonProperty("key")
    @JSONField(name = "key")

    String[] getKeyFieldNames();

    @ConfigProp(name = "key")
    @JsonProperty("key")
    @JSONField(name = "key")
    void setKeyFieldNames(String names);

    @JsonIgnore
    @JSONField(serialize = false)
    Collection<IIndicatorField> getFieldsCollection();

    /**
     * the result may not contain undefined fields. default false
     */
    @JsonProperty("extend_fields")
    @JSONField(name = "extend_fields")
    boolean isFieldsExtendable();

    IIndicatorField createField(String name, FieldType fieldType);
}
