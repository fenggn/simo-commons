package com.uxsino.simo.indicator;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.uxsino.commons.utils.config.ConfigProp;
import com.uxsino.reactorq.model.FieldType;

/**
 * expression field for compound indicator
 * 
 *  
 */
public class ExprField extends IndicatorField implements IExprItem {

    protected String formula;

    @ConfigProp(name = "use-formula-no-value-only")
    public Boolean useFormulaNoValueOnly;

    @JsonIgnore
    protected Indicator indicator;

    public ExprField(Indicator indicator, String fieldName, FieldType fieldType) {
        super(fieldName, fieldType);
        this.indicator = indicator;
    }

    @Override
    public void setFormula(String formula) {
        this.formula = formula;
    }

    @Override
    public String getFormula() {
        return formula;
    }

    @JsonIgnore
    public Indicator getIndicator() {
        return indicator;
    }

    // all fields referred in formula
    @SuppressWarnings("rawtypes")
    @JsonIgnore
    private Map<String, Class> referredFields = null;

    // compiled expression if available. actual type depends on the engine used
    @JsonIgnore
    private Object compiledExpr = null;

    @JsonIgnore
    private boolean exprDisabled = false;

    @Override
    @JsonIgnore
    public Object getCompiled() {
        return compiledExpr;
    }

    @Override
    @JsonIgnore

    public void setCompiled(Object compiled) {
        compiledExpr = compiled;
    }

    @SuppressWarnings("rawtypes")
    @Override
    @JsonIgnore
    public void setReferredVariables(Map<String, Class> refs) {
        this.referredFields = refs;
    }

    @SuppressWarnings("rawtypes")
    @Override
    @JsonIgnore
    public Map<String, Class> getReferredVariables() {
        return referredFields;
    }

    @Override
    public void disable() {
        exprDisabled = true;
    }

    @Override
    public boolean isDisabled() {
        return exprDisabled;
    }
}
