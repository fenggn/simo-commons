package com.uxsino.simo.indicator;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.uxsino.reactorq.model.IndicatorValue;
import com.uxsino.simo.networkentity.EntityInfo;

public class NEIndicatorDepo {

    public EntityInfo entity;

    private IndicatorNamespace indicatorNamespace;

    private ConcurrentMap<String, IndicatorValue> values = new ConcurrentHashMap<>();

    private NSIndicatorDepo globalDepo;

    public NEIndicatorDepo(EntityInfo ne, NSIndicatorDepo globalDepo) {
        this.entity = ne;
        this.globalDepo = globalDepo;
        this.indicatorNamespace = globalDepo.getIndicatorNamespace();
    }

    public NEIndicatorDepo(EntityInfo ne, IndicatorNamespace ns) {
        this.entity = ne;
        this.indicatorNamespace = ns;
    }

    public void setIndicatorValue(IndicatorValue value) {
        values.put(value.indicatorName, value);
        if (null != globalDepo) {
            globalDepo.setIndicatorValue(value);
        }
    }

    public void setIndicatorValue(String indicatorName, IndicatorValue value) {
        values.put(indicatorName, value);
    }

    public IndicatorValue getIndicatorValue(String indicatorName) {
        return values.get(indicatorName);
    }

    public IndicatorValue getIndicatorValue(Indicator indicator) {
        return getIndicatorValue(indicator.name);
    }

    public IndicatorNamespace getNamespace() {
        return indicatorNamespace;
    }

    public boolean hasIndicatorValue(String indicatorName) {

        return values.containsKey(indicatorName);
    }

    public void clearIndicator(Indicator indicator) {
        values.remove(indicator.name);

    }
}
