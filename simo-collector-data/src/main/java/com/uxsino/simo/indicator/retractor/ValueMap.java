package com.uxsino.simo.indicator.retractor;

import java.util.HashMap;
import java.util.Map;

public class ValueMap {

    private Map<String, String> valueMap;

    private String defaultValue;

    public ValueMap() {
        valueMap = new HashMap<>();
    }

    public void put(String key, String value) {
        valueMap.put(key, value);
    }

    public String get(String key) {
        String s = valueMap.get(key);

        return s == null ? defaultValue : s;
    }

    // "key1:value1,ke2:value2" ...
    public void addFromStringList(String expr) {
        if (expr == null)
            return;
        String[] kvs = expr.split(",");

        for (String s : kvs) {
            String[] kv = s.split(":");
            if (kv.length != 2) {
                // TO DO Log something
                continue;
            }
            if (kv[0] == "*") {
                defaultValue = kv[1];
            } else
                put(kv[0], kv[1]);
        }
    }

}
