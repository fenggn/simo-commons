package com.uxsino.simo.indicator.retractor;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class HCNetStructureModel {

    private Integer length;
    private String host;
    private List<Object> structures;
    private boolean[] enables;

}
