package com.uxsino.simo.indicator.retractor;

import java.util.HashMap;

import org.mvel2.MVEL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringExprTransformer {
    private static Logger logger = LoggerFactory.getLogger(StringExprTransformer.class);

    private String expr;

    public StringExprTransformer(String expr) {
        this.expr = expr;
    }

    public Object transForm(Object s) {
        HashMap<String, Object> variables = new HashMap<>();
        variables.put("value", s);

        try {
            Object r = MVEL.eval(expr, variables);
            if (r != null)
                return r;
        } catch (Exception e) {
            logger.error("error evaluating transformer: {}, value=\"{}\"", expr, s);
        }
        return null;
    }

    public String getExpr() {
        return expr;
    }
}
