package com.uxsino.simo.indicator.retractor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.uxsino.commons.utils.config.ConfigProp;

public class RegexRowListRetractor extends TextListRetractor {

    // regex expression to split a record into fields
    private Pattern rowSplitPattern;

    @ConfigProp(name = "row_pattern", required = true, trim = false)
    public void setPattern(String pattern) {
        rowSplitPattern = Pattern.compile(pattern, Pattern.DOTALL);
    }

    @Override
    protected String[] splitRow(String row) {
        if (rowSplitPattern == null) {
            return null;
        }
        Matcher m = rowSplitPattern.matcher(row);
        if (m.find() && m.groupCount() > 0) {
            String[] r = new String[m.groupCount()];
            for (int i = 0; i < m.groupCount(); i++) {
                r[i] = m.group(i + 1);
            }
            return r;
        }
        return null;
    }

}
