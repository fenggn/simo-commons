package com.uxsino.simo.indicator.retractor;

import com.uxsino.commons.utils.config.PropElement;
import com.uxsino.reactorq.model.INDICATOR_TYPE;
import com.uxsino.simo.indicator.Indicator;
import com.uxsino.simo.networkentity.EntityInfo;
import com.uxsino.simo.query.QueryContext;
import com.uxsino.simo.query.QueryTemplate;
import com.uxsino.simo.utils.ConfigLoadingContext;

public interface IValueRetractor {

    public enum EXPRTYPE {
                          PARAMETER_NAME,
                          FORMULA
    };

    Object retract(EntityInfo entity, QueryContext ctxt, QueryTemplate qt, Object obj);

    INDICATOR_TYPE getValueType();

    void addFieldExpr(String fieldName, EXPRTYPE exprType, String expr);

    void postRetract(EntityInfo entity, QueryContext ctxt, QueryTemplate qt, Object obj);

    //Object cast(Object obj);

    /**
     * load property for the Retractor
     * @param ind indicator to retract
     * @param eRetractor the PropElement of the retractor config
     * @param lctxt config loading context to trace loading info
     */
    void loadProp(Indicator ind, PropElement eRetractor, ConfigLoadingContext lctxt);
    // Class<?> getValueClass();
}
