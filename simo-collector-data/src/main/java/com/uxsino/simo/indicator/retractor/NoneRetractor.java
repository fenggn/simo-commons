package com.uxsino.simo.indicator.retractor;

import com.uxsino.commons.utils.config.PropElement;
import com.uxsino.reactorq.model.INDICATOR_TYPE;
import com.uxsino.simo.indicator.Indicator;
import com.uxsino.simo.networkentity.EntityInfo;
import com.uxsino.simo.query.QueryContext;
import com.uxsino.simo.query.QueryTemplate;
import com.uxsino.simo.utils.ConfigLoadingContext;

public class NoneRetractor implements IValueRetractor {

    @Override
    public Object retract(EntityInfo entity, QueryContext ctxt, QueryTemplate qt, Object obj) {
        return null;
    }

    @Override
    public INDICATOR_TYPE getValueType() {
        return INDICATOR_TYPE.NONE;
    }

    @Override
    public void addFieldExpr(String fieldName, EXPRTYPE exprType, String expr) {

    }

//    @Override
//    public Object cast(Object obj) {
//        return obj;
//    }

    @Override
    public void postRetract(EntityInfo entity, QueryContext ctxt, QueryTemplate qt, Object obj) {

    }

    @Override
    public void loadProp(Indicator ind, PropElement eRetractor, ConfigLoadingContext lctxt) {

    }

}
