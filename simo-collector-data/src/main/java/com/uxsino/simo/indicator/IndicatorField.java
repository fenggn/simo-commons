package com.uxsino.simo.indicator;

import java.util.Map;

import com.uxsino.reactorq.model.FieldType;

public class IndicatorField implements IIndicatorField {
    public String name;

    private String label;

    private FieldType fieldType;

    private String unit;

    private String format;

    private String withoutrule;

    private Map<String, String> desc;

    private String tag;

    public IndicatorField(String fieldName, FieldType fieldType) {
        this.name = fieldName;
        this.fieldType = fieldType;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public FieldType getFieldType() {
        return fieldType;
    }

    @Override
    public void setFieldType(FieldType fieldType) {
        this.fieldType = fieldType;
    }

    @Override
    public String getUnit() {
        return unit;
    }

    @Override
    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String getFormat() {
        return format;
    }

    @Override
    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getWithoutrule() {
        return withoutrule;
    }

    @Override
    public void setWithoutrule(String withoutrule) {
        this.withoutrule = withoutrule;
    }

    @Override
    public Map<String, String> getDesc() {
        return desc;
    }

    @Override
    public void setDesc(Map<String, String> desc) {
        this.desc = desc;
    }

    @Override
    public String getTag() {
        return tag;
    }

    @Override
    public void setTag(String tag) {
        this.tag = tag;
    }

}
