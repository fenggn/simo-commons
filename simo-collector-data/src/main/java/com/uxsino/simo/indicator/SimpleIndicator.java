package com.uxsino.simo.indicator;

import com.uxsino.reactorq.model.INDICATOR_TYPE;
import com.uxsino.simo.networkentity.EntityInfo;
import com.uxsino.simo.query.QueryContext;

public class SimpleIndicator extends Indicator {
    private INDICATOR_TYPE type;

    public SimpleIndicator(String indicatorName, INDICATOR_TYPE indicatorType) {
        super(indicatorName);
        type = indicatorType;
    }

    @Override
    public INDICATOR_TYPE getIndicatorType() {
        return type;
    }

    @Override
    public void doPostQuery(EntityInfo entity, QueryContext ctxt, Object value) {

    }

}
