package com.uxsino.simo.indicator.retractor;

import com.uxsino.reactorq.model.INDICATOR_TYPE;

public class ValueRetractorFactory {

    private static IValueRetractor createPassThroughValueRetractor(INDICATOR_TYPE valueType) {
        switch (valueType) {
        case COMPOUND:
            return new PassThroughCompoundValueRetractor();
        case LIST:
            return new PassThroughListValueRectractor();
        default:
            return new SimpleValueRetractor(valueType);
        }
    }

    private static IValueRetractor createScriptValueRetractor(INDICATOR_TYPE valueType) {
        switch (valueType) {
        case COMPOUND:
            return new ScriptCompoundValueRetractor();
        case LIST:
            return new ScriptListValueRetractor();
        default:
            return new SimpleValueRetractor(valueType);
        }
    }

    public static IValueRetractor createValueRetractor(String retreiverType, INDICATOR_TYPE valueType)
        throws Exception {
        if (retreiverType == null) {
            return new NoneRetractor();
        }
        // TO DO use some registration mechanism
        switch (retreiverType) {
	        case "rac":
		        return new OracleRacRetractor();
        case "regex":
            return new RegexValueRetractor(valueType);
        case "compound":
            return new CompoundValueRetractor();
        case "csv_title":
            return new CSVListTitleRetractor();
        case "csv2":
            return new CSVListRetractor2();
        case "csv":
            return new CSVListRetractor();
        case "csv3":
            return new CSVListRetractor3();
        case "csv_reverse":
            return new CSVReverseRangeRetractor();
        case "fixedwidth":
            return new FixedWidthListRetractor();
        case "regex_table":
            return new RegexRowListRetractor();
        case "table":
            return new DatasetListRetractor();
        case "dataset":
            return new DatasetRetractor();
        case "json_array_no":
            return new JSONArrayNoRetractor();
        case "json_array":
            return new JSONArrayValueRetractor();
        case "map_array":
            return new SNMPListValueRetractor();
        case "snmp_map_sum_compound":
            return new SNMPMapSumValueRetractor();
        case "snmp_map_avg_compound":
            return new SNMPMapAvgValueRetractor();
        case "snmp_map_max_compound":
            return new SNMPMapMaxValueRetractor();
        case "map_simple":
            return new SNMPSimpleValueRetractor(valueType);
        case "sql_kv":
            return new DatasetKVRetractor();
        case "json_string":
            return new JSONStringValueRetractor();
        case "json_string_array":
            return new JSONStringArrayRetractor();
        case "redis_text":
            return new RedisKVRetractor();
        case "tomcat_vm_info":
            return new TomcatVmInfoRetractor();
        case "tomcat_mem_pool":
            return new TomcatMemPoolRetracor();
        case "tomcat_OS_info":
            return new TomcatOSInfoRetractor();
        case "line_list":
            return new LineListRetractor();
        case "json_list":
            return new JSONListRetractor();
        case "xml_list":
            return new XMLRetractor();
        // case "empty_entry_table":
        // return new EmptyEntryTableRetractor();
        case "snmp_vlan":
            return new SNMPVLANRetractor();
        case "snmp_fdb_list":
            return new SNMPFDBListRetractor();
        case "snmp_vlan_cisco":
            return new SNMPVLANCiscoRetractor();
        case "passthrough":
            return createPassThroughValueRetractor(valueType);
        case "script":
            return createScriptValueRetractor(valueType);
        case "simple":
            return new SimpleValueRetractor(valueType);
        case "hcnet":
            return new HCNetStructureRetractor();
        case "tuxedo":
            return new TuxedoInfoRetractor();
        case "origin":
            return new OriginRetractor();
        case "asis":
        case "":
            return valueType == INDICATOR_TYPE.COMPOUND ? new CompoundValueRetractor()
                    : new SimpleValueRetractor(valueType);
        }
        throw new Exception("error creating retractor for: " + retreiverType);

    }
}
