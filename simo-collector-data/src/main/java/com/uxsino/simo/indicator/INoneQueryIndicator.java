package com.uxsino.simo.indicator;

/**
 * 
 * an interface to mark an indicator to be evaluated after connection based query
 * like expression, refer-set etc.
 *
 */
public interface INoneQueryIndicator {

}
