package com.uxsino.simo.indicator.retractor;

import com.uxsino.commons.utils.config.ConfigProp;

public class CSVListRetractor2 extends CSVListRetractor {
    @ConfigProp(name = "row_combine")
    public int rowCombine;

    public CSVListRetractor2() {
        super();
        rowCombine = 1;
    }

    @Override
    protected String[] getLines(String s) {
        int combine = rowCombine;
        // try {
        // combine = Integer.parseInt(rowCombine);
        // } catch (NumberFormatException e) {
        // logger.error("parse row_combine error: {}", e.getMessage());
        // return null;
        // }
        String[] strArr = s.split(rowDelimeter);
        int len = strArr.length / combine;
        if (strArr.length % combine != 0) {
            len++;
        }
        String[] result = new String[len];
        int index = 0;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < len; i++) {
            sb.setLength(0);
            for (int j = 0; j < combine && index < strArr.length; j++) {
                sb.append(strArr[index++]);
                sb.append(columnDelimeter);
            }
            result[i] = sb.toString();
        }
        return result;
    }
}
