// package com.uxsino.simo.indicator.expression.groovy;
//
// import java.util.Map;
//
// import com.uxsino.simo.indicator.Indicator;
// import com.uxsino.simo.indicator.NEIndicatorDepo;
// import com.uxsino.simo.networkentity.EntityInfo;
//
// import groovy.lang.Binding;
//
// public class IndicatorBinding extends Binding {
// // private static Logger logger =
// // LoggerFactory.getLogger(IndicatorBinding.class);
//
// private EntityInfo currentNE;
//
// private NEIndicatorDepo currentNEDepo;
//
// // for expr field calculate
// private Map<String, Object> currentListItem;
//
// public IndicatorBinding() {
// }
//
// public void setNE(NEIndicatorDepo neDepo) {
// currentListItem = null;
// currentNE = neDepo.entity;
// currentNEDepo = neDepo;
// }
//
// public EntityInfo getCurrentNE() {
// return currentNE;
// }
//
// public void setListItem(Map<String, Object> item) {
// currentListItem = item;
// }
//
// @Override
// public Object getVariable(String name) {
// if (currentListItem != null && currentListItem.containsKey(name)) {
// return currentListItem.get(name);
// }
//
// if (currentNEDepo == null)
// return null;
// Indicator ind = currentNEDepo.getNamespace().getIndicator(name);
// if (ind != null) {
// return currentNEDepo.getIndicatorValue(name);
// }
// return super.getVariable(name);
// }
// }
