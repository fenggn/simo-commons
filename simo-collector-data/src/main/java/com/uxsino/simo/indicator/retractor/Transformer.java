package com.uxsino.simo.indicator.retractor;

import org.mvel2.MVEL;

import java.util.HashMap;

/**
 * @ClassName Transformer
 * @Description TODO
 * @Author <a href="mailto:royrxc@gmail.com">Ran</a>
 * @Daate 2019/9/11 16:52
 **/
public interface Transformer {
    public <T> T transForm(Object s);
}
