package com.uxsino.simo.indicator.retractor;

import com.uxsino.simo.networkentity.EntityInfo;
import com.uxsino.simo.query.QueryContext;
import com.uxsino.simo.query.QueryTemplate;

/**
 * 
 * a retractor returns result of {@link IConnection} as List
 *
 */
public class PassThroughListValueRectractor extends ListValueRetractor {

    @Override
    public Object doRetract(EntityInfo entity, QueryContext ctxt, QueryTemplate qt, Object obj) {
        return obj;
    }

}
