package com.uxsino.simo.indicator;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import com.uxsino.reactorq.model.IndicatorValue;


/**
 * aggregate a list indicator value to compound indicator value
 * a wrapper of {@link ListAggregateFunction}
 *
 *
 */
public class IndicatorValueAggregateFunction implements Function<IndicatorValue, IndicatorValue> {

    private String resultIndicatorName;

    private ListAggregateFunction innerFunc = new ListAggregateFunction();

    public IndicatorValueAggregateFunction(String resultIndicatorName) {
        this.resultIndicatorName = resultIndicatorName;
    }

    /**
     * add a field to aggregate on
     * @param srcField field name in source indicator value
     * @param resultField field name in result indicator value
     * @param func aggregate type see {@link AGGREGATE_FUNCTION}
     */
    public void addField(String srcField, String resultField, AGGREGATE_FUNCTION func) {

        innerFunc.addField(srcField, resultField, func);
    }

    @SuppressWarnings("unchecked")
    @Override
    public IndicatorValue apply(IndicatorValue v) {

        IndicatorValue r = new IndicatorValue();
        r.entityId = v.entityId;
        r.indicatorName = resultIndicatorName;
        r.queryTimeMillis = v.queryTimeMillis;
        r.sequenceId = v.sequenceId;
        r.taskId = v.taskId;
        //r.task = v.task;

        r.value = innerFunc.apply((List<Map<String, Object>>) v.value);
        return r;
    }

}
