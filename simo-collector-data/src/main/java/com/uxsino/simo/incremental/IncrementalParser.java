package com.uxsino.simo.incremental;

import com.uxsino.reactorq.model.IndicatorValue;

public interface IncrementalParser {
    void apply(IndicatorValue oldValue, IndicatorValue value);
}
