package com.uxsino.simo.incremental;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.uxsino.reactorq.model.IndicatorValue;
import com.uxsino.simo.incremental.parser.IfEntryParser;
import com.uxsino.simo.incremental.parser.MemoryParser;
import com.uxsino.simo.incremental.parser.ProcessParser;
import com.uxsino.simo.incremental.parser.TPSParser;

public class IncrementalUtil {
    private static Map<String, IncrementalParser> parser = new HashMap<String, IncrementalParser>();

    private static ConcurrentHashMap<String, IndicatorValue> incrementalCache = new ConcurrentHashMap<>();

    private static ConcurrentHashMap<String, Double> memTotalCache = new ConcurrentHashMap<>();
    static {
        parser.put("if_entry", new IfEntryParser());
        parser.put("process", new ProcessParser());
        parser.put("memory", new MemoryParser());
        parser.put("tps",new TPSParser());
    }

    public static void incrementalValue(String parserKey, String cacheKey, IndicatorValue value) {
        IncrementalParser currParser = parser.get(parserKey);
        currParser.apply(incrementalCache.get(cacheKey), value);
        incrementalCache.put(cacheKey, value);
    }

    public static void putMemTotalCache(String key, Double value) {
        if (key == null || value == null) {
            return;
        }
        memTotalCache.put(key, value);
    }

    public static Double getMemTotalCache(String key) {
        return memTotalCache.get(key);
    }
}
