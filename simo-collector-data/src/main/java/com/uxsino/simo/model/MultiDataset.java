package com.uxsino.simo.model;


import java.util.ArrayList;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

@Data
public class MultiDataset {
    @JSONField(name = "datasets")
    private ArrayList<Dataset> datasets = new ArrayList<>();

    public int size() {
        return datasets.size();
    }

    public Dataset get(int index) {
        return datasets.get(index);
    }

    public void add(Dataset ds) {
        datasets.add(ds);
    }

}
