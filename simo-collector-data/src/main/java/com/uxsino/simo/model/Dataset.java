package com.uxsino.simo.model;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

@Data
public class Dataset {
    @JSONField(name = "columnName", ordinal = 1)
    public Map<String, Integer> columnNames;

    @JSONField(name = "records", ordinal = 2)
    public List<String[]> records;

    public Dataset() {
        columnNames = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        records = new ArrayList<>();
    }

}
