package com.uxsino.rule.model;

public enum AppId {
                   RULEENGINE("规则管理"),
                   ALERT("告警管理"),
                   OTHER("其他系统");

    private String text;

    AppId(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

}
