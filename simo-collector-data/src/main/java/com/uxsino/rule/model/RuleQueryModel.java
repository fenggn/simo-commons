package com.uxsino.rule.model;

import lombok.Data;

/**
 * 规则查询model
 * 
 *
 */
@Data
public class RuleQueryModel {

    private String beginDate;

    private String endDate;

    private String jpql = "from Rule where 1=1 ";

    private String countjpql = "select count(id) from Rule where 1=1";

    private String createCondition() {
        String conditon = "";
        if (beginDate != null) {
            conditon += and("operationDate >= " + beginDate);
        }

        if (endDate != null) {
            conditon += and("operationDate <= " + endDate);
        }

        return conditon;
    }

    public String createJPQL() {
        return jpql + createCondition();
    }

    public String createCountJPQL() {
        return countjpql + createCondition();
    }

    public String and(String str) {
        str = " and" + str;
        return str;
    }

}
