
package com.uxsino.commons.logicSelector;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;

import org.junit.Test;

import com.uxsino.simo.networkentity.EntityClass;
import com.uxsino.simo.networkentity.EntityDomain;
import com.uxsino.simo.networkentity.EntityInfo;
import com.uxsino.simo.selector.EntitySelectorContext;

public class PropSelectorTest {

	@Test
	public void test() {
		SelectorGroup<EntityInfo> g = new SelectorGroup<>(GROUPBY_TYPE.All);
		PropSelector<EntityInfo> p = new PropSelector<>("version", "in", "v1,v3,v5");

		g.add(p);

		EntityClass cls = new EntityClass();

		cls.id = "class_a";

		EntityInfo entity = new EntityInfo(cls);
		entity.id = "test_entity_1";
		entity.protocols = new HashMap<>();
		entity.setEntityClass(cls);
		entity.setProperty("version", "v1");

		// SelectorFactory<EntityInfo> factory = new SelectorFactory<>();
		// SelectorContext< EntityInfo> = new SelectorContext<>(EntityInfo.class, thisClass)

		EntityDomain domain = new EntityDomain();
		domain.addEntity(entity);

		EntitySelectorContext ctxt = new EntitySelectorContext(domain);

		ctxt.setObject(entity);
		assertTrue(g.accept(ctxt));
		entity.setProperty("version", "v33");
		assertFalse(g.accept(ctxt));

		// fail("Not yet implemented");
	}

}
