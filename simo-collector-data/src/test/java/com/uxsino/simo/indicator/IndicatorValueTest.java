package com.uxsino.simo.indicator;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uxsino.reactorq.model.IndicatorValue;

public class IndicatorValueTest {

	@Test
	public void testEqualityOfFastJsonAndJackson() throws IOException {
		IndicatorValue v = new IndicatorValue();
		v.entityId = "123";
		v.indicatorName = "abc";
		v.value = "hello";
		v.sequenceId = "w3rfepsd;f";
		v.queryTimeMillis = System.currentTimeMillis();
		JSONObject j = (JSONObject) JSONObject.toJSON(v);

		ObjectMapper mapper = new ObjectMapper();
		IndicatorValue v2 = mapper.readValue(j.toJSONString(), IndicatorValue.class);
		JSONObject j2 = (JSONObject) JSONObject.toJSON(v2);
		assertEquals(j, j2);
	}

}
