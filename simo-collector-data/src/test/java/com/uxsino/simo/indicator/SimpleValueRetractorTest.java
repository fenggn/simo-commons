package com.uxsino.simo.indicator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.uxsino.reactorq.model.INDICATOR_TYPE;
import com.uxsino.simo.indicator.retractor.SimpleValueRetractor;
import com.uxsino.simo.indicator.retractor.StringExprTransformer;

public class SimpleValueRetractorTest {

	@Test
	public void testTransForm() {

		StringExprTransformer transformer = new StringExprTransformer("\"__\"+value.toUpperCase()+\"__\"");
		Object r = transformer.transForm("abC");
		assertEquals("__ABC__", r);
	}

	@Test
	public void testTrim() {
		SimpleValueRetractor retractor = new SimpleValueRetractor(INDICATOR_TYPE.STRING);
		retractor.trimResultString = true;

		String r = (String) retractor.convertValue(" \ta b c\n");

		assertEquals("a b c", r);
	}

	@Test
	public void testMap() {
		SimpleValueRetractor retractor = new SimpleValueRetractor(INDICATOR_TYPE.STRING);
		retractor.setMap("doe:smith,dark:black");

		String r = (String) retractor.convertValue("doe");

		assertEquals("smith", r);

		r = (String) retractor.convertValue("xyz");

		assertEquals("xyz", r);
	}
}
