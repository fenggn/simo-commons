package com.uxsino.simo;

import static org.junit.Assert.assertNotNull;

import java.io.File;

import org.junit.Test;

import com.uxsino.commons.utils.config.FileResourceWalker;
import com.uxsino.simo.indicator.IndicatorNamespace;

public class IndicatorTest {

	IndicatorNamespace ns = new IndicatorNamespace();

	public IndicatorTest() {

		String path = (new File("src/test/resources/test_config/indicators")).getAbsolutePath();
		FileResourceWalker fw = new FileResourceWalker(path, ".xml");
		ns.loadIndicators(fw);
	}

	@Test
	public void testIndicatorLoad() {
		assertNotNull(ns.getIndicator("xxx_list"));
	}

}
