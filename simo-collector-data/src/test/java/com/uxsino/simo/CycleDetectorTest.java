package com.uxsino.simo;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.uxsino.commons.utils.CycleDetector;

public class CycleDetectorTest {

	private static class Node<T> {
		public T key;

		public List<T> dests;

		@SafeVarargs
		public Node(T key, T... dests) {
			this.key = key;
			this.dests = Arrays.asList(dests);
		}
	}

	@Test
	public void testDetect() {

		Map<Integer, Node<Integer>> graph = new HashMap<>();
		graph.put(1, new Node<Integer>(1, 3, 5, 7, 9));
		graph.put(2, new Node<Integer>(2, 4, 6));
		graph.put(7, new Node<Integer>(7, 3, 5, 9));

		CycleDetector<Integer> detector = new CycleDetector<>(key -> {
			Node<Integer> node = graph.get(key);
			return node == null ? null : node.dests.iterator();
		});

		List<Integer> circle = null;
		for (Node<Integer> node : graph.values()) {
			circle = detector.detect(node.key);
			if (circle != null)
				break;
		}
		assertNull("" + circle, circle);

		detector.reset();
		graph.clear();
		graph.put(5, new Node<Integer>(5, 2));
		graph.put(2, new Node<Integer>(2, 3));
		graph.put(3, new Node<Integer>(3, 5));

		for (Node<Integer> node : graph.values()) {
			circle = detector.detect(node.key);
			if (circle != null)
				break;
		}
		assertNotNull("" + circle, circle);

	}

}
