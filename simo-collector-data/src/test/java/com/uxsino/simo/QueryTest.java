package com.uxsino.simo;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

import com.uxsino.commons.utils.config.FileResourceWalker;
import com.uxsino.simo.indicator.IndicatorNamespace;
import com.uxsino.simo.networkentity.EntityClass;
import com.uxsino.simo.networkentity.EntityDomain;
import com.uxsino.simo.networkentity.EntityInfo;
import com.uxsino.simo.query.IQueryGroup;
import com.uxsino.simo.query.Querier;

public class QueryTest {

	IndicatorNamespace ns = new IndicatorNamespace();

	EntityDomain domain = new EntityDomain();

	Querier querier = new Querier(domain, ns);

	public QueryTest() {

		String path = (new File("src/test/resources/test_config/indicators")).getAbsolutePath();
		FileResourceWalker fw = new FileResourceWalker(path, ".xml");
		ns.loadIndicators(fw);

		path = (new File("src/test/resources/test_config/ne-selectors")).getAbsolutePath();
		fw = new FileResourceWalker(path, "*.xml");
		domain.loadSelectors(fw);

		path = (new File("src/test/resources/test_config/query-templates")).getAbsolutePath();
		fw = new FileResourceWalker(path, ".xml");
		querier.loadQueryTemplates(fw);

	}

	@Test
	public void testQueryLoad() {
		assertTrue(querier.getQueryCount() > 0);
	}

	@Test
	public void testPriority() {
		EntityClass cls = new EntityClass();
		cls.id = "linux";
		cls.setProperty("os", "ubuntu");
		domain.addEntityClass(cls);

		EntityInfo entity = new EntityInfo(cls);
		entity.id = "ne_1";
		domain.addEntity(entity);
		IQueryGroup[] queries = querier.getFeasibleQueries(entity, ns.getIndicator("xxx_list"));

		assertTrue(queries.length == 2);
		assertTrue(queries[0].getPriority() >= queries[1].getPriority());
	}

}
