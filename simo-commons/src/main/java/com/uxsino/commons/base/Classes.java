package com.uxsino.commons.base;

import java.util.Collection;

/**
 * @ClassName Classes
 * @Description TODO
 * @Author <a href="mailto:royrxc@gmail.com">Ran</a>
 * @Daate 2019/12/11 11:47
 **/
public class Classes {
    /**
     * 是否是原始类型
     * @param obj
     * @return
     */
    public static boolean isPrimitive(Object obj){
        if (obj == null){
            return false;
        }
        return isPrimitive(obj.getClass());
    }

    public static boolean isPrimitive(Class cls){
        if (cls == null){
            return false;
        }
        return cls.isPrimitive();
    }

    /**
     * 简单类型，包括 boolean、char、byte、short、int、long、float、double, 和对应的包装类 Boolean、Character、Byte、Short、Integer、Long、Float、Double
     * @return
     */
    public static boolean isSimple(Object obj){
        if(obj == null){
            return false;
        }
        return isSimple(obj.getClass());
    }

    /**
     * 简单类型，包括 boolean、char、byte、short、int、long、float、double, 和对应的包装类 Boolean、Character、Byte、Short、Integer、Long、Float、Double
     * @return
     */
    public static boolean isSimple(Class cls){
        if (cls == null){
            return false;
        }

        boolean bol = Boolean.class.isAssignableFrom(cls)
                || Character.class.isAssignableFrom(cls)
                || Byte.class.isAssignableFrom(cls)
                || Short.class.isAssignableFrom(cls)
                || Integer.class.isAssignableFrom(cls)
                || Long.class.isAssignableFrom(cls)
                || Float.class.isAssignableFrom(cls)
                || Double.class.isAssignableFrom(cls)
                ;
        return isPrimitive(cls)||bol;
    }

    public static boolean isCollection(Object obj){
        if(obj ==null){
            return false;
        }
        return isCollection(obj.getClass());
    }

    public static boolean isCollection(Class cls){
        if(cls == null){
            return false;
        }
        return Collection.class.isAssignableFrom(cls);
    }

    public static boolean isArray(Object obj){
        if(obj ==null){
            return false;
        }
        return isArray(obj.getClass());
    }

    public static boolean isArray(Class cls){
        if(cls ==null){
            return false;
        }
        return cls.isArray();
    }

}
