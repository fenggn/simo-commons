package com.uxsino.commons.model;

/**
 * @description 告警通知方式枚举对象
 * 
 * @date 2017年4月13日
 */
public enum AlertNotifyWay {
                            // SYSTEM(0,"系统"), // 发送系统消息在程序界面上闪烁
                            MAIL(1,"邮件"),      // 通过邮件提醒
                            SMS(2,"短信"),      // 通过手机短信提醒
                            VOICE(3,"声音"),   // 可以声音提醒，非登录状态下也能进行播放，采用HTML5实现
                            CAROUSEL(4,"轮播"), // 系统右上方通过走马灯的形势提醒
                            POPUP(5,"弹窗"),
                            MP(7,"微信"),
                            MSG(6,"消息中心"); // 消息中心站内信
    private int value;

    private String text;

    AlertNotifyWay(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return this.value;
    }

    public String getText() {
        return text;
    }

    /**
     * 将传入的告警通知方式枚举值转化为枚举对象
     * @param value 传入的告警通知方式枚举值
     * @return 告警通知方式枚举对象
     */
    public static AlertNotifyWay fromValue(int value) {
        for (AlertNotifyWay item : AlertNotifyWay.values()) {
            if (item.value == value) {
                return item;
            }
        }
        throw new IllegalArgumentException("0-3 is a range of parameter('value')");
    }

    /**
     * 将传入的告警通知方式中文显示转化为枚举对象
     * @param value 传入的告警通知方式中文显示
     * @return 告警通知方式枚举对象
     */
    public static AlertNotifyWay fromText(String text) {
        for (AlertNotifyWay item : AlertNotifyWay.values()) {
            if (item.text.equals(text)) {
                return item;
            }
        }
        throw new IllegalArgumentException();
    }

}
