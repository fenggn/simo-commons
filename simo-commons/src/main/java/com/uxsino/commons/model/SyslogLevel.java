package com.uxsino.commons.model;

public enum SyslogLevel {

                         // 0 Emergency: system is unusable
                         //
                         // 1 Alert: action must be taken immediately
                         //
                         // 2 Critical: critical conditions
                         //
                         // 3 Error: error conditions
                         //
                         // 4 Warning: warning conditions
                         //
                         // 5 Notice: normal but significant condition
                         //
                         // 6 Informational: informational messages
                         //
                         // 7 Debug: debug-level messages

                         Emergency(0,"紧急"),
                         Alert(1,"报警"),
                         Critical(2,"关键"),
                         Error(3,"错误"),
                         Warning(4,"警告"),
                         Notice(5,"通知"),
                         Informational(6,"消息"),
                         Debug(7,"调试")

    ;

    private String text;

    private int value;

    private SyslogLevel(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public int getValue() {
        return value;
    }

    public static SyslogLevel getByValue(int value) {
        for (SyslogLevel level : SyslogLevel.values()) {
            if (level.value == value) {
                return level;
            }
        }
        return null;
    }

}
