package com.uxsino.commons.model;

/**
 * @description 启用状态枚举对象
 * 
 * @date 2017年7月7日
 */
public enum LockStatus {
                        OPEN(1,"开启"),
                        CLOSED(0,"关闭");
    private int value;

    private String text;

    LockStatus(int value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 将传入的启用状态枚举值转化为枚举对象.
     * @param value 传入的启用状态枚举值
     * @return 启用状态枚举对象
     */
    public static LockStatus fromValue(int value) {
        for (LockStatus item : LockStatus.values()) {
            if (item.value == value) {
                return item;
            }
        }
        throw new IllegalArgumentException("0-1 is a range of parameter('value')");
    }

    /**
     * 将传入的启用状态中文显示转化为枚举对象.
     * @param value 传入的启用状态中文显示
     * @return 启用状态枚举对象
     */
    public static LockStatus fromText(String text) {
        for (LockStatus item : LockStatus.values()) {
            if (item.text.equals(text)) {
                return item;
            }
        }
        throw new IllegalArgumentException();
    }

    public int getValue() {
        return this.value;
    }

    public String getText() {
        return text;
    }

}
