package com.uxsino.commons.model;

/**
 * An instance is HEALTHY if a heartbeat is received in the most recent
 * verification
 * 
 * is STRUGGLING if the most recent verification is unable to get the heartbeat
 * 
 * is INVALID if unable to get heartbeat for at least {@code allowedElapse} many
 * most recent verifications
 * 
 * 
 *
 */
public enum HeartbeatStatus {
                             HEALTHY,
                             STRUGGLING,
                             INVALID,
                             UNKNOWN
}
