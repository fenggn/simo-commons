package com.uxsino.commons.model;

/**
 * @description 日志来源枚举对象
 * 
 * @date 2017年7月25日
 */
public enum LogSource {
                       CUSTOMER_DB(1,"数据库"),           // 客户方数据库
                       RECEIVE_INTERFACE(2,"接收接口"),   // 为客户方提供日志接收接口
                       QUERY_INTERFACE(3,"查询接口");     // 客户方提供日志查询接口
    private int value;

    private String text;

    LogSource(int value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 将传入的日志来源枚举值转化为枚举对象.
     * @param value 传入的日志来源枚举值
     * @return 日志来源枚举对象
     */
    public static LogSource fromValue(int value) {
        for (LogSource item : LogSource.values()) {
            if (item.value == value) {
                return item;
            }
        }
        throw new IllegalArgumentException("1-3 is a range of parameter('value')");
    }

    /**
     * 将传入的日志来源中文显示转化为枚举对象.
     * @param value 传入的日志来源中文显示
     * @return 日志来源枚举对象
     */
    public static LogSource fromText(String text) {
        for (LogSource item : LogSource.values()) {
            if (item.text.equals(text)) {
                return item;
            }
        }
        throw new IllegalArgumentException();
    }

    public int getValue() {
        return this.value;
    }

    public String getText() {
        return text;
    }

}
