package com.uxsino.commons.model;

public enum RunStatus {
                       Unknown(0,"未监控"),
                       Loading(1,"检测中"),    // 正常检测数据
                       Good(2,"正常"),    // 状态正常
                       Warning(3,"告警"),  // 状态异常
                       Unconnection(4,"失联");    // 无法连接

    /*运行状态的枚举值*/
    private int value;

    /*运行状态的枚举名称*/
    private String name;

    /**
     * 
     * 创建一个 ManageStatus 对象实例.
     * 
     * @param value
     */
    private RunStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    /**
     * 
     * 由枚举值生成枚举对象
     * 
     * @param value
     * @return
     */
    public static RunStatus fromValue(int value) {
        for (RunStatus runStatus : RunStatus.values()) {
            if (runStatus.value == value) {
                return runStatus;
            }
        }
        throw new IllegalArgumentException("0-3 is a range of parameter('value')");
    }

    /**
     * 
     * 取得枚举对象的值
     * 
     * @return
     */
    public int getValue() {
        return this.value;
    }

    public String getName() {
        return this.name;
    }

}
