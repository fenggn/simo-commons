package com.uxsino.commons.model;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * **********************************************************
 * 管理对象支持的登录方式的枚举对象
 * 
 * 
 * @version iSoc Service Platform, 2015-3-5
 ***********************************************************
 */
public enum ITProtocol {

    SNMP(1,true),
    TELNET(2,true),
    SSH(3,true),
    // Rlogin(4),
    // VNC(5),
    // RDP(6),
    // SPICE(7),
    JDBC(8,true),
    JMX(9,true),
    SMIS(10,true),
    WMI(12,true),
    /*TFTP(13),*/
    SDK(14,false),
    REDIS(15,true),
    MONGODB(16,true),
    HTTP(17,true),
    HTTPS(18,false),
    MEMCACHED(19,true),
    WEBSPHERE(20,true),
    SFTP(21,false),
    SNMP_COMPONENT(22,true),
    IBMMQ(23,true),
    SSH_DOWNLOAD(24,true),
    ACTIVEMQ(25,true),
    URL(26,true),
    IPMI(27,true),
    FTP(28,true),
    CDPS_AGENT(29,true),
    HCNET(30,true),
    SFTP_FILE(31,false),
    HTTP_CAS(32,true),
    TONGWEB(33,true);




    /*登录方式的枚举值*/
    private int value;

    private boolean discovery;

    /**
     * 
     * 创建一个 LoginType 对象实例.
     * 
     * @param value
     */
    private ITProtocol(int value, boolean discovery) {
        this.value = value;
        this.discovery = discovery;
    }

    /**
     * 
     * 由枚举值生成枚举对象
     * 
     * @param value
     * @return
     */
    public static ITProtocol fromValue(int value) {
        for (ITProtocol ITProtocol : ITProtocol.values()) {
            if (ITProtocol.value == value) {
                return ITProtocol;
            }
        }
        throw new IllegalArgumentException("0-5 is a range of parameter('value')");
    }

    public static ITProtocol fromName(String name) {
        for (ITProtocol protocol : ITProtocol.values()) {
            if (name.compareToIgnoreCase(protocol.name()) == 0) {
                return protocol;
            }
        }
        return null;
    }

    /**
     * 
     * 取得枚举对象的值
     * 
     * @return
     */
    public int getValue() {
        return this.value;
    }

    public boolean allowDiscovery() {
        return this.discovery;
    }

    public static JSONArray toJson() {
        JSONArray array = new JSONArray();
        for (ITProtocol protocol : ITProtocol.values()) {
            JSONObject json = new JSONObject();
            json.put("name", protocol.toString());
            json.put("value", protocol.getValue());
            json.put("discovery", protocol.discovery);
            array.add(json);
        }
        return array;
    }
}
