package com.uxsino.commons.model;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.uxsino.commons.baseclass.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * **********************************************************
 * 操作系统的枚举对象
 * 
 * 
 * @version iSoc Service Platform, 2015-5-6
 ***********************************************************
 */
public enum NeClass {
    windows("Windows",BaseNeClass.host,"snmp,wmi,sftp,cdps_agent",Constant.BNS_HOST_INDS),//,agent
    linux("Linux",BaseNeClass.host,"snmp,ssh,telnet,sftp,cdps_agent",Constant.BNS_HOST_INDS),//,agent
    aix("Aix",BaseNeClass.host,null,Constant.BNS_HOST_INDS),//,agent
    hpux("HPUX",BaseNeClass.host,null,Constant.BNS_HOST_INDS),
    solaris("Solaris",BaseNeClass.host,null,Constant.BNS_HOST_INDS),
    unix("其他UNIX",BaseNeClass.host,null,Constant.BNS_HOST_INDS),//,agent
    //android("Android", BaseNeClass.host, "agent", null),

    switches("交换机",BaseNeClass.network,null,Constant.BNS_NETWORK_INDS),
    router("路由器",BaseNeClass.network,null,Constant.BNS_NETWORK_INDS),
    ap("无线接入器",BaseNeClass.network,null,Constant.BNS_NETWORK_INDS),
    ac("无线控制器",BaseNeClass.network,null,Constant.BNS_NETWORK_INDS),
    firewall("防火墙",BaseNeClass.network,null,Constant.BNS_NETWORK_INDS),
    gap("网闸",BaseNeClass.network,null,Constant.BNS_NETWORK_INDS),
    fibre_channel_switch("光纤交换机",BaseNeClass.network,null,Constant.BNS_NETWORK_INDS),
    gateway("网关",BaseNeClass.network,null,Constant.BNS_NETWORK_INDS),
    vpn("VPN",BaseNeClass.network,null,Constant.BNS_NETWORK_INDS),
    other_network("其他网络设备",BaseNeClass.network,null,Constant.BNS_NETWORK_INDS),

    diskArray("磁盘阵列",BaseNeClass.storage,"smis,snmp",null),
    dawn("曙光存储",BaseNeClass.storage,"snmp",null),
    ceph("Ceph文件系统", BaseNeClass.storage,"ssh",null),
    // tapeLibrary("磁带库",BaseNeClass.storage,null,null), //目前还没有任何支持的指标，暂时去掉

    f5("F5",BaseNeClass.loadBalancing,null,null),
    citrix("Citrix",BaseNeClass.loadBalancing,null,null),
    array("Array",BaseNeClass.loadBalancing,null,null),
    sangfor("Sangfor",BaseNeClass.loadBalancing,null,null),
    radware("Radware",BaseNeClass.loadBalancing,null,null),
    venustech("Venustech",BaseNeClass.loadBalancing,null,null),

    nbu("NetBackUp",BaseNeClass.backup,null,null),

    oracle("Oracle",BaseNeClass.database,"jdbc,sftp_file",Constant.BNS_ORACLE_INDS),
    mysql("MySql",BaseNeClass.database,null,null),
    sybase("Sybase",BaseNeClass.database,null,null),
    sqlServer("SQL Server",BaseNeClass.database,null,null),
    db2("DB2",BaseNeClass.database,"jdbc",Constant.BNS_DB2_INDS),
    postgre("PostgreSQL",BaseNeClass.database,null,null),
    uxdb("uxsinoDB",BaseNeClass.database,null,null),
    cache("cacheDB",BaseNeClass.database,null,null),
    dm("达梦数据库",BaseNeClass.database,null,null),
    mongodb("MongoDB",BaseNeClass.database,"mongodb",null),
    redis("Redis",BaseNeClass.database,"redis",null),
    redisCluster("RedisCluster",BaseNeClass.rac,"redis",null),

    orclRac("Oracle Rac",BaseNeClass.rac,null,null),
    mongodbCluster("MongoDBCluster",BaseNeClass.rac,"mongodb",null),

    activeMQ("ActiveMQ",BaseNeClass.middleware,"activemq",null),
    weblogic("Weblogic",BaseNeClass.middleware,null,null),
    tomcat("Tomcat",BaseNeClass.middleware,"http",null),
    tongweb("TongWeb",BaseNeClass.middleware,"tongweb",null),
    tuxedo("Tuxedo", BaseNeClass.middleware,"ssh",null),
    apache("Apache",BaseNeClass.middleware,"http",null),
    iis("IIS",BaseNeClass.middleware,"snmp_component",null),
    memcached("Memcached",BaseNeClass.middleware,"memcached",null),
    websphere("Websphere",BaseNeClass.middleware,"websphere",null),
    elasticsearch("ElasticSearch",BaseNeClass.middleware,"http",null),
    ibmmq("IBM MQ",BaseNeClass.middleware,"ibmmq",null),
    nginx("NGINX",BaseNeClass.middleware,"http,ssh_download",null),
    tlq("TLQ",BaseNeClass.middleware,"ssh",null),
    nodejs("NodeJS",BaseNeClass.middleware,"ssh",null),

    vmware("VMware",BaseNeClass.virtualization,null,null),
    xen("Xen",BaseNeClass.virtualization,"ssh",null),
    kvm("KVM",BaseNeClass.virtualization,"ssh",null),
    esxi("esxi主机",BaseNeClass.virtualization, null, null),
    cas("cas",BaseNeClass.virtualization, "http_cas", null),
    uxcloud("优炫云",BaseNeClass.virtualization,"ssh",null),

    taiji("太极云",BaseNeClass.cloud, null, null),

    printer("打印机",BaseNeClass.other,"snmp",null),
    copier("复印机",BaseNeClass.other,"snmp",null),
    hcnet("海康威视", BaseNeClass.video, "hcnet,snmp", null),

    service("应用服务",BaseNeClass.app,"url",Constant.BNS_SERVICE_INDS),
    ftp("FTP",BaseNeClass.app,"ftp",null),

    ipmiServer("IPMI 服务器",BaseNeClass.server,"ipmi",null),
    unknow("其他snmp设备",BaseNeClass.unknow,"snmp",null);
    /*枚举名称*/
    private String text;

    private BaseNeClass baseClass;

    private String protocols;

    /**
     * 业务所需的资源指标列表
     */
    private List<String> bnsNeInds;

    /**
     * 创建一个 网元类型枚举对象.
     */
    private NeClass(String text, BaseNeClass baseClass, String protocols, List<String> bnsNeInds) {
        this.text = text;
        this.baseClass = baseClass;
        this.protocols = protocols;
        this.bnsNeInds = bnsNeInds;
    }

    public static NeClass fromText(String text) {
        for (NeClass neClass : NeClass.values()) {
            if (neClass.text.equals(text)) {
                return neClass;
            }
        }
        throw new IllegalArgumentException("无此枚举项:" + text);
    }

    public String getText() {
        return this.text;
    }

    public BaseNeClass getBaseNeClass() {
        return baseClass;
    }

    public JSONObject getProtocols() {
        JSONObject protocolsJson = new JSONObject();
        if (this.protocols == null) {
            protocolsJson = this.getBaseNeClass().getProtocols();
        } else {
            for (String protocol : this.protocols.split(",")) {
                protocolsJson.put(protocol, "unconfigured");
            }
        }
        return protocolsJson;
    }

    public List<String> getBnsNeInds() {
        return bnsNeInds;
    }

    public boolean isHardware() {
        return this.getBaseNeClass().isHardware();
    }

    public static String getHardware() {
        String hardware = "";
        for (NeClass neClass : NeClass.values()) {
            if (neClass.isHardware()) {
                hardware += ",'" + neClass.toString() + "'";
            }
        }
        if (hardware.length() != 0) {
            hardware = hardware.substring(1);
        }
        return hardware;
    }

    public static List<BaseNeClass> getBaseNeClass(String protocol) {
        List<BaseNeClass> baseClassList = new ArrayList<>();
        for (NeClass neclass : NeClass.values()) {
            JSONObject protocolJson = neclass.getProtocols();
            if (protocolJson.containsKey(protocol.toLowerCase()) && !baseClassList.contains(neclass.baseClass)) {
                baseClassList.add(neclass.baseClass);
            }
        }
        return baseClassList;
    }

    public static List<String> getBnsNeClsList() {
        List<String> neClsList = new ArrayList<>();
        for (NeClass neclass : NeClass.values()) {
            if (neclass.getBnsNeInds() != null) {
                neClsList.add(neclass.toString());
            }
        }
        return neClsList;
    }

    public static List<String> getBnsNeIndList() {
        List<String> indList = new ArrayList<>();
        for (NeClass neclass : NeClass.values()) {
            if (neclass.getBnsNeInds() != null) {
                indList.addAll(neclass.getBnsNeInds());
            }
        }
        return indList;
    }
    
    public static List<NeClass> getNoIndNeClsList() {
        return Lists.newArrayList(NeClass.citrix, NeClass.array, NeClass.sangfor, NeClass.radware, NeClass.venustech,
            NeClass.copier);
    }

}
