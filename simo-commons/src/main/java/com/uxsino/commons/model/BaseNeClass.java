package com.uxsino.commons.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.uxsino.commons.utils.StringUtils;

/**
 * **********************************************************
 * 管理对象的枚举对象
 * 
 * 
 * @version iSoc Service Platform, 2015-3-5
 ***********************************************************
 */
public enum BaseNeClass {
                         server("服务器",true,"ipmi"),
                         host("主机",true,"snmp,ssh,telnet,sftp"),
                         network("网络设备",true,"snmp,sftp"),
                         storage("存储",true,"smis,snmp"),
                         loadBalancing("负载均衡",true,"snmp"),
                         backup("备份系统",false,"ssh"),
                         database("数据库",false,"jdbc"),
                         rac("数据库集群",false,"ssh"),
                         middleware("中间件",false,"jmx"),
                         app("应用",false,null),
                         virtualization("虚拟化平台",true,"sdk"),
                         cloud("云平台",true,"http"),
                         video("视频设备",true, null),
                         other("其他设备",true, null),
                         unknow("未知",true, null);
    /*管理对象类型的*/
    private String text;

    private Boolean hardware;

    private String protocols;

    /**
     * 创建一个 BaseNeClass 对象实例.
     */
    private BaseNeClass(String text, Boolean hardware, String protocols) {
        this.text = text;
        this.hardware = hardware;
        this.protocols = protocols;
    }

    /**
     * 
     * 由枚举值生成枚举对象
     * 
     * @param value
     * @return
     */
    public static BaseNeClass fromText(String text) {
        for (BaseNeClass baseClass : BaseNeClass.values()) {
            if (baseClass.text.equals(text)) {
                return baseClass;
            }
        }
        throw new IllegalArgumentException("无法识别的类型：" + text);
    }

    /**
     * 
     * 取得枚举对象的名称
     * 
     * @return
     */

    public String getText() {
        return this.text;
    }

    public Boolean isHardware() {
        return this.hardware;
    }

    public String getDefaultProtocol() {
        String[] protocols = this.protocols.split(",");
        if (protocols.length == 0) {
            return null;
        }
        return protocols[0];
    }

    public JSONObject getProtocols() {
        JSONObject protocolsJson = new JSONObject();
        if (this.protocols != null) {
            for (String protocol : this.protocols.split(",")) {
                protocolsJson.put(protocol, "unconfigured");
            }
        }
        return protocolsJson;
    }

    public List<NeClass> getNeClass() {
        List<NeClass> list = new ArrayList<>();
        for (NeClass neClass : NeClass.values()) {
            if (neClass.getBaseNeClass().equals(this)) {
                list.add(neClass);
            }
        }
        return list;
    }

    public List<NeClass> getNeClass(String protocol) {
        List<NeClass> list = new ArrayList<>();
        for (NeClass neClass : NeClass.values()) {
            if (StringUtils.isNotEmpty(protocol) && neClass.getBaseNeClass().equals(this)
                    && neClass.getProtocols().containsKey(protocol.toLowerCase())) {
                list.add(neClass);
            }
        }
        return list;
    }

    public String getNeClassString() {
        String neClasses = "";
        for (NeClass neClass : NeClass.values()) {
            if (neClass.getBaseNeClass().equals(this)) {
                neClasses += ",'" + neClass.toString() + "'";
            }
        }
        if (neClasses.length() != 0) {
            neClasses = neClasses.substring(1);
        }
        return neClasses;
    }

    public static List<BaseNeClass> getBaseClass(boolean hardOrSoft) {
        List<BaseNeClass> datas = Arrays.asList(BaseNeClass.values()).stream()
            .filter(c -> c.isHardware().equals(hardOrSoft)).collect(Collectors.toList());
        return Lists.newArrayList(datas);
    }

    public static List<NeClass> getNeClass(boolean hardOrSoft) {
        List<NeClass> datas = Lists.newArrayList();
        getBaseClass(hardOrSoft).forEach(b -> {
            datas.addAll(b.getNeClass());
        });
        return datas;
    }

    public static List<NeClass> getNeClassByProtocols(String... protocols) {
        List<NeClass> datas = Lists.newArrayList();
        for (NeClass cls : NeClass.values()) {
            for (String p : protocols) {
                if (cls.getProtocols().containsKey(p)) {
                    datas.add(cls);
                    continue;
                }
            }
        }
        return datas;
    }

    public static boolean contains(String str) {
        for (BaseNeClass baseClass : BaseNeClass.values()) {
            if (baseClass.toString().equals(str)) {
                return true;
            }
        }
        return false;
    }
}
