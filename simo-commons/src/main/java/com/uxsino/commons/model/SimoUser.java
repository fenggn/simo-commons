package com.uxsino.commons.model;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * 扩展原生spring userdetails 中的user对象
 * 
 * 
 *
 */
public class SimoUser extends User {

    private Long accountId;

    private boolean notPermit; // 不允许登录

    private boolean notDeleted; // 账号是否已经删除

    private boolean notStopped; // 账号是否已经禁用

    private boolean tooManyAccount; // 同名账号不止一个

    private static final long serialVersionUID = -8534586335931392026L;

    public SimoUser(Long id, String username, String password, boolean enabled, boolean accountNonExpired,
        boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.accountId = id;
    }

    public SimoUser(Long id, String username, String password, boolean enabled, boolean accountNonExpired,
        boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities,
        boolean notPermit, boolean notDeleted, boolean notStopped, boolean tooManyAccount) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.accountId = id;
        this.notPermit = notPermit;
        this.notDeleted = notDeleted;
        this.notStopped = notStopped;
        this.tooManyAccount = tooManyAccount;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public boolean isNotPermit() {
        return notPermit;
    }

    public void setNotPermit(boolean notPermit) {
        this.notPermit = notPermit;
    }

    public boolean isNotDeleted() {
        return notDeleted;
    }

    public void setNotDeleted(boolean notDeleted) {
        this.notDeleted = notDeleted;
    }

    public boolean isNotStopped() {
        return notStopped;
    }

    public void setNotStopped(boolean notStopped) {
        this.notStopped = notStopped;
    }

    public boolean isTooManyAccount() {
        return tooManyAccount;
    }

    public void setTooManyAccount(boolean tooManyAccount) {
        this.tooManyAccount = tooManyAccount;
    }

}
