package com.uxsino.commons.model;

public enum DataSourceType {
    all,
    mine,
    share;
}
