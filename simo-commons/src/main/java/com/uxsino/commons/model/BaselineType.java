package com.uxsino.commons.model;

/**
 * @description 基线类型的枚举对象
 * 
 * @date 2017年7月7日
 */
public enum BaselineType {
                          Bilateral(1,"双侧基线"),
                          Upward(2,"上基线"),
                          Downward(3,"下基线");
    /**
     * 基线类型的枚举值
     */
    private int value;

    /**
     * 基线类型的枚举名称
     */
    private String name;

    private BaselineType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    /**
     * 
     * 由枚举值生成枚举对象
     * 
     * @param value
     * @return
     */
    public static BaselineType fromValue(int value) {
        for (BaselineType manageStatus : BaselineType.values()) {
            if (manageStatus.value == value) {
                return manageStatus;
            }
        }
        throw new IllegalArgumentException("1-3 is a range of parameter('value')");
    }

    public int getValue() {
        return this.value;
    }

    public String getName() {
        return this.name;
    }
}
