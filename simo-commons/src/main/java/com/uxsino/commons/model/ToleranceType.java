package com.uxsino.commons.model;

/**
 * @description 容忍度类型的枚举对象
 * 
 * @date 2017年7月7日
 */
public enum ToleranceType {
                           No_Tolerance(1,"无容忍度"),        // 基线
                           Absolute(2,"绝对容忍度"),          // 容量临界值
                           Relative(3,"相对容忍度");          // 基于基线的浮动比例%
    /**
     * 容忍度类型的枚举值
     */
    private int value;

    /**
     * 容忍度类型的枚举名称
     */
    private String name;

    private ToleranceType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    /**
     * 
     * 由枚举值生成枚举对象
     * 
     * @param value
     * @return
     */
    public static ToleranceType fromValue(int value) {
        for (ToleranceType manageStatus : ToleranceType.values()) {
            if (manageStatus.value == value) {
                return manageStatus;
            }
        }
        throw new IllegalArgumentException("1-3 is a range of parameter('value')");
    }

    public int getValue() {
        return this.value;
    }

    public String getName() {
        return this.name;
    }
}
