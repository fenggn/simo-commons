package com.uxsino.commons.model;

/**
 * @description 请求状态枚举类
 *
 * @date 2017年6月21日
 */
public enum TaskTimeType {
                          InfoNotify(0,"消息通知"),
                          PasswordNotify(1,"口令维护"),
                          DateCleanTask(2,"数据清理");

    /**
     * 请求状态的枚举值
     */
    private int value;

    /**
     * 请求状态的枚举名称
     */
    private String name;

    private TaskTimeType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return this.value;
    }

    public String getName() {
        return this.name;
    }

    /**
     * 由枚举值生成枚举对象
     * @param value 枚举值
     * @return 枚举对象
     */
    public static TaskTimeType fromValue(int value) {
        for (TaskTimeType runStatus : TaskTimeType.values()) {
            if (runStatus.value == value) {
                return runStatus;
            }
        }
        throw new IllegalArgumentException("0-1 is the range of the parameter('value')");
    }

}
