package com.uxsino.commons.model;

import lombok.Data;

@Data
public class TreeBean {
    private String id;

    private String name;

    private boolean leaf;

    private String image;

    private boolean mo;

}
