package com.uxsino.commons.model;

/**
 * **********************************************************
 * 接口的状态
 * 
 * 
 * @version iSoc Service Platform, 2015-3-5
 ***********************************************************
 */
public enum InterfaceStatus {
                             up(1,"正常"),
                             down(2,"故障"),
                             testing(3,"测试"),
                             unknown(4,"未知"),
                             dormant(5,"休眠"),
                             notPresent(6,"硬件缺失"),
                             lowerLayerDown(7,"低层不可用"),
                             // breakdown(8,"产生故障"),
                             notUsed(8,"未使用");
    /*接口状态的枚举值*/
    private int value;

    /*接口状态的枚举名称*/
    private String name;

    /**
     * 
     * 创建一个 InterfaceStatus 对象实例.
     * 
     * @param value
     */
    private InterfaceStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    /**
     * 
     * 由枚举值生成枚举对象
     * 
     * @param value
     * @return
     */
    public static InterfaceStatus fromValue(int value) {
        for (InterfaceStatus manageStatus : InterfaceStatus.values()) {
            if (manageStatus.value == value) {
                return manageStatus;
            }
        }
        throw new IllegalArgumentException("1-7 is a range of parameter('value')");
    }

    public static InterfaceStatus fromName(String name) {
        for (InterfaceStatus manageStatus : InterfaceStatus.values()) {
            if (manageStatus.name.equals(name)) {
                return manageStatus;
            }
        }
        return null;
    }

    /**
     * 
     * 取得枚举对象的值
     * 
     * @return
     */
    public int getValue() {
        return this.value;
    }

    /**
     * 
     * 取得枚举对象的名称
     * 
     * @return
     */
    public String getName() {
        return this.name;
    }
}
