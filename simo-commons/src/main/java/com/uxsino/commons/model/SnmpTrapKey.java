package com.uxsino.commons.model;

import java.util.ArrayList;
import java.util.List;

public enum SnmpTrapKey {
                         coldStart("coldStart"),
                         warmStart("warmStart"),
                         linkDown("linkDown"),
                         linkUp("linkUp"),
                         authenticationFailure("authenticationFailure"),
                         egpNeighborLoss("egpNeighborLoss"),
                         enterpriseSpecific("enterpriseSpecific");

    private final String keyWord;

    private SnmpTrapKey(String k) {
        this.keyWord = k;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public static List<String> getAllKeyWords() {
        List<String> result = new ArrayList<>();
        for (SnmpTrapKey key : SnmpTrapKey.values()) {
            result.add(key.keyWord);
        }
        return result;
    }

}
