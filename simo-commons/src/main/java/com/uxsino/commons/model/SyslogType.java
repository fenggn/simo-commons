package com.uxsino.commons.model;

public enum SyslogType {

                        // 0 kernel messages
                        // 1 user-level messages
                        // 2 mail system
                        // 3 system daemons
                        // 4 security/authorization messages
                        // 5 messages generated internally by syslogd
                        // 6 line printer subsystem
                        // 7 network news subsystem
                        // 8 UUCP subsystem
                        // 9 clock daemon
                        // 10 security/authorization messages
                        // 11 FTP daemon
                        // 12 NTP subsystem
                        // 13 log audit
                        // 14 log alert
                        // 15 clock daemon
                        // 16 local use 0 (local0)
                        // 17 local use 1 (local1)
                        // 18 local use 2 (local2)
                        // 19 local use 3 (local3)
                        // 20 local use 4 (local4)
                        // 21 local use 5 (local5)
                        // 22 local use 6 (local6)
                        // 23 local use 7 (local7)
                        kernelMessages(0,"内核消息"),
                        userLevelMessages(1,"用户进程"),
                        mailSystem(2,"邮件系统"),
                        systemDaemons(3,"后台进程"),
                        authorization(4,"授权消息"),
                        syslogd(5,"系统日志"),
                        linePrinterSubsystem(6,"打印服务"),
                        networkNewsSubsystem(7,"新闻组信息"),
                        UUCP_Subsystem(8,"UUCP子系统"),
                        corn(9,"计划和任务信息"),

                        security(10,"安全信息"),
                        ftp(11,"FTP进程"),
                        ntp(12,"NTP进程"),
                        logAudit(13,"日志审计"),
                        logAlert(14,"日志告警"),
                        clock(15,"锁进程"),

                        local0(16,"本地用户1"),
                        local1(17,"本地用户2"),
                        local2(18,"本地用户3"),
                        local3(19,"本地用户4"),
                        local4(20,"本地用户5"),
                        local5(21,"本地用户6"),
                        local6(22,"本地用户7"),
                        local7(23,"本地用户8");

    private String text;

    private int value;

    private SyslogType(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public int getValue() {
        return value;
    }

    public static SyslogType getByValue(int value) {
        for (SyslogType type : SyslogType.values()) {
            if (type.value == value) {
                return type;
            }
        }

        return null;
    }

}
