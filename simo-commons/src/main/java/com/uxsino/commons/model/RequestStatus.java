package com.uxsino.commons.model;

/**
 * @description 请求状态枚举类
 * 
 * @date 2017年6月21日
 */
public enum RequestStatus {
                           Success(0,"成功"),
                           Response(1,"有响应"),
                           No_Response(2,"无响应"),
                           Error(3,"错误");

    /**
     * 请求状态的枚举值
     */
    private int value;

    /**
     * 请求状态的枚举名称
     */
    private String name;

    private RequestStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return this.value;
    }

    public String getName() {
        return this.name;
    }

    /**
     * 由枚举值生成枚举对象
     * @param value 枚举值
     * @return 枚举对象
     */
    public static RequestStatus fromValue(int value) {
        for (RequestStatus runStatus : RequestStatus.values()) {
            if (runStatus.value == value) {
                return runStatus;
            }
        }
        throw new IllegalArgumentException("0-3 is a range of parameter('value')");
    }

}
