package com.uxsino.commons.logicSelector.propFunction;

public interface IPropValueFunction {
    Object get(Object object);
}
