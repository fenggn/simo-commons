package com.uxsino.commons.logicSelector;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.uxsino.commons.utils.config.PropJsonDocument;

public class SelectorJsonDeserializer<T> extends StdDeserializer<ISelector<T>> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    protected SelectorJsonDeserializer() {
        this(null);
    }

    protected SelectorJsonDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public ISelector<T> deserialize(JsonParser jp, DeserializationContext ctxt)
        throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);

        PropJsonDocument doc = new PropJsonDocument(node);
        return (new SelectorFactory<T>()).createSelector(doc.getRootElement());
    }

}
