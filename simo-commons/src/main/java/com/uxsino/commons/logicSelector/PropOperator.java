package com.uxsino.commons.logicSelector;

public interface PropOperator {

    boolean accept(SelectorContext<?> context, Object object, String valueString);
}
