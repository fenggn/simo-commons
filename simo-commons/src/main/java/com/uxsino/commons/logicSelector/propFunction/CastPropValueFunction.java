package com.uxsino.commons.logicSelector.propFunction;

public class CastPropValueFunction implements IPropValueFunction {

    private IPropValueFunction propValueFunctionBase;

    private IPropValueFunction propValueFunctionNext;

    public CastPropValueFunction(IPropValueFunction pvf, IPropValueFunction pvfNext) {
        propValueFunctionBase = pvf;
        this.propValueFunctionNext = pvfNext;
    }

    @Override
    public Object get(Object object) {
        if (propValueFunctionBase == null || propValueFunctionNext == null) {
            return null;
        }
        Object base = propValueFunctionBase.get(object);

        return base == null ? null : propValueFunctionNext.get(base);
    }

}
