package com.uxsino.commons.logicSelector;

public enum GROUPBY_TYPE {
                          Any,
                          All,
                          NoneOf;

    public static GROUPBY_TYPE fromName(String name) {
        switch (name.toLowerCase()) {
        case "any":
            return Any;
        case "all":
            return All;
        case "noneof":
            return NoneOf;
        }
        throw new IllegalArgumentException("Unkonwn selector group by type " + name);
    }

};
