package com.uxsino.commons.logicSelector.propFunction;

import java.lang.reflect.Field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropFieldValueFunction implements IPropValueFunction {

    private static Logger logger = LoggerFactory.getLogger(PropFieldValueFunction.class);

    private Field field;

    PropFieldValueFunction(Field field) {
        this.field = field;
    }

    @Override
    public Object get(Object object) {

        try {
            return field.get(object);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            logger.error("illegal", e);
        }
        return null;
    }

}
