package com.uxsino.commons.logicSelector;

import java.util.function.Function;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

// @JsonTypeInfo(use = Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "stype")
// @JsonSubTypes({ @Type(value = PropSelector.class, name = "prop"), @Type(value = RefSelector.class, name = "ref") })
@JsonDeserialize(using = SelectorJsonDeserializer.class)
public interface ISelector<ObjectType> {
    boolean accept(SelectorContext<ObjectType> context);

    boolean solveReference(Function<String, ISelector<ObjectType>> finder);

    String toXml();

    @JsonProperty("type")
    @JSONField(name = "type")
    String getSelectorTypeName();

}
