package com.uxsino.commons.logicSelector;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.uxsino.commons.utils.debug.SourceInfo;
import com.uxsino.commons.utils.debug.SourceTracable;

public abstract class AbstractSelector<ObjectType> implements ISelector<ObjectType>, SourceTracable {

    private SourceInfo sourceInfo = new SourceInfo();

    @Override
    @JsonIgnore
    public SourceInfo getSourceInfo() {
        return sourceInfo;
    }

    @Override
    @JsonIgnore
    public void setSourceInfo(String sourceName, String sourceLocation) {
        sourceInfo.source = sourceName;
        sourceInfo.location = sourceLocation;
    }

    /**
     * Test if a given target matches the selector condition
     */
    @Override
    public final boolean accept(SelectorContext<ObjectType> context) {
        boolean r = false;

        r = _accept(context);
        return context.afterAccept(this, r);
    }

    public abstract boolean _accept(SelectorContext<ObjectType> context);

}
