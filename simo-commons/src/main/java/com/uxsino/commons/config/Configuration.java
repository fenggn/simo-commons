package com.uxsino.commons.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;

@Getter
@Component
public class Configuration {

    @Value("${service-mc:'mc'}")
    private String feignMc;

    @Value("${service-processengine:'processengine'}")
    private String feignProcessengine;

    @Value("${service-alert:'alert'}")
    private String feignAlert;

    @Value("${service-monitoring:'monitoring'}")
    private String feignMonitoring;

    @Value("${service-patrol:'patrol'}")
    private String feignPatrol;

    @Value("${service-report:'report'}")
    private String feignReport;

    @Value("${service-business:'business'}")
    private String feignBusiness;

    @Value("${service-knowledgebase:'knowledgebase'}")
    private String feignKnowledgebase;

    @Value("${service-datasource:'datasource'}")
    private String feignDatasource;
    
    @Value("${service-work:'work'}")
    private String feignWork;

    @Value("${service-msg:'msg'}")
    private String feignMsg;
}
