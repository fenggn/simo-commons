package com.uxsino.commons.regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Strings;

public class RegExp {
    /**
     * 比较宽泛校验
     * 邮箱格式正则 
     * 以字母数字开头，@前面部分包括-._在内的格式，且-._不能在结尾部分
     * 有且仅有一个@符号
     * 末尾部分@后面的部分，必须是xx.xx这种格式，且点号前后的字符为  数字和字符串
     */
    public static final String REGEXP_EMAIL = "^[\\w]+([\\.|\\-|\\_]{1}[\\w]{1,})*([\\w])*[@]{1}[\\w]{1,}([.]{1}[a-zA-Z]{2,3})$";

    public static final String REGEXP_READER_IMAGE_CONTENT_TYPE_READER = "(png|jpg|jpeg|gif|ico|tiff|fax|x\\-icon|x\\-jpe|pnetvue|vnd\\.rn\\-realpix|vnd\\.wap\\.wbmp){1}";

    /**
     * 整型数据格式 以数字开头，且以数字结尾 不包括小数点，包括正负数
     */
    public static final String REGEXP_INTEGER = "^[\\-]{0,1}[0-9]{1,}$";

    /**
     * 包括正负的数字，小数或者整数
     */
    public static final String REGEXP_NUMERIC = "[\\-]{0,1}[0-9]{1,}[.]{0,1}[0-9]{0,}$";

    /**
     * HTTP HTTPS URL格式判定，忽略字母大小写
     */
    public static final String REGEXP_HTTP_URL = "(?i)^(http[s]{0,1}://){1}.{1,}$";

    /**
     * 手机号码格式 - 中国内地
     */
    public static final String REGEXP_PHONE = "^1[3|4|5|7|8][0-9]{9}$";

    /**
     * 电话号码 格式校验
     */
    public static final String REGEXP_TELEPHONE = "^(\\(\\d{3,4}\\)|\\d{3,4}-){1}\\d{7,8}$";

    /**
     * 密码格式校验
     * 6 ~ 16位字母或者数字或者组合
     */
    public static final String REGEXP_PWD = "^[0-9a-zA-Z]{6,16}$";

    /**
     * IP地址格式校验
     */
    public static final String IP_FMT = "([0-9]{1,3}\\.){3}[0-9]{1,3}";

    /**
     * 非必填的IP地址格式校验
     */
    public static final String IP_NOT_MUST = "(" + IP_FMT + ")?";

    public static final String IPV4_PATTERN = "^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$";

    /**
     * MAC地址格式校验
     */
    public static final String MAC_FMT = "^([0-9a-fA-F]{2})(((-[0-9a-fA-F]{2}){5}$)|((:[0-9a-fA-F]{2}){5}$))";

    /**
     * 非必填的MAC地址格式校验
     */
    public static final String MAC_NOT_MUST = "(" + MAC_FMT + ")?";

    /**
     * 支持中英文、数字和'_'的组合
     */
    public static final String NOT_SPECICAL_FMT = "^[\\u4e00-\\u9fa5_a-zA-Z0-9]+$";

    /**
     * 金额格式校验
     */
    public static final String PRICE_FMT = "(^[1-9](\\d+)?(\\.\\d+?)$)|(^(0){1}$)|(^\\d\\.\\d+?$)|(^\\d+?$)";

    /**
     * 网址格式校验
     */
    public static final String URL_FMT = "^(\\w+:\\/\\/)?\\w+(\\.\\w+){1,}$";

    /**
     * 字母或者数字，且不能为空
     */
    public static final String REGEXP_LETTER_OR_DIGITS = "^[0-9a-zA-Z]{1,}$";

    /**
     * 序列号格式校验
     */
    public static final String SERIES_FMT = "^[0-9a-zA-Z]{1,}$";

    /**
     * OID 格式校验
     */
    public static final String OID_FMT = "^([0-9A-Za-z]+)(.[0-9A-Za-z]+)+$";
    
    /**
     * 数字用,分隔
     */
    public static final String NUMBER_SPLIT = "(\\d+)(,\\d+)*";

    /**
     * 字符串校验
     */
    public static final String STRING_FMT = "^[a-zA-Z0-9_\\-\\[\\]\\【\\】\\（\\）\\(\\)\\.{\\}<>《》\\u4E00-\\u9FA5\\,，、\\ ]{0,50}$";

    public static boolean match(String exp, String src) {
        return Pattern.compile(Strings.nullToEmpty(exp)).matcher(Strings.nullToEmpty(src)).matches();
    }

    /**
     * 查询字符串src 中所有满足 regexp 正则表达式的数据
     * @param src
     * @param regexp
     * @return
     */
    public static String fetch(String src, String regexp) {
        String res = "";
        Matcher matcher = Pattern.compile(Strings.nullToEmpty(regexp)).matcher(Strings.nullToEmpty(src));
        while (matcher.find()) {
            res += matcher.group();
        }
        return res;
    }

    public static boolean isEmail(String str) {
        return match(REGEXP_EMAIL, str);
    }

    public static void main(String[] args) {
        String src = "UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1";
        String regexp = "(?<=(\\s|,))(RUNNING|DOWN)(?=(\\s|,))";
        System.out.println(fetch(src, regexp));
    }
}
