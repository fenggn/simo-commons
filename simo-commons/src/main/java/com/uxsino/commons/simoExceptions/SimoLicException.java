package com.uxsino.commons.simoExceptions;

public class SimoLicException extends RuntimeException{
    public SimoLicException(){
        super();
    }
    public SimoLicException(String msg){
        super(msg);
    }
    public SimoLicException(Throwable cause) {
        super(cause);
    }
}
