package com.uxsino.commons.baseclass;

import java.io.File;
import org.apache.commons.lang3.tuple.Pair;

public class Constants {

    public static final String SEPARATOR = File.separator;

    private static String file_path = System.getProperty("webapp.root");

    private static Object LICENSE = null;

    public static final String LICENSE_PATH = "license" + SEPARATOR + "simo.lic";

    public final static String APP_ID = "SIMO";

    // public static JsonModel SYSTEM_STATUS = null;
    public static final Pair<String, String> LIC_INFO = Pair.of("_simo_license_info_", "_simo_license_info_");

    /**
     * 时间格式
     */
    public final static String[] datePatterns = new String[] { "yyyy-MM-dd", "yyyy-MM-dd HH", "yyyy-MM-dd HH:mm",
            "yyyy-MM-dd HH:mm:ss" };

    public static Object getLicense() {
        return LICENSE;
    }

    public static void setLicense(Object license) {
        Constants.LICENSE = license;
    }

    public static String getFile_path() {
        return file_path;
    }

    public static void setFile_path(String file_path) {
        Constants.file_path = file_path;
    }

}
