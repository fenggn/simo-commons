package com.uxsino.commons.baseclass;

import com.alibaba.fastjson.JSONObject;

public interface ICreateMenu {

    JSONObject create();

}
