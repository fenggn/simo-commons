package com.uxsino.commons.baseclass;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * 常量类
 * 
 *
 */
public class Constant {

    public final static String SESSION_USER = "SITE_USER";

    public final static String SITE_USER_ROLES = "SITE_USER_ROLES";

//    public final static String SITE_USER_DOMAINS = "SITE_USER_DOMAINS";

//    public final static String SITE_USER_DOMAIN_IDS = "SITE_USER_DOMAIN_IDS";

//    public final static String SITE_DEFAULT_DOMAIN_ID = "SITE_DEFAULT_DOMAIN_ID";

    public final static String IS_SUPER_ADMIN = "IS_SUPER_ADMIN";

    public final static String IS_SYSTEM_ADMIN = "IS_SYSTEM_ADMIN";

    public final static String IS_AUDIT_ADMIN = "IS_AUDIT_ADMIN";

    public final static String IS_POLICY_ADMIN = "IS_POLICY_ADMIN";

    public final static String LICENSE = "LICENSE";

    public static String WEBSOCKET_USERNAME = "username";

    public static String HTTP_SESSION = "httpsession";

    public static String WEBSOCKET_USERID = "userid";

    /**
     * 消息类型：创建告警
     */
    public static String MSG_TYPE_CREATE_ALERT = "alert";

    /**
     * 消息类型：恢复告警
     */
    public static String MSG_TYPE_RECOVER_ALERT = "recover";

    /**
     * 消息类型：告警失效
     */
    public static String MSG_TYPE_INVALID_ALERT = "invalid";

    /**
     * 消息来源：虚拟化监控
     */
    public static String MSG_SOURCE_VM = "vm";

    /**
     * 消息类型：SIMO组件信息
     */
    public static String SIMO_SERVICE_MSG = "simo_service_msg";

    /**
     * 消息类型：系统通知
     */
    public static String SIMO_SYSTEM_MSG = "simo_system_msg";

    /**
     * 业务相关的应用服务指标列表
     */
    public final static List<String> BNS_SERVICE_INDS = Lists.newArrayList("url_connection_check");

    /**
     * 业务相关的主机指标列表
     */
    public final static List<String> BNS_HOST_INDS = Lists.newArrayList(new String[] { "disk_usage", "cpu_usage_avg",
            "physical_memory", "linux_disk_io", "network_if_entry", "tcp_connection" });

    /**
     * 业务相关的oracle指标列表
     */
    public final static List<String> BNS_ORACLE_INDS = Lists.newArrayList("oracle_table_space_info");

    /**
     * 业务相关的db2指标列表
     */
    public final static List<String> BNS_DB2_INDS = Lists.newArrayList("db2_tablespace");

    /**
     * 业务相关的网络设备指标列表
     */
    public final static List<String> BNS_NETWORK_INDS = Lists.newArrayList("network_if_entry");

}
