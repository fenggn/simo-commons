package com.uxsino.commons.enums;

import com.uxsino.commons.model.BaseNeClass;

public enum StrategyType {
    VD,
    VM,
    MON;

    public static StrategyType ChangeStrategyTypeByBaseNeClass(BaseNeClass baseNeClass) {
        StrategyType transType = null;
        switch (baseNeClass) {
        case virtualization:// 虚拟化
            transType = StrategyType.VM;
            break;
        case video:// 视频设备
            transType = StrategyType.VD;
            break;
        default:
            transType = StrategyType.MON;
        }
        return transType;
    }

}
