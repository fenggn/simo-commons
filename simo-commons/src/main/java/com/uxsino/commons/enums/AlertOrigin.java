package com.uxsino.commons.enums;

/**
 * @description 告警来源的枚举对象
 * 
 * @date 2017年4月13日
 */
public enum AlertOrigin {
                         POLLING(0,"指标采集轮询"),
                         TRAP_SYSLOG(1,"网元Trap/Syslog推送"),//作废 拆分成SYSLOG和SNMPTRAP 怕有旧数据所以暂保留
                         THIRD_PARTY(2,"第三方监控系统"),
                         VENDER_ADMIN(3,"设备厂家网管"),
                         SELF_CHECK(4,"系统自检"),   // SIMO系统组件运行故障；告警模块定期检查未及时处理的告警，产生新的告警提醒用户
                         UPLEVEL_BY_HAND(5,"手动升级告警"),    // 手动升级告警，产生新的告警信息
                         UPLEVEL_AUTOMATIC(6,"自动升级告警"),   // 自动升级告警，产生新的告警信息
                         AVAILABILITY(7,"可用性检测"),
                         MANUAL(8,"手动新增告警"),// 用于补充系统漏报的故障
                         TERMINAL(9,"终端检测"),
                         CDPS(10,"CDPS"),
                         BMS(11,"BMS"),
                         SYSLOG(12,"Syslog推送"),
                         SNMPTRAP(13,"snmptrap推送");
    private int value;

    private String text;

    AlertOrigin(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return this.value;
    }

    public String getText() {
        return this.text;
    }

    /**
    * 将传入的告警来源枚举值转化为枚举对象
    * @param value 传入的告警来源枚举值
    * @return 告警来源枚举对象
    */
    public static AlertOrigin fromValue(int value) {
        for (AlertOrigin item : AlertOrigin.values()) {
            if (item.value == value) {
                return item;
            }
        }
        throw new IllegalArgumentException();
    }

    /**
    * 将传入的告警来源中文显示转化为枚举对象
    * @param value 传入的告警来源中文显示
    * @return 告警来源枚举对象
    */
    public static AlertOrigin fromText(String text) {
        for (AlertOrigin item : AlertOrigin.values()) {
            if (item.text.equals(text)) {
                return item;
            }
        }
        throw new IllegalArgumentException();
    }

}
