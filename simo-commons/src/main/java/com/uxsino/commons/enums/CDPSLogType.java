package com.uxsino.commons.enums;

/**
 * @description CDPS日志的枚举对象
 *
 * @date 2017年4月13日
 */
public enum CDPSLogType {
    STRATEGYLOG("strategyLog","策略日志"),
    BASELINELOG("baselineLog","基线日志"),
    OPTLOG("optLog","操作日志");
    private String value;

    private String text;

    CDPSLogType(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return this.value;
    }

    public String getText() {
        return this.text;
    }

    /**
     * 将传入的CDPS日志枚举值转化为枚举对象
     * @param value 传入的告警来源枚举值
     * @return 告警来源枚举对象
     */
    public static CDPSLogType fromValue(String value) {
        for (CDPSLogType item : CDPSLogType.values()) {
            if (item.value == value) {
                return item;
            }
        }
        throw new IllegalArgumentException();
    }

    /**
     * 将传入的CDPS日志中文显示转化为枚举对象
     * @param text 传入的告警来源中文显示
     * @return 告警来源枚举对象
     */
    public static CDPSLogType fromText(String text) {
        for (CDPSLogType item : CDPSLogType.values()) {
            if (item.text.equals(text)) {
                return item;
            }
        }
        throw new IllegalArgumentException();
    }

}
