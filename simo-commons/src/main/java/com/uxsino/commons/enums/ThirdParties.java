package com.uxsino.commons.enums;

public enum ThirdParties {
    CDPS(0,"CDPS接入"),
    BMS(0,"BMS接入");
    private int value;

    private String text;

    ThirdParties(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return this.value;
    }

    public String getText() {
        return this.text;
    }
}
