package com.uxsino.commons.enums;

/**
 * 
 * 描述：针对与各种指定类型有关的枚举种类，如int string等
 * 
 * 时间：2017年9月26日
 * @param <T>
 */
public interface TypedEnum<T> {

    public T key();

    public String desc();
}
