package com.uxsino.commons.enums;

/**
 * 数字序号的枚举
 * 
 * 
 *
 */
public interface IntEnum extends TypedEnum<Integer> {

}
