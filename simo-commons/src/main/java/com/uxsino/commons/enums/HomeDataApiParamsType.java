package com.uxsino.commons.enums;

public enum HomeDataApiParamsType {
    text, list, number
}
