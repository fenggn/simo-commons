/**
 * 
 */
package com.uxsino.commons.enums;

/**
 * 
 * 
 */
public enum YesNo {
                   Y(1,"是"),
                   N(0,"否"),;

    private int value;

    private String text;

    private YesNo(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return this.value;
    }

    public String getText() {
        return this.text;
    }

    public static YesNo valueOf(int value) {
        for (YesNo yn : YesNo.values()) {
            if (yn.getValue() == value) {
                return yn;
            }
        }
        throw new IllegalArgumentException();
    }

}
