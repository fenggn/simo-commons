package com.uxsino.commons.tftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.apache.commons.net.tftp.TFTP;
import org.apache.commons.net.tftp.TFTPClient;

import com.google.common.base.Strings;

/**
 * 描述：TFTP client 
 * @author <a href="mailto:royrxc@gmail.com">Ran</a>
 *
 * @date 2018年3月7日
 */
public class TFTPS {
    private String host;

    private int port = TFTP.DEFAULT_PORT;

    TFTPClient client;

    private int timeoutSecond = TFTP.DEFAULT_TIMEOUT;// 超时时间 秒数

    public static TFTPS of() {
        return new TFTPS();
    }

    public TFTPS() {
    }

    public TFTPS host(String host) {
        this.host = host;
        return this;
    }

    public TFTPS port(int port) {
        this.port = port;
        return this;
    }

    public TFTPS timeout(int timeoutSecond) throws SocketException {
        this.timeoutSecond = timeoutSecond;
        if (this.client != null) {
            this.client.setDefaultTimeout(timeoutSecond * 1000);
        }
        return this;
    }

    private synchronized void init() {
        if (this.client == null) {
            this.client = new TFTPClient();
        }
        if (Strings.isNullOrEmpty(this.host)) {
            throw new RuntimeException("TFTP host not present!");
        }
    }

    private synchronized void open() throws SocketException {
        this.init();
        if (!this.client.isOpen()) {
            this.client.open(this.port);
        }
        this.client.setDefaultTimeout(timeoutSecond * 1000);
    }

    private synchronized void close() throws SocketException {
        this.init();
        if (this.client.isOpen()) {
            this.client.close();
        }
    }

    public synchronized void get(String file, OutputStream out) throws UnknownHostException, IOException {
        this.open();
        this.client.receiveFile(file, TFTP.BINARY_MODE, out, this.host);
        this.close();
    }

    public synchronized void get(String file, String filePath) throws UnknownHostException, IOException {
        FileOutputStream out = new FileOutputStream(new File(filePath));
        this.get(file, out);
    }

    public synchronized void put(String file, InputStream in) throws UnknownHostException, IOException {
        this.open();
        client.sendFile(file, TFTP.BINARY_MODE, in, this.host);
        this.close();
    }

    public synchronized void put(String file, String in) throws UnknownHostException, IOException {
        FileInputStream input = new FileInputStream(new File(in));
        this.put(file, input);
    }

    public TFTPClient getClient() {
        return this.client;
    }

    /*public static void main(String[] args) throws UnknownHostException, IOException {
        TFTPS.of().host("192.168.5.62").timeout(999999).port(69).get("222.txt", "E:\\WPS\\cache\\1.txt");
    }*/

}
