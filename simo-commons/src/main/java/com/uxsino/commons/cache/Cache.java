package com.uxsino.commons.cache;

import com.uxsino.commons.utils.Dates;
import lombok.Data;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

/**
 * 全局缓存
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public final class Cache {
    private static ConcurrentHashMap<Pair<String, String>, Item> pool = new ConcurrentHashMap<>();

    private static final Thread checker = new Thread(() -> {
        while (true) {
            try {
                if (!pool.isEmpty()) {
                    pool.keySet().parallelStream().filter(k -> pool.get(k).expired()).forEach(k -> {
                        Item item = Cache.remove(k);
                        if (item != null) {
                            item.destroy();
                        }
                    });
                }
            } catch (Exception e) {
            }
            try {
                Thread.currentThread();
                Thread.sleep(1000);
            } catch (Exception e) {
            }
        }
    });
    static {
        checker.setDaemon(true);
        checker.start();
    }

    /**
     * 存储并设置 存储的参数， 如destroy
     * 默认30s 过期。
     * @param key
     * @param cacheFn
     * @param <T>
     */
    public static <T> void cache(Pair<String, String> key, Consumer<Item<T>> cacheFn) {
        synchronized (Cache.class) {
            Item<T> item = pool.get(key);
            if (item == null) {
                item = Item.<T> of().expired(30 * 1000);
            }
            if (cacheFn != null) {
                cacheFn.accept(item);
            }
            pool.put(key, item);
        }
    }

    /**
     *
     * @param key
     * @param value
     * @param expireMs  过期时间毫秒数，相对于当前时间+ 毫秒数过期，如果不传或者传递的值小于等于0，则默认使用30s有效期存储
     * @param <T>
     */
    public synchronized static <T> void store(Pair<String, String> key, T value, Long expireMs) {
        cache(key, itm -> {
            itm.setDatas(value);
            if (expireMs != null) {
                itm.expired(expireMs);
            }
        });
    }

    /**
     * 存储 缓存
     * 默认使用30s有效期存储
     * @param key
     * @param value
     * @param <T>
     */
    public synchronized static <T> void store(Pair<String, String> key, T value) {
        store(key, value, null);
    }

    /**
     * remove cache，not be destoried
     * @param key
     */
    public static Item remove(Pair<String, String> key) {
        synchronized (Cache.class) {
            Item item = pool.remove(key);
            return item;
        }
    }

    public static Item destroy(Pair<String, String> key) {
        synchronized (Cache.class) {
            Item item = pool.remove(key);
            if(item != null){
                item.destroy();
            }
            return item;
        }
    }

    /**
     * get cached data, return null if expired or not cached.
     * @param key
     * @param <T>
     * @return
     */
    public static <T> T get(Pair<String, String> key) {
        Item<T> itm = pool.get(key);
        if(itm == null){
            return null;
        }
        if (itm.expired()) {
            remove(key);
            return null;
        }



        return itm.getDatas();
    }

}
