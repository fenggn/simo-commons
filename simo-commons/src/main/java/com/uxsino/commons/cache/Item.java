package com.uxsino.commons.cache;

import com.uxsino.commons.utils.Dates;
import lombok.Data;

import java.util.Date;
import java.util.function.Consumer;

/**
 * @ClassName Item
 * @Description TODO
 * @Author <a href="mailto:royrxc@gnail.com">Administrator</a>
 * @Daate 2019/6/17 16:40
 **/
@Data
public final class Item<T> {
    public Item() {
        this.setExpired(new Date());
    }

    public static <T> Item<T> of() {
        return new Item<T>();
    }

    private Date expired;

    private T datas;

    private Consumer<T> destroy;

    public synchronized boolean expired() {
        if (this.expired != null) {
            return new Date().after(expired);
        }
        return false;
    }

    /**
     * 销毁数据
     * @param destory
     */
    public void destroy(Consumer<T> destory) {
        this.destroy = destory;
    }

    final void destroy() {
        if (this.destroy != null) {
            try {
                this.destroy.accept(this.datas);
            } catch (Exception e) {
            }
        }
    }

    public Item<T> setDatas(T datas) {
        if (this.datas != null) {
            this.destroy();// 销毁之前的数据
        }
        this.datas = datas;
        return this;
    }

    public synchronized Item<T> expired(long expiredMs) {
        if (expiredMs > 0) {
            this.expired = Dates.from(System.currentTimeMillis() + expiredMs).toDate();
        }
        return this;
    }
}
