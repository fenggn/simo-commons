package com.uxsino.commons.cache;

import org.apache.commons.lang3.tuple.Pair;

import java.util.function.Consumer;

public interface ICache<T> {
    /**
     * 存储并设置 存储的参数， 如destroy
     * 默认30s 过期。
     * @param key
     * @param cacheFn
     * @param <T>
     */
    <T> void cache(Pair<String, String> key, Consumer<Item<T>> cacheFn);

    /**
     *
     * @param key
     * @param value
     * @param expireMs  过期时间毫秒数，相对于当前时间+ 毫秒数过期，如果不传或者传递的值小于等于0，则默认使用30s有效期存储
     * @param <T>
     */
    <T> void store(Pair<String, String> key, T value, Long expireMs);

    /**
     * 存储 缓存
     * 默认使用30s有效期存储
     * @param key
     * @param value
     * @param <T>
     */
    default  <T> void store(Pair<String, String> key, T value) {
        store(key, value, null);
    }

    /**
     * remove cache
     * @param key
     */
    Item remove(Pair<String, String> key);

    /**
     * get cached data, return null if expired or not cached.
     * @param key
     * @param <T>
     * @return
     */
    <T> T get(Pair<String, String> key);
}
