package com.uxsino.commons.utils;

import java.util.Date;

import javax.servlet.http.HttpSession;

import com.alibaba.fastjson.JSONObject;
import com.uxsino.commons.model.AuditLogType;

public class AuditLogUtils {
    public static JSONObject crateLog(HttpSession session, String msg) {
        JSONObject json = new JSONObject();
        if (session == null) {
            json.put("logType", AuditLogType.system);

        } else {
            json.put("logType", AuditLogType.user);
            JSONObject user = SessionUtils.getCurrentUserFromSession(session);
            if (user != null) {
                json.put("logMsg",
                    "用户：" + user.getString("userName") + "(" + user.getString("loginName") + ")-->" + msg);
                json.put("userId", SessionUtils.getCurrentUserIdBySession(session));
            } else {
                json.put("logMsg", "未知用户：" + msg);
            }
        }
        json.put("generateTime", new Date());
        return json;
    }
}
