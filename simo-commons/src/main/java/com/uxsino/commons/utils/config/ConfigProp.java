package com.uxsino.commons.utils.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
@Inherited
public @interface ConfigProp {

    enum CASE_MODE {
                    UPPER,
                    LOWER,
                    NONE
    }

    public String name();

    public boolean required() default false;

    // if this property can be set as a child element
    public boolean subElement() default false;

    // trim string value
    public boolean trim() default true;

    // to upper/lower case of string value
    public CASE_MODE letterCase() default CASE_MODE.NONE;
}
