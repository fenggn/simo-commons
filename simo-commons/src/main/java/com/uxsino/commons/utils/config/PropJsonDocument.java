package com.uxsino.commons.utils.config;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;

public class PropJsonDocument implements PropDocument {
    // private static Logger logger =
    // LoggerFactory.getLogger(PropJsonDocument.class);
    JsonNode doc;

    PropJsonElement rootElement;

    public PropJsonDocument(JsonNode doc) {
        this.doc = doc;
        if (doc.getNodeType() == JsonNodeType.ARRAY) {
            rootElement = new PropJsonArrayElement(this, doc);
        } else {
            rootElement = new PropJsonObjectElement(this, doc);
        }
    }

    @Override
    public String getSourceName() {
        return "";
    }

    @Override
    public String getProp(String name) {
        return rootElement.getProp(name);
    }

    @Override
    public List<PropElement> getElements(String name) {
        return rootElement.getElements(name);
    }

    @Override
    public List<PropElement> getElements() {
        return rootElement.getChildElements();
    }

    @Override
    public PropElement getFirstElement(String name, String fieldAsTagName) {
        return rootElement.getFirstElement(name, fieldAsTagName);
    }

    public PropElement getRootElement() {
        return rootElement;
    }

    public PropElement createElement(JsonNode node) {
        if (node.getNodeType() == JsonNodeType.ARRAY) {
            return new PropJsonArrayElement(this, node);
        }
        return new PropJsonObjectElement(this, node);
    }
}
