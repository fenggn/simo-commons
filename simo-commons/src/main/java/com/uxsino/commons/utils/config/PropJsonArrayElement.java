package com.uxsino.commons.utils.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;

public class PropJsonArrayElement extends PropJsonElement {

    public PropJsonArrayElement(PropJsonDocument doc, JsonNode node) {
        super(doc, node);
    }

    @Override
    public List<PropElement> getChildElements(String childFieldName) {
        ArrayList<PropElement> elements = new ArrayList<>();

        Iterator<Map.Entry<String, JsonNode>> fields = node.fields();

        while (fields.hasNext()) {
            Map.Entry<String, JsonNode> entry = fields.next();
            JsonNode x = entry.getValue();
            JsonNodeType t = x.getNodeType();

            if (t == JsonNodeType.ARRAY) {
                for (int i = 0; i < node.size(); i++) {
                    elements.add(doc.createElement(x.get(i)));
                }
            }
        }
        return elements;
    }

    @Override
    public List<PropElement> getElements(String name) {
        ArrayList<PropElement> elements = new ArrayList<>();

        return elements;
    }

    @Override
    public PropElement getFirstElement(String name, String fieldAsTagName) {

        return null;
    }

    @Override
    public String getProp(String name) {
        return "";
    }

    @Override
    public Map<String, String> getProps() {
        return new HashMap<>();
    }

    @Override
    public String getText() {
        return null;
    }

}
