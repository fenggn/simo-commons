package com.uxsino.commons.utils.debug;

public class SourceInfo {
    public String source = "unknown";

    public String location = "unknown";

    public String toString() {
        return source + ":" + location;
    }
}
