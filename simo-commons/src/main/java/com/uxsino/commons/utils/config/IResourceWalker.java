package com.uxsino.commons.utils.config;

import java.io.IOException;
import java.net.URL;
import java.util.function.Consumer;

public interface IResourceWalker {

    public void forEach(Consumer<URL> url) throws IOException;
}
