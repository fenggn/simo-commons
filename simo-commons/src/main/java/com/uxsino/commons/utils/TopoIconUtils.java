package com.uxsino.commons.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.alibaba.fastjson.JSONObject;
import com.google.common.io.Files;

/**
 * 
 * @description 拓扑图标工具类
 * @date 2019年7月3日
 */
public class TopoIconUtils {

    private static final Logger logger = LoggerFactory.getLogger(TopoIconUtils.class);

    private static List<JSONObject> defIconList = new ArrayList<>();

    /**
     * 初始化默认图标
     */
    private static void initDefaultIcon() {
        try {
            new ClassPathResourceWalker("classpath*:/img/topo/**/*.png").forEach(file -> {
                InputStream in;
                try {
                    in = file.openStream();
                } catch (IOException e) {
                    logger.error("Logo Icon file failed: ", e);
                    return;
                }
                String fileName = Files.getNameWithoutExtension(file.getFile()) + "."
                        + Files.getFileExtension(file.getFile());
                JSONObject icon = new JSONObject();
                icon.put("name", fileName);
                try {
                    /**
                     * 说明：读取文件不能使用  in.read(byte[in.available()])一次性读取，读取大文件可能存在读取不完整的情况，使用缓存进行逐步读取。
                     */
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    byte[] cache = new byte[1024];
                    int size = -1;
                    while ((size = in.read(cache)) != -1) {
                        out.write(cache, 0, size);
                    }
                    icon.put("iconStream", out.toByteArray());
                } catch (IOException e) {
                    logger.error("", e);
                } finally {
                    try {
                        in.close();
                    } catch (Exception e) {
                        logger.error("关闭流失败", e);
                    }
                }

                File f = new File(file.getPath());
                File parentFile = f.getParentFile();
                String dirName = parentFile.getName();
                icon.put("dirName", dirName);
                if (parentFile.getParentFile() != null) {
                    icon.put("preDirName", parentFile.getParentFile().getName());
                }
                defIconList.add(icon);
            });
        } catch (IOException e) {
            logger.error("读取文件失败： ", e);
        }
    }

    /**
     * 获取默认图标
     * @return
     */
    public static List<JSONObject> getDefIconList() {
        if (defIconList.isEmpty()) {
            initDefaultIcon();
        }
        return defIconList;
    }

}
