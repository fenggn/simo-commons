package com.uxsino.commons.utils;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 字体工具类
 * @author jane
 *
 */
public class FontsUtil {
    private static final Logger logger = LoggerFactory.getLogger(FontsUtil.class);

    public static Font font;

    static {
        InputStream simsunFontFile = FontsUtil.class.getResourceAsStream("/fonts/SIMSUN.TTC");
        try {
            try {
                font = Font.createFont(Font.PLAIN, simsunFontFile);
            } catch (FontFormatException e) {
                font = new Font("宋体", Font.BOLD, 6);
                logger.error("", e);
            }
        } catch (IOException e) {
            font = new Font("宋体", Font.BOLD, 6);
            logger.error("", e);
        }
    }

    /**
     * 宋体
     * @param style
     * @param size
     */
    public static Font getSIMSUN(int style, float size) {
        return font.deriveFont(style, size);
    }

}
