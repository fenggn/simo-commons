package com.uxsino.commons.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @ClassName UxsinoLinkedBlockingQueue
 * @Description TODO
 * @Author <a href="mailto:royrxc@gmail.com">Ran</a>
 * @Daate 2019/8/10 18:40
 **/
public class UxsinoLinkedBlockingQueue<E> extends LinkedBlockingQueue<E> {

    public List<E> poll(int size) {
        List<E> ves = new ArrayList<>();
        while (ves.size() < size && !this.isEmpty()){
            E ele = this.poll();
            if(ele != null){
                ves.add(ele);
            }else{
                break;
            }
        }
        return ves;
    }
}
