package com.uxsino.commons.utils.config;

import java.util.List;
import java.util.Map;

public interface PropElement {

    PropDocument getOwnerDoc();

    default String getTagName() {
        return getTagName(null);
    }

    String getTagName(String fieldAsTagName);

    String getProp(String name);

    default String getProp(String name, String defaultValue) {
        String s = getProp(name);
        return null == s ? defaultValue : s;
    }

    Map<String, String> getProps();

    default List<PropElement> getChildElements() {
        return getChildElements(null);
    }

    List<PropElement> getChildElements(String childFieldName);

    List<PropElement> getElements(String name);

    default PropElement getFirstElement(String name) {
        return getFirstElement(name, null);
    }

    PropElement getFirstElement(String name, String fieldAsTag);

    String getText();

    String getSourceLocation();

    default String getSourceName() {
        return getOwnerDoc().getSourceName();
    }
}
