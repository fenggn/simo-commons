package com.uxsino.commons.utils.config;

import java.util.List;

public interface PropDocument {

    String getSourceName(); // file names, url etc.

    String getProp(String name);

    List<PropElement> getElements(String name);

    List<PropElement> getElements();

    default PropElement getFirstElement(String name) {
        return getFirstElement(name, null);
    }

    PropElement getFirstElement(String name, String fieldAsTagName);

}
