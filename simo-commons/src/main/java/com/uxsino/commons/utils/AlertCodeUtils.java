package com.uxsino.commons.utils;

import org.apache.commons.lang3.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.uxsino.commons.enums.CDPSLogType;
import com.uxsino.commons.model.IpAlertRuleType;

/**
 * @description 告警编码工具类
 * @date 2019年10月9日
 */
public class AlertCodeUtils {

    public static final String AVAILABILITY_CODEKEY = "availability";

    public static final String MANUAL_CODEKEY = "manual";

    private static final String SYSTEM_CODEKEY = "service_status";

    /**
     * 获取资源指标告警编码
     * @param neId 资源ID
     * @param componentId 部件ID
     * @param indicatorId 指标ID
     * @param field 属性ID
     * @param ruleId 告警规则ID
     * @param setting 告警设置
     * @return
     */
    public static String getNeIndAlertCode(String neId, String componentId, String indicatorId, String field,
        Long ruleId, JSONObject setting) {
        return EncryptUtil.md5AndSha(neId + componentId + indicatorId + field + ruleId + setting.toString());
    }

    /**
     * 获取业务指标告警编码
     * @param ruleId 策略ID
     * @param indicator 指标
     * @param setting 告警设置
     * @return
     */
    public static String getBnsIndAlertCode(Long ruleId, String indicator, JSONObject setting) {
        return EncryptUtil.md5AndSha(String.valueOf(ruleId) + indicator + setting.toString());
    }

    /**
     * 获取链路告警编码
     * @param id 链路ID
     * @param ruleId 策略ID
     * @param setting 告警设置
     * @return
     */
    public static String getLinkAlertCode(String id, Long ruleId, JSONObject setting) {
        return EncryptUtil.md5AndSha(id + ruleId + setting.toString());
    }

    /**
     * 获取终端告警编码
     * @param type 类型
     * @param terminalMac mac地址
     * @param ruleId 策略ID
     * @param eventId 事件模板ID
     * @param recordId 记录ID
     * @return
     */
    public static String getTerminalAlertCode(String type, String terminalMac, Long ruleId, Long eventId,
        String recordId) {
        return EncryptUtil.md5AndSha(type + terminalMac + ruleId + eventId + recordId);
    }

    /**
     * 获取系统告警编码
     * @param ruleId 策略ID
     *  @param indicator 指标
     * @param setting 告警设置
     * @return
     */
    public static String getSystemAlertCode(String objectId, Integer level) {
        return EncryptUtil.md5AndSha(SYSTEM_CODEKEY + objectId + level);
    }

    /**
     * 获取SnmpTrap告警编码
     * @param ruleId 策略ID
     * @param typeKeys 类型
     * @param level 告警级别
     * @param neId 资源ID
     * @param id SnmpTrap ID
     * @return
     */
    public static String getTrapAlertCode(Long ruleId, String typeKeys, Integer level, String neId, Long id) {
        return EncryptUtil.md5AndSha(String.valueOf(ruleId) + typeKeys + level + neId + id);
    }

    /**
     * 获取Syslog告警编码
     * @param ruleId 策略ID
     * @param logLevels 日志级别
     * @param logTypes 日志类型
     * @param keyWords 关键字
     * @param symbol 逻辑关系
     * @param level 告警级别
     * @param objectId 资源ID
     * @param id Syslog ID
     * @return
     */
    public static String getLogAlertCode(Long ruleId, String logLevels, String logTypes, String keyWords,
        String symbol, Integer level, String objectId, Long id) {
        return EncryptUtil.md5AndSha(String.valueOf(ruleId) + logLevels + logTypes + keyWords + symbol + level
                + objectId + id);
    }

    /**
     * 获取BMS告警编码
     * @param objName 资源名称
     * @param ip IP地址
     * @param baseNeClass 资源类型
     * @param alertBrief 告警内容
     * @param level 告警级别
     * @return
     */
    public static String getBmsAlertCode(String objName, String ip, String baseNeClass, String alertBrief, Integer level) {
        return EncryptUtil.md5AndSha(objName + ip + baseNeClass + alertBrief + level);
    }

    /**
     * 获取CDPS告警编码
     * @param ip IP地址
     * @param logType 日志类型
     * @param alertBrief 告警内容
     * @param level 告警级别
     * @return
     */
    public static String getCdpsAlertCode(String ip, CDPSLogType logType, String alertBrief, Integer level) {
        return EncryptUtil.md5AndSha(ip + logType + alertBrief + level);
    }

    /**
     * 获取手动资源告警编码
     * @param neId 资源ID
     * @param componentId 部件ID
     * @param indicatorId 指标ID
     * @param field 属性ID
     * @return
     */
    public static String getManualNeAlertCode(String neId, String componentId, String indicatorId, String field) {
        return EncryptUtil.md5AndSha(MANUAL_CODEKEY + neId + componentId + indicatorId + field
                + System.currentTimeMillis());
    }

    /**
     * 获取手动业务告警编码
     * @param businessId 业务ID
     * @param indicatorId 指标ID
     * @return
     */
    public static String getManualBnsAlertCode(String businessId, String indicatorId) {
        return EncryptUtil.md5AndSha(MANUAL_CODEKEY + businessId + indicatorId + System.currentTimeMillis());
    }

    /**
     * 获取手动终端告警编码
     * @param terminalMac mac地址
     * @param ip IP地址
     * @return
     */
    public static String getManualTerminalAlertCode(String terminalMac, String ip) {
        return EncryptUtil.md5AndSha(MANUAL_CODEKEY + terminalMac + ip + System.currentTimeMillis());
    }

    /**
     * 获取手动告警编码
     * @param objectId 告警对象ID
     * @return
     */
    public static String getManualAlertCode(String objectId) {
        return EncryptUtil.md5AndSha(MANUAL_CODEKEY + objectId + System.currentTimeMillis());
    }

    /**
     * 获取IP告警编码
     * @param subnetId 子网ID
     * @param ip 子网IP
     * @param ruleId 策略ID
     * @param ruleType 策略类型
     * @param valRange 阈值范围
     * @param setting 告警设置
     * @return
     */
    public static String getIpAlertCode(String subnetId, String ip, Long ruleId, IpAlertRuleType ruleType,
        String valRange, JSONObject setting) {
        valRange = StringUtils.isBlank(valRange) ? "未知" : valRange;
        valRange = IpAlertRuleType.ip_conflict.equals(ruleType) ? valRange.replace("-", ":").toLowerCase() : valRange;
        return EncryptUtil.md5AndSha(subnetId + ip + ruleId + ruleType.toString() + valRange + setting.toString());
    }

}
