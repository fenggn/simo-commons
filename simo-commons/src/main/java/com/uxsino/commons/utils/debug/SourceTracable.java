package com.uxsino.commons.utils.debug;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;

public interface SourceTracable {

    @JsonIgnore
    @JSONField(serialize = false)
    public SourceInfo getSourceInfo();

    @JsonIgnore
    @JSONField(serialize = false)
    default public void setSourceInfo(SourceInfo info) {
        setSourceInfo(info.source, info.location);
    }

    @JsonIgnore
    @JSONField(serialize = false)
    public void setSourceInfo(String sourceName, String sourceLoaction);

}
