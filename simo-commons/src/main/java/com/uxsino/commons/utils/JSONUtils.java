package com.uxsino.commons.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.alibaba.fastjson.JSONObject;

public class JSONUtils {

    private static Logger logger = LoggerFactory.getLogger(JSONUtils.class);

    public static Double getDouble(JSONObject obj, String key) {
        Double value = null;
        if (obj == null) {
            return value;
        }
        try {
            value = obj.getDouble(key);
        } catch (Exception e) {
            logger.error("JSONObject.getDouble(\"" + key + "\") error! -> " + obj.get(key));
        }
        return value;
    }

    public static double getDoubleValue(JSONObject obj, String key) {
        double value = 0;
        if (obj == null) {
            return value;
        }
        try {
            value = obj.getDoubleValue(key);
        } catch (Exception e) {
            logger.error("JSONObject.getDoubleValue(\"" + key + "\") error! -> " + obj.get(key));
        }
        return value;
    }

    public static Integer getInteger(JSONObject obj, String key) {
        Integer value = null;
        if (obj == null) {
            return value;
        }
        try {
            value = obj.getInteger(key);
        } catch (Exception e) {
            logger.error("JSONObject.getInteger(\"" + key + "\") error! -> " + obj.get(key));
        }
        return value;
    }

    public static int getIntValue(JSONObject obj, String key) {
        int value = 0;
        if (obj == null) {
            return value;
        }
        try {
            value = obj.getIntValue(key);
        } catch (Exception e) {
            logger.error("JSONObject.getIntValue(\"" + key + "\") error! -> " + obj.get(key));
        }
        return value;
    }

    public static Long getLong(JSONObject obj, String key) {
        Long value = null;
        if (obj == null) {
            return value;
        }
        try {
            value = obj.getLong(key);
        } catch (Exception e) {
            logger.error("JSONObject.getLong(\"" + key + "\") error! -> " + obj.get(key));
        }
        return value;
    }

    public static long getLongValue(JSONObject obj, String key) {
        long value = 0;
        if (obj == null) {
            return value;
        }
        try {
            value = obj.getLongValue(key);
        } catch (Exception e) {
            logger.error("JSONObject.getLongValue(\"" + key + "\") error! -> " + obj.get(key));
        }
        return value;
    }

    public static Boolean getBoolean(JSONObject obj, String key) {
        Boolean value = null;
        if (obj == null) {
            return value;
        }
        try {
            value = obj.getBoolean(key);
        } catch (Exception e) {
            logger.error("JSONObject.getBoolean(\"" + key + "\") error! -> " + obj.get(key));
        }
        return value;
    }

    public static boolean getBooleanValue(JSONObject obj, String key) {
        boolean value = false;
        if (obj == null) {
            return value;
        }
        try {
            value = obj.getBooleanValue(key);
        } catch (Exception e) {
            logger.error("JSONObject.getBooleanValue(\"" + key + "\") error! -> " + obj.get(key));
        }
        return value;
    }
}
