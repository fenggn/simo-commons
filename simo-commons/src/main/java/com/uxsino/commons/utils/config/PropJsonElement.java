package com.uxsino.commons.utils.config;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.JsonNode;

public abstract class PropJsonElement implements PropElement {

    protected PropJsonDocument doc;

    protected JsonNode node;

    public PropJsonElement(PropJsonDocument doc, JsonNode node) {
        this.doc = doc;
        this.node = node;
    }

    @Override
    public PropDocument getOwnerDoc() {
        return doc;
    }

    @Override
    public String getSourceLocation() {
        return node.asText();
    }

    @Override
    public String toString() {
        return node.toString();
    }

    @Override
    public String getTagName(String fieldAsTagName) {
        if (StringUtils.isBlank(fieldAsTagName)) {
            fieldAsTagName = "tagName";
        }
        return getProp(fieldAsTagName);
    }

}
