package com.uxsino.commons.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 时间处理工具类
 * 
 *
 */
public class DateTools {

    /**
     * 将字符串转为日期格式
     * @param time
     * 		格式yyyy-MM-dd HH:mm:ss
     * @return
     * @throws ParseException 
     */
    public static Date format(String time) throws ParseException {
        if (time == null || "".equals(time) || "永不过期".equals(time)) {
            return null;
        }
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        date = format.parse(time);
        return date;
    }

    public static Date parse(String time) throws ParseException {
        return parse("yyyy-MM-dd HH:mm:ss");
    }

    public static Date parse(String time, String fmt) throws ParseException {
        if (null == time)
            return null;
        if (null == fmt) {
            fmt = "yyyy-MM-dd HH:mm:ss";
        }
        DateFormat format = new SimpleDateFormat(fmt);
        Date date = null;
        date = format.parse(time);
        return date;
    }

    public static Date formatTime(String time) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 将日期转为字符串
     * @param date
     * @return
     */
    public static String format(Date date) {
        if (date == null) {
            return null;
        }
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(date);
    }

}
