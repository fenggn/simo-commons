package com.uxsino.commons.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/************************************************************
 * 避免算术运算的精度丢失.<br/>
 * 
 * @version isoc Service Platform, 2015年12月28日
 ************************************************************/
public class ArithUtils {
    // 默认就取两位小数
    private static final int DEF_DIV_SCALE = 2;

    /**
     * 格式化设置
     */
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");

    /**
     * 两数相加.<br/>
     * @param v1
     * @param v2
     * @return
     */
    public static double add(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.add(b2).doubleValue();
    }

    /**
     * 两数相减.<br/>
     * @param v1
     * @param v2
     * @return
     */
    public static double sub(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.subtract(b2).doubleValue();
    }

    /**
     * 两数相乘.<br/>
     * @param v1
     * @param v2
     * @return
     */
    public static double mul(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.multiply(b2).doubleValue();
    }

    /**
     * 两数相除.<br/>
     * @param v1 被除数
     * @param v2 除数
     * @return 保留两位小数
     */
    public static double div(double v1, double v2) {
        return div(v1, v2, DEF_DIV_SCALE);
    }

    /**
     * 两数相除.<br/>
     * @param v1 被除数
     * @param v2 除数
     * @param scale 小数位数
     * @return
     */
    public static double div(double v1, double v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException("The scale must be a positive integer or zero");
        }
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static double round(double v, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException("The scale must be a positive integer or zero");
        }
        BigDecimal b = new BigDecimal(Double.toString(v));
        BigDecimal one = new BigDecimal("1");
        return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * 值1与值2的百分占比
     * @param v1 值1
     * @param v2 值2
     * @param scale 精度，保留几位小数
     * @return
     */
    public static String getPersent(double v1, double v2, int scale) {
        double c = div(div(v1, v2, scale + 2) * 100.0, 1, scale);
        String cStr = String.valueOf(c);
        if (cStr.split("\\.")[1].length() < scale) {// 补位
            for (int i = 0; i < scale - 1; i++) {
                cStr += "0";
            }
        }
        return cStr + "%";
    }

    /**
     * Double数值格式化
     * @param val 数值
     * @param defStr 默认显示
     * @return
     */
    public static String formatDouble(Double val, String defStr) {
        defStr = defStr == null ? "--" : defStr;
        if (val == null) {
            return defStr;
        }
        return DECIMAL_FORMAT.format(val);
    }

}
