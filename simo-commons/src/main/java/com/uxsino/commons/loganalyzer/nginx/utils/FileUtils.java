package com.uxsino.commons.loganalyzer.nginx.utils;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileUtils {

    private static Logger logger = LoggerFactory.getLogger(FileUtils.class);

    public static void deleteFile(String filePrefixPattern, String fileName) {
        File file = new File(filePrefixPattern + fileName);
        boolean success = file.delete();
        if (!success) {
            logger.error("failed to delete file {}", filePrefixPattern + fileName);
        }
    }

}
