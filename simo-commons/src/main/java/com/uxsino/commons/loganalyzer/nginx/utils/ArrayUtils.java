package com.uxsino.commons.loganalyzer.nginx.utils;

import java.util.Arrays;

public class ArrayUtils {

    public static void fillTwoDArray(Object[][] arr, Object value) {
        for (int i = 0; i < arr.length; i++) {
            Arrays.fill(arr[i], value);
        }
    }

}
