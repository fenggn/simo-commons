package com.uxsino.commons.loganalyzer.nginx.func;

public interface IModifyData {
    public Object modifyData(Object input);
}
