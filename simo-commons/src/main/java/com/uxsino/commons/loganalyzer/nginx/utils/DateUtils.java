package com.uxsino.commons.loganalyzer.nginx.utils;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;

public class DateUtils {

    public static int getDayIndex(OffsetDateTime recordTime, OffsetDateTime cur) {
        OffsetDateTime todayStart = cur.truncatedTo(ChronoUnit.DAYS);
        long recordSec = recordTime.toEpochSecond();
        long todayStartSec = todayStart.toEpochSecond();
        long diff = (todayStartSec - recordSec) / (24l * 3600l);
        long rest = (todayStartSec - recordSec) % (24l * 3600l);
        return (int) (6l - diff - (rest > 0l ? 1l : 0l));
    }

    public static int getHourIndex(OffsetDateTime recordTime, OffsetDateTime cur) {
        OffsetDateTime curHourStart = cur.truncatedTo(ChronoUnit.HOURS);
        long recordSec = recordTime.toEpochSecond();
        long curHourStartSec = curHourStart.toEpochSecond();
        long diff = (curHourStartSec - recordSec) / 3600l;
        long rest = (curHourStartSec - recordSec) % 3600l;
        return (int) (23l - diff - (rest > 0l ? 1l : 0l));
    }

}
