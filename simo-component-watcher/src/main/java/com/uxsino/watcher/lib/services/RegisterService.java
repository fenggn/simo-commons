// package com.uxsino.watcher.lib.services;
//
// import com.alibaba.fastjson.JSONObject;
// import com.google.common.base.Strings;
// import com.google.common.collect.Maps;
// import com.uxsino.commons.http.Https;
// import com.uxsino.commons.model.NeClass;
// import com.uxsino.watcher.lib.interceptor.ComponentInterceptor;
// import org.apache.http.entity.ByteArrayEntity;
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.scheduling.annotation.Scheduled;
// import org.springframework.stereotype.Service;
// import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
// import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
//
// import javax.sql.DataSource;
// import java.net.URL;
// import java.util.Map;
//
// @Service
// public class RegisterService extends WebMvcConfigurerAdapter {
// private static final Logger LOG = LoggerFactory.getLogger(RegisterService.class);
//
// @Value("${spring.application.name:#{null}}")
// private String app_name;
//
// @Value("${server.port:#{80}}")
// private String app_port;
//
// @Value("${simo.watcher.component-name:#{null}}")
// private String component_name;
//
// @Value("${simo.watcher.ip:127.0.0.1}")
// private String component_ip;
//
// @Value("${simo.watcher.protocols:#{null}}")
// private String component_protocols;
//
// @Value("${simo.watcher.os:#{linux}}")
// private String component_os;
//
// //带http头的地址
// @Value("${simo.watcher.center-url:#{null}}")
// private String watcher_center;
//
//
// //DB
// @Value("${spring.jpa.database:#{null}}")
// private String db_type;
// @Value("${spring.datasource.url:#{null}}")
// private String db_url;
// @Value("${spring.datasource.username:#{null}}")
// private String db_user;
// @Value("${spring.datasource.password:#{null}}")
// private String db_pwd;
//
// //REDIS
// @Value("${spring.redis.password:#{null}}")
// private String redis_pwd;
// @Value("${spring.redis.host:#{null}}")
// private String redis_host;
// @Value("${spring.redis.password:#{null}}")
// private int redis_port;
//
// //MQ
// @Value("${simo.watcher.mq.host:#{null}}")
// private String mq_host;
//
// @Value("${simo.watcher.mq.brokerName:#{null}}")
// private String mq_brokerName;
//
// @Value("${simo.watcher.mq.password:#{null}}")
// private String mq_pwd;
//
// @Value("${simo.watcher.mq.password:#{null}}")
// private String mq_user;
//
// public Map<String, Object> db() {
// Map<String, Object> db = null;
// if(!Strings.isNullOrEmpty(this.db_url)){
// try{
// db = Maps.newHashMap();
// String up = this.db_url.substring(this.db_url.indexOf("//")+2);
// URL uu = new URL("http://"+ up);//辅助解析
// String host = uu.getHost();
// int port = uu.getPort();
// String dbName = uu.getPath().replaceAll("/","");
// db.put("host", host);
// db.put("neClass", "POSTGRESQL".equalsIgnoreCase(this.db_type)?NeClass.postgre.name():NeClass.uxdb.name());
// db.put("user", this.db_user);
// db.put("password", this.db_pwd);
// db.put("dbName", dbName);
// db.put("port", port);
// }catch (Exception e){
// LOG.error("【自监控】[获取组件数据库配置失败]", e);
// }
// }
// return db;
// }
//
// public Map<String, Object> component(){
// Map<String, Object> component = Maps.newHashMap();
// component.put("name", Strings.isNullOrEmpty(this.component_name)?this.app_name:this.component_name);
// component.put("port", app_port);
// component.put("ip", component_ip);
// component.put("os", this.component_os);
// component.put("protocols", this.component_protocols);
// return component;
// }
//
// public Map<String, Object> redis(){
// if(Strings.isNullOrEmpty(this.redis_host)){
// return null;
// }
// Map<String, Object> redis = Maps.newHashMap();
// redis.put("host", this.redis_host);
// redis.put("password", this.redis_pwd);
// redis.put("port", this.redis_port);
//
// return redis;
// }
//
// public Map<String, Object> mq(){
// if(Strings.isNullOrEmpty(this.mq_host)){
// return null;
// }
// Map<String, Object> mq = Maps.newHashMap();
// mq.put("host", this.mq_host);
// mq.put("username", this.mq_user);
// mq.put("password", this.mq_pwd);
// mq.put("brokerName", this.mq_brokerName);
// return mq;
// }
//
//
// @Override
// public void addInterceptors(InterceptorRegistry registry) {
// registry.addInterceptor(new ComponentInterceptor());
// super.addInterceptors(registry);
// }
//
// @Scheduled(fixedRate = 10000)
// public void register() throws Exception{
// Map<String, Object> components = component();
// components.put("db", db());
// components.put("mq", mq());
// components.put("redis", redis());
// try{
// Https.Response resp = Https.of().post(this.watcher_center+"/watcher/register", new
// ByteArrayEntity(JSONObject.toJSONString(components).getBytes("UTF-8")));
// }catch (Exception e){}
// }
//
// @Autowired
// DataSource dataSource;
//
// }
