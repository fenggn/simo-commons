package com.uxsino.watcher.lib.annoation;


import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Business {
    /**
     * 业务名称，不能为空 针对对应的证书进行过滤
     * @return
     */
    String name() default "";

    /**
     * 是否控制业务权限, 设置为false， 表示不进行权限校验，以接口 -> 对象 的优先级进行控制
     * @return
     */
    boolean validate() default true;
}
