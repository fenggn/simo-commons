package com.uxsino.watcher.lib.services;

/**
 * @ClassName IVersion
 * @Description TODO
 * @Author <a href="mailto:royrxc@gmail.com">Ran</a>
 * @Daate 2019/12/17 9:51
 **/

public interface IVersion {
    /**
     * 项目名称
     * @return
     */
    String name();

    /**
     * git 版本记录文件， 默认：git.[project-name].properties
     * @return
     */
    String git();
}
