package com.uxsino.watcher.lib.bean;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class ComponentBean {

    // cpu totalTime
    private String cpuTotalTime;

    // cpu cpuIdleTime 空闲时间
    private String cpuIdleTime;

    // cpu使用率
    private String cpuUsage;

    // 使用中的内存数
    private String memoryCount;

    // 已分配内存
    private String totalMemory;

    // 剩余内存
    private String freeMemory;

    // 当前线程总数
    private String currentThreadCount;

    // 活动线程数
    private String threadCount;

    // 守护线程数
    private String daemonThreadCount;

}
