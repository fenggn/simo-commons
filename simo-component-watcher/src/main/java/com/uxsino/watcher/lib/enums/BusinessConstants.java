package com.uxsino.watcher.lib.enums;

public final class BusinessConstants {

    /**
     * 资产管理
     */
    public static final String CMDB = "cmdb";

    /**
     * 工作台
     */
    public static final String WORKBENCH = "workbench";

    /**
     * 值班管理
     */
    public static final String DUTY = "duty";

    /**
     * 服务台
     */
    public static final String RECEPTION = "reception";

    /**
     * 配置管理
     */
    public static final String SCRIPT = "script";

    /**
     * 系统管理
     */
    public static final String SYSTEM = "system";

    /**
     * 流程管理，工单
     */
    public static final String WORKFLOW = "workflow";

    /**
     * 告警管理
     */
    public static final String ALERT = "alert";

    /**
     * 业务管理
     */
    public static final String BUSINESS = "business";

    /**
     * 知识库
     */
    public static final String KNOWLEDGEBASE = "knowledgebase";

    /**
     * 虚拟化
     */
    public static final String VM = "vm";

    /**
     * 云监控
     */
    public static final String CLOUD = "cloud";

    /**
     * 巡检
     */
    public static final String PARTOL = "patrol";

    /**
     * 资源
     */
    public static final String MONITOR = "monitoring";

    /**
     * 终端管理
     */
    public static final String TERMINAL = "terminal";

    /**
     * IP地址管理
     */
    public static final String IP = "ip";

    /**
     * 策略管理
     */
    public static final String RULEENGINE = "ruleengine";

    /**
     * 拓扑管理
     */
    public static final String TOPO = "topo";

    /**
     * 自愈
     */
    public static final String RECOVERY = "recovery";

    /**
     * 报表
     */
    public static final String REPORT = "report";

    /**
     * 消息中心
     */
    public static final String MSG = "msg";

}
