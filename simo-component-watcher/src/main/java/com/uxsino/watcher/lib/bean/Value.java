package com.uxsino.watcher.lib.bean;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 值表述
 */
@AllArgsConstructor
@Data
public class Value {
    // 键
    private String key;

    // 名
    private String name;

    // 值
    private Object value;

    //单位
    private String suffix;

    public Value(String key, String name, Object value){
        this.key = key;
        this.name = name;
        this.value = value;
    }

    public Value suffix(String suffix){
        this.suffix = suffix;
        return this;
    }

    public static Value of(String key, String name, Object value) {
        return new Value(key, name, value);
    }
}
