package com.uxsino.watcher.lib.services;

/**
 * 系统证书发生更改，通知程序包进行处理对应逻辑。
 */
public interface LicChanged {
    public void accept();
}
