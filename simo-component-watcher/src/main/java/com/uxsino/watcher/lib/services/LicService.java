package com.uxsino.watcher.lib.services;

import com.uxsino.commons.baseclass.Constants;
import com.uxsino.commons.cache.Cache;
import com.uxsino.commons.lic.AppProperty;
import org.springframework.stereotype.Service;

@Service
public class LicService {

    public AppProperty get(){
        return Cache.get(Constants.LIC_INFO);
    }

}
