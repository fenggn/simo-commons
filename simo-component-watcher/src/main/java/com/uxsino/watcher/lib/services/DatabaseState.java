package com.uxsino.watcher.lib.services;

/**
 * @ClassName DatabaseState
 * @Description TODO
 * @Author <a href="mailto:royrxc@gmail.com">Ran</a>
 * @Daate 2019/11/16 16:04
 **/
public interface DatabaseState {
    /**
     * 最大连接数
     * @return
     */
    long maxActive();

    /**
     * 活动链接数
     * @return
     */
    long active();

    /**
     * 正在运行的sql数量
     */
    long runningSqlCount();


    Object state();
}
