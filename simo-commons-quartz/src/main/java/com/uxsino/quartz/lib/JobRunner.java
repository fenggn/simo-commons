package com.uxsino.quartz.lib;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @Author: jane
 * @Date: 2019/11/12 下午 4:00
 */
public class JobRunner implements Job {
    @Override
    public void execute(JobExecutionContext context) {
        com.uxsino.quartz.lib.Job<?> job = (com.uxsino.quartz.lib.Job<?>) context.getMergedJobDataMap().get("scheduleJob");;
        job.setNextFireTime(context.getNextFireTime());
        job.run();
    }
}
