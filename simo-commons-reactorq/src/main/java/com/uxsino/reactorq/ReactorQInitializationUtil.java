package com.uxsino.reactorq;

import org.apache.activemq.broker.BrokerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReactorQInitializationUtil {
    public static Logger logger = LoggerFactory.getLogger(ReactorQInitializationUtil.class);

    public static BrokerService startBroker(Boolean useEmbedded, String brokerUrl, String stompUrl, String brokerName,
        String websocketUrl) {

        if (!useEmbedded || ConnectionStatusMonitor.isConnected(brokerUrl)) {
            return null;
        }
        BrokerService broker = new BrokerService();

        try {
            // configure the broker
            broker.addConnector(brokerUrl);
            broker.setBrokerName(brokerName);
            broker.setUseJmx(true);

            if (stompUrl != null) {
                logger.info("stomp enabled on: {}", stompUrl);
                broker.addConnector(stompUrl);
            }
            if (websocketUrl != null) {
                logger.info("websocket enabled on: {}", websocketUrl);
                broker.addConnector(websocketUrl);
            }

            broker.start();
        } catch (Exception e) {
            logger.error("Error starting activemq broker.", e);
        }
        return broker;
    }

}
