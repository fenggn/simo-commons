package com.uxsino.reactorq;

import java.io.IOException;
import java.net.URI;
import java.security.cert.X509Certificate;

import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.transport.FutureResponse;
import org.apache.activemq.transport.ResponseCallback;
import org.apache.activemq.transport.Transport;
import org.apache.activemq.transport.TransportListener;
import org.apache.activemq.wireformat.WireFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectionStatusMonitor implements TransportListener, Transport {
    private static final Logger log = LoggerFactory.getLogger(ConnectionStatusMonitor.class);

    @Override
    public void onCommand(Object command) {
        log.debug("MQ Command : {}", command);
    }

    @Override
    public void onException(IOException exception) {
        log.error("MQ Exception : {}", exception.getMessage());
    }

    @Override
    public void transportInterupted() {
        log.error("MQ Transport Interuption.");
    }

    @Override
    public void transportResumed() {
        log.info("MQ Transport Resumption.");
    }

    @Override
    public void start() throws Exception {

    }

    @Override
    public void stop() throws Exception {

    }

    @Override
    public void oneway(Object command) throws IOException {

    }

    @Override
    public FutureResponse asyncRequest(Object command, ResponseCallback responseCallback) throws IOException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object request(Object command) throws IOException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object request(Object command, int timeout) throws IOException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TransportListener getTransportListener() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setTransportListener(TransportListener commandListener) {
        // TODO Auto-generated method stub

    }

    @Override
    public <T> T narrow(Class<T> target) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getRemoteAddress() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isFaultTolerant() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isDisposed() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isConnected() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isReconnectSupported() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isUpdateURIsSupported() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void reconnect(URI uri) throws IOException {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateURIs(boolean rebalance, URI[] uris) throws IOException {
        // TODO Auto-generated method stub

    }

    @Override
    public int getReceiveCounter() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public X509Certificate[] getPeerCertificates() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setPeerCertificates(X509Certificate[] certificates) {
        // TODO Auto-generated method stub

    }

    @Override
    public WireFormat getWireFormat() {
        // TODO Auto-generated method stub
        return null;
    }

    public static boolean isConnected(String brokerUrl) {
        ConnectionStatusMonitor transportListenerObject = new ConnectionStatusMonitor();
        boolean connected = false;
        try {
            ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(brokerUrl);
            // set transport listener so that active MQ start is notified.
            // factory.setWatchTopicAdvisories(false);
            factory.setTransportListener(transportListenerObject);
            ActiveMQConnection connection = (ActiveMQConnection) factory.createConnection();
            // This will throw error if activeMQ is not running.
            connection.setClientID("my_client_id");
            connected = true;
        } catch (JMSException ex) {
            if (ex.getLinkedException() instanceof IOException) {
                // ActiveMQ is not running. Do some logic here.
                // use the TransportListener to restart the activeMQ connection
                // when activeMQ comes back up.
            } else {
                // Something seriously went wrong with the factory or connection
                // creation. Abort the process here, as nothing can be done.
                // Log the error and troubleshoot.
            }
        }
        return connected;
    }
}
