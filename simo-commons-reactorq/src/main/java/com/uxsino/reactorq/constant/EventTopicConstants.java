package com.uxsino.reactorq.constant;

public interface EventTopicConstants {

    // 采集器开关
    public static final String SIMO_NOTIFY_COLLECTOR_SWITCHER = "SIMO_NOTIFY_COLLECTOR_SWITCHER";

    /**
     * 系统 证书变更
     */
    public static final String SIMO_LICENSE_NOTIFY = "SIMO_LICENSE_NOTIFY";

    public static final String SIMO_LOAD_INDICATORS = "SIMO_LOAD_INDICATORS";

    // 向 collector添加巡检任务
    public static final String SIMO_PATROL_TASK = "SIMO_PATROL_TASK";

    public static final String SIMO_PATROL_RESULT = "SIMO_PATROL_RESULT";

    // collector 查询结果发往rule
    public static final String SIMO_IND_V_TO_RULE = "SIMO_IND_V_TO_RULE";

    // 终端告警发往Alert
    public static final String SIMO_ALERT_TERMINAL = "SIMO_ALERT_TERMINAL";

    // 终端管理变更结果发往rule
    public static final String SIMO_IPMACPORT_TO_RULE = "SIMO_IPMACPORT_TO_RULE";

    // collector 查询结果发往monitor
    public static final String SIMO_IND_V_TO_MONITOR = "SIMO_IND_V_TO_MONITOR";

    // collector 广播手动采集的指标数据
    public static final String SIMO_IND_V = "SIMO_IND_V";

    // monitor 向 collector 发送临时网元，用于发现
    public static final String SIMO_DISCOVER_NES = "SIMO_DISCOVER_NES";

    // collector 向 monitor 发送发现结果
    public static final String SIMO_DISCOVER_RESULT = "SIMO_DISCOVER_RESULT";

    // monitor 向 collector 发送网元类型定义
    public static final String SIMO_NECLASS = "SIMO_NECLASS";

    // 向collector发送网元变更
    public static final String SIMO_NES_TO_COLLECTOR = "SIMO_NES_TO_COLLECTOR";

    public static final String SIMO_EMPLOYEE_TO_RULEENGINE = "SIMO_EMPLOYEE_TO_RULEENGINE";

    /** 告警发送的队列 */
    public static final String SIMO_SEND_ALERT = "SIMO_SEND_ALERT";

    public static final String SIMO_VIDEO_STATE = "SIMO_VIDEO_STATE";

    public static final String SIMO_VIDEO_BYTE = "SIMO_VIDEO_BYTE";

    /** 链路告警发送的队列 **/
    public static final String SIMO_SEND_LINK_ALERT = "SIMO_SEND_LINK_ALERT";

    /** 业务告警发送的队列 */
    public static final String SIMO_SEND_BUSINESS_ALERT = "SIMO_SEND_BUSINESS_ALERT";

    /** IP告警发送的队列 */
    public static final String SIMO_SEND_IP_ALERT = "SIMO_SEND_IP_ALERT";

    public static final String SIMO_AGENCY_EVENT = "SIMO_AGENCY_EVENT";

    public static final String SIMO_MC_TO_WORKFLOW = "SIMO_MC_TO_WORKFLOW";

    public static final String SIMO_RULEENGIN_TO_PATROL = "SIMO_RULEENGIN_TO_PATROL";

    public static final String SIMO_PATROL_TO_MONITOR = "SIMO_PATROL_TO_MONITOR";

    /** 实时告警推送的Topic消息 */
    public static final String SIMO_TOPIC_ALERT_TO_SOCKET = "SIMO_TOPIC_ALERT_TO_SOCKET";

    /** 监控对象运行状态的更新 */
    public static final String SIMO_RUNSTATUS_TO_MC = "SIMO_RUNSTATUS_TO_MC";

    /** 监控指标数据记录运行状态的更新 */
    public static final String SIMO_IND_V_RUNSTATUS_TO_DATACENTER = "SIMO_IND_V_RUNSTATUS_TO_DATACENTER";

    public static final String SIMO_COLLECTOR_TO_MONITOR = "SIMO_COLLECTOR_TO_MONITOR";

    public static final String SIMO_MONITOR_DISCOVERY_ALL = "SIMO_MONITOR_DISCOVERY_ALL";

    /** 用于其他模块向mc发送更新menu的消息 */
    public static final String SIMO_UPDATE_MENU = "SIMO_UPDATE_MENU";

    /** 用于发送审计日志 **/
    public static final String SIMO_AUDIT_LOG = "SIMO_AUDIT_LOG";

    // /** MC发送domains信息 **/
    // public static final String SIMO_LOAD_DOMAINS = "SIMO_LOAD_DOMAINS";

    /** 用于发送用户信息 */
    public static final String SIMO_SEND_USER = "SIMO_SEND_USER";

    /** 用于策略校验结果至monitor,以便用于修改NE状态 */
    public static final String SIMO_INDSTATUS_TO_MONITOR = "SIMO_INDSTATUS_TO_MONITOR";

    /** 用于发送资syslog */
    public static final String SIMO_SYSLOG_SERVER = "SIMO_SYSLOG_SERVER";

    /** syslog 告警 */
    public static final String SIMO_ALERT_SYSLOG = "SIMO_ALERT_SYSLOG";

    /** 告警策略 */
    public static final String SIMO_ALERT_STRATEGY = "SIMO_ALERT_STRATEGY";

    /** 用于发送业务信息给告警模块 */
    public static final String SIMO_BUSINESS_TO_ALERT = "SIMO_BUSINESS_TO_ALERT";

    /** 用于发送 snmp trap 给 monitor */
    public static final String SIMO_SNMP_TRAP_TO_MONITOR = "SIMO_SNMP_TRAP_TO_MONITOR";

    /** 用于 collector 发送 可用性检测结果 给 monitor */
    public static final String SIMO_AVAILABILITY_TO_MONITOR = "SIMO_AVAILABILITY_TO_MONITOR";

    /** 用于发送 snmptrap 告警给 alert */
    public static final String SIMO_ALERT_SNMPTRAP = "SIMO_ALERT_SNMPTRAP";

    /** 资源健康度策略 */
    public static final String SIMO_NE_HEALTH_STRATEGY = "SIMO_NE_HEALTH_STRATEGY";

    /** 第三方告警发送的队列 */
    public static final String SIMO_THIRD_PARTY_ALERT = "SIMO_THIRD_PARTY_ALERT";

    /** 系统告警发送的队列 */
    public static final String SIMO_SEND_SYSTEM_ALERT = "SIMO_SEND_SYSTEM_ALERT";

    /** 发送collector状态变化给monitor */
    public static final String SIMO_SEND_COLLECTOR_STATUS = "SIMO_SEND_COLLECTOR_STATUS";

    /** collector 发送switch ip mac给 monitor */
    public static final String SIMO_SWITCH_IP_MAC_TO_MONITOR = "SIMO_SWITCH_IP_MAC_TO_MONITOR";

    /** collector 发送IP采集结果给 monitor */
    public static final String SIMO_COLLECTOR_IP_TO_MONITOR = "SIMO_COLLECTOR_IP_TO_MONITOR";

    // monitor 向 collector 发送终端发现配置
    public static final String SIMO_IPMACPORT_TO_COLLECTOR = "SIMO_IPMACPORT_TO_COLLECTOR";

    /** monitor 向 collector 发送子网IP采集配置 */
    public static final String SIMO_SUBNET_TO_COLLECTOR = "SIMO_SUBNET_TO_COLLECTOR";

    /** 报表模块离线pdf生成成功后，发送至MC,触发Socket信息发送 */
    public static final String SIMO_REPORT_DOWNLOAD_TO_MC = "SIMO_REPORT_DOWNLOAD_TO_MC";

    public static final String SIMO_SOCKET_MSG = "SIMO_SOCKET_MSG";

    /** 发送根源策略到alert */
    public static final String SIMO_ROOT_STRATEGY = "SIMO_ROOT_STRATEGY";

    /** 发送资源到business */
    public static final String SIMO_NE_TO_BUSINESS = "SIMO_NE_TO_BUSINESS";

    /** 发送业务相关的资源到monitor */
    public static final String SIMO_BNS_NES_TO_MONITOR = "SIMO_BNS_NES_TO_MONITOR";

    /** 发送更新的资源指标值到business */
    public static final String SIMO_NE_INDVAL_UPDATE_TO_BUSINESS = "SIMO_NE_INDVAL_UPDATE_TO_BUSINESS";

    public static final String SIMO_ALERT_RECOVERY = "SIMO_ALERT_RECOVERY";

    public static final String SIMO_COLLECTOR_REQUEST_RELOAD_NE = "SIMO_COLLECTOR_REQUEST_RELOAD_NE";

    public static final String SIMO_LEADERVIEW_API = "SIMO_LEADERVIEW_API";

    /** 需要发送的消息 */
    public static final String UXSINO_MSG = "UXSINO-MSG";

    /** 域信息变更广播 */
    public static final String SIMO_DOMAIN_NOTIFY = "SIMO_DOMAIN_NOTIFY";

    /** 用户删除-工作移交广播 */
    public static final String SIMO_HANDOVER_NOTIFY = "SIMO_HANDOVER_NOTIFY";

    /** 资源域变更广播 */
    public static final String SIMO_NE_DOMAIN_NOTIFY = "SIMO_NE_DOMAIN_NOTIFY";

}
