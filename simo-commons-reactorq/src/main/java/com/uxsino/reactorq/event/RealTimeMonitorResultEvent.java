package com.uxsino.reactorq.event;

import com.alibaba.fastjson.JSONObject;

/**
 * @ClassName RealTimeMonitorResultEvent
 * @Description TODO
 * @Author LXH
 * @Daate 2020/7/14 9:19
 **/
@UxEvent(name = "REAL_TIME_MONITOR_RESULT")
public class RealTimeMonitorResultEvent extends Event{

    public JSONObject json;

}
