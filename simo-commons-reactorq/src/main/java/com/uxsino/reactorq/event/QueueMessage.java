package com.uxsino.reactorq.event;

import com.alibaba.fastjson.JSON;
import com.uxsino.reactorq.commons.IMessageWithReceiver;

public class QueueMessage implements IMessageWithReceiver {

    public JSON queueObj;

    public String receiverId;

    @Override
    public String getReceiverId() {
        return receiverId;
    }
}
