package com.uxsino.reactorq.event;

import java.util.List;

import com.uxsino.commons.logicSelector.GROUPBY_TYPE;
import com.uxsino.reactorq.model.SelectorConf;

/**
 * 
 * upload a batch file
 *
 */
@UxEvent(name = "BATCH_FILE_UPLOAD")
public final class BatchFileUploadCmd extends Event {

    /**
     * a unique string id for each batch
     */
    public String batchId;

    /**
     * the file name 
     */
    public String fileName;

    /**
     * the batch file
     */
    public String batchTemplate;

    /**
     * in which directory should the file.
     */
    public String filePath;

    /**
     * 上传文件协议
     */
    public String[] uploadProtocols;

    public GROUPBY_TYPE groupBy;

    public List<SelectorConf> selectorConf;

}
