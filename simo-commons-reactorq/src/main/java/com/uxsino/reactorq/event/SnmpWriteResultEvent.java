package com.uxsino.reactorq.event;

@UxEvent(name = "SNMP_WRITE_RESULT_EVENT")
public class SnmpWriteResultEvent extends Event {

    public String neId;

    public boolean succeed;

    public String info;
}
