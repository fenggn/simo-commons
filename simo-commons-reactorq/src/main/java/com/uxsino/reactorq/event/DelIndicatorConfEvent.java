package com.uxsino.reactorq.event;

import java.util.List;

@UxEvent(name = "DEL_INDICATOR_CONF")
public class DelIndicatorConfEvent extends Event {
    public List<String> indicatorNames;
}
