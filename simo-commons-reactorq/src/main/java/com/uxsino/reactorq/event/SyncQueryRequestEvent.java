package com.uxsino.reactorq.event;

import java.util.List;

@UxEvent(name = "SYNC_QUERY_REQUEST_EVENT")
public class SyncQueryRequestEvent extends Event {

    public List<String> indicatorNameList;

    public String collectorId;

    public Long timeoutMillis;

    public String neId;
}
