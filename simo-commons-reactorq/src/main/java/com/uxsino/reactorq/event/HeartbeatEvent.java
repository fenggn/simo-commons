package com.uxsino.reactorq.event;

/**
 * heartbeat event for simo modules
 * 
 * 
 *
 */
@UxEvent(name = "MODULE_HEARTBEAT")
public class HeartbeatEvent extends Event {
    public String module;

    public String id;

    /**
     * Time the event is created
     */
    public long heartbeatMillis;

}
