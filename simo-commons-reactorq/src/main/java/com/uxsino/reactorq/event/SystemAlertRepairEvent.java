package com.uxsino.reactorq.event;

@UxEvent(name = "SYSTEM_ALERT_REPAIR")
public final class SystemAlertRepairEvent extends Event {

    public String serviceId;

    public String indicatorId;

}
