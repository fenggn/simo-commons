package com.uxsino.reactorq.event;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

@UxEvent(name = "ENTITY_UPDATE")
public final class NEUpdateEvent extends Event {

    // clean database before update
    public boolean reload;

    public List<String> drop = new ArrayList<>();

    public JSONArray add = new JSONArray();

    public JSONArray update = new JSONArray();

    public JSONObject params = new JSONObject();

}
