package com.uxsino.reactorq.event;

import com.alibaba.fastjson.JSONObject;

@UxEvent(name = "SIMO_DEVICE_MODEL_RESULT")
public class DeviceModelResultEvent extends Event {

    private JSONObject msg;

    public JSONObject getMsg() {
        return msg;
    }

    public void setMsg(JSONObject msg) {
        this.msg = msg;
    }

}
