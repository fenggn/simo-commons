package com.uxsino.reactorq.event;

@UxEvent(name = "SIMO_DEVICE_MODEL")
public class DeviceModelEvent extends Event {

    private String neClass;

    private String venderId;

    public String getNeClass() {
        return neClass;
    }

    public void setNeClass(String neClass) {
        this.neClass = neClass;
    }

    public String getVenderId() {
        return venderId;
    }

    public void setVenderId(String venderId) {
        this.venderId = venderId;
    }

}
