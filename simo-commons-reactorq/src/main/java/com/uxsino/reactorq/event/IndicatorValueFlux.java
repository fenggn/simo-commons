package com.uxsino.reactorq.event;

import javax.jms.JMSException;

import com.uxsino.reactorq.commons.ReactorQFactory;
import com.uxsino.reactorq.constant.EventTopicConstants;
import com.uxsino.reactorq.model.IndicatorValue;

import reactor.core.publisher.Flux;

/**
 * To create a TopicFlux<IndicatorValue>
 * 
 *
 */
public class IndicatorValueFlux {

    // To create a TopicFlux<IndicatorValue> from given brokerURL
    public static Flux<IndicatorValue> createJms(ReactorQFactory factory) throws JMSException {
        return factory.<IndicatorValue> createTopicFlux(EventTopicConstants.SIMO_IND_V_TO_RULE, IndicatorValue.class);
    }

    public static Flux<IndicatorValue> createJms(ReactorQFactory factory, String topic) throws JMSException {
        return factory.<IndicatorValue> createTopicFlux(topic, IndicatorValue.class);
    }

}
