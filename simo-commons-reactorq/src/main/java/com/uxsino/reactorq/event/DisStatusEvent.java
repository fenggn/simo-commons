package com.uxsino.reactorq.event;

@UxEvent(name = "DIS_STATUS")
public class DisStatusEvent extends Event {

    public String collectorId;

    public String getCollectorId() {
        return collectorId;
    }

    public void setCollectorId(String collectorId) {
        this.collectorId = collectorId;
    }

}
