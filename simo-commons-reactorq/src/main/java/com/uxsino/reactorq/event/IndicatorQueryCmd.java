package com.uxsino.reactorq.event;

import com.uxsino.reactorq.commons.IMessageWithReceiver;

@UxEvent(name = "QUERY_IND")
public class IndicatorQueryCmd extends Event implements IMessageWithReceiver {

    // entity Id
    public String neId;

    // indicator names to query
    public String[] indicators;

    // unique id for each query request
    public String queryId;

    // message topic name to return value

    public String valueTopicName;

    public String collectorId;

    @Override
    public String getReceiverId() {
        return collectorId;
    }
}
