package com.uxsino.reactorq.event;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;

public interface IEvent {
    String getFrom();

    void setFrom(String from);

    @JsonProperty("event")
    @JSONField(name = "event")
    String getEventName();

    @JsonProperty("event")
    @JSONField(name = "event")
    default void setEventName(String s) {
    }
}
