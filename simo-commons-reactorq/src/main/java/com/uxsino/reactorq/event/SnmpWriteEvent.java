package com.uxsino.reactorq.event;

import java.util.Map;

import com.uxsino.commons.enums.SnmpWriteType;

@UxEvent(name = "SNMP_WRITE_EVENT")
public class SnmpWriteEvent extends Event {

    public SnmpWriteType writeType;

    public String neId;

    public Map<String, String> info;

}
