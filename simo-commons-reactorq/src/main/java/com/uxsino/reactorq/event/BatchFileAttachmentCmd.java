package com.uxsino.reactorq.event;

import java.io.InputStream;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.uxsino.reactorq.commons.IWithAttachmentStream;

@UxEvent(name = "BATCH_ATTACH")
public class BatchFileAttachmentCmd extends Event implements IWithAttachmentStream {

    /**
     * a unique string id for each batch
     */
    public String batchId;

    public String filePath;

    private InputStream attachement;

    @Override
    public void setAttachement(InputStream attachement) {
        this.attachement = attachement;
    }

    @JsonIgnore
    @JSONField(serialize = false)
    @Override
    public InputStream getAttachement() {
        return attachement;
    }

}
