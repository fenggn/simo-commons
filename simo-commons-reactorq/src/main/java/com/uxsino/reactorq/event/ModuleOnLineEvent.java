package com.uxsino.reactorq.event;

@UxEvent(name = "MODULE_ONLINE")
public final class ModuleOnLineEvent extends Event {
    public String Module;

    public String id;

    public long onLineTimeMillis;
}
