package com.uxsino.reactorq.event;

public enum CmdEventType {
                          OPEN_CONN,
                          CLOSE_CONN,
                          EXEC_CMD
}
