package com.uxsino.reactorq.event;

import java.util.List;

import com.uxsino.reactorq.commons.IFlowItem;
import com.uxsino.reactorq.model.IndicatorValue;

@UxEvent(name = "SYNC_QUERY_RES_EVENT")
public class SyncQueryResEvent extends Event {

    public List<IndicatorValue> indValList;

    public IFlowItem.STATUS status;

}
