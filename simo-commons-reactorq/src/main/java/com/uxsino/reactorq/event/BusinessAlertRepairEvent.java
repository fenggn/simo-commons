package com.uxsino.reactorq.event;

@UxEvent(name = "BUSINESS_ALERT_REPAIR")
public final class BusinessAlertRepairEvent extends Event {

    public String businessIds;

    public String indicatorName;

}
