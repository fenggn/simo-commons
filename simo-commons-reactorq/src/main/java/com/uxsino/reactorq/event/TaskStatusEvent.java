package com.uxsino.reactorq.event;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.uxsino.reactorq.commons.IFlowItem;

@UxEvent(name = "TASK_STATUS")
public class TaskStatusEvent extends Event {

    public IFlowItem.STATUS status;

    public String taskId;

    public String seq;

    @JsonProperty("t")
    @JSONField(name = "t")
    public long statusTimeMillis;

    public String msg;

}
