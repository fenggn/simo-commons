package com.uxsino.reactorq.event;

@UxEvent(name = "DIS_STATUS_RESULT")
public class DisStatusResultEvent extends Event {

    public boolean discovering;

    public String batch;

    // 发现结果滚动条展示的发现进度信息；
    public String disProcess;

    public String collectorId;
}
