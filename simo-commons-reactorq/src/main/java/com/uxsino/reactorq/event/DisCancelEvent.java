package com.uxsino.reactorq.event;

@UxEvent(name = "SIMO_DIS_CANCEL")
public class DisCancelEvent extends Event {

    private String msg;

    private String collectorId;

    public DisCancelEvent() {
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCollectorId() {
        return collectorId;
    }

    public void setCollectorId(String collectorId) {
        this.collectorId = collectorId;
    }

}
