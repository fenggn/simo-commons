package com.uxsino.reactorq.event;

@UxEvent(name = "CUSTOM_INDICATOR")
public class CustomIndicatorEvent extends Event {
    public String customIndicator;

    public String customQuery;

    public OPERATION_TYPE operationType;

    public boolean reload = false;

    public enum OPERATION_TYPE {
                                create,
                                update,
                                delete
    }

}
