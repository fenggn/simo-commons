package com.uxsino.reactorq.event;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.uxsino.commons.logicSelector.PropProxy;

/**
 * base class to implement an {@link IEvent}
 * 
 *
 */
public abstract class Event implements IEvent {
    // from which node this event is sent
    @JsonIgnore
    @JSONField(serialize = false)
    private String from;

    /**
     * where should server to response to? 
     * the object type depends on what kind of transport is binded.
     * in case of JMS, it is a queue or topic  
     */
    @JsonIgnore
    @JSONField(serialize = false)
    public Object replyTo;

    @Override
    public String getFrom() {
        return from;
    }

    @Override
    public void setFrom(String from) {
        this.from = from;
    }

    @JsonIgnore
    @JSONField(serialize = false)
    private String eventName;

    private Map<String, String> properties;

    public Event() {
        eventName = getEventName(this.getClass());
        properties = new HashMap<String, String>();
    }

    @Override
    public String getEventName() {
        return eventName;
    }

    public static String getEventName(Class<? extends IEvent> cls) {
        UxEvent annotation = cls.getAnnotation(UxEvent.class);
        return annotation == null ? cls.getName() : annotation.name();

    }

    @JsonAnyGetter
    private Map<String, String> getProperties() {
        return properties;
    }

    @PropProxy
    public String getProperty(String propName) {
        return properties.get(propName);
    }

    @JsonAnySetter
    public void setProperty(String propName, String value) {
        properties.put(propName, value);
    }
}
