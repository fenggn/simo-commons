package com.uxsino.reactorq.event;

@UxEvent(name = "CONN_TEST")
public class ConnectionTestEvent extends Event {

    public String id;

    public String protocolName;

    public String neClassName;

    public String version;

    // public String cmd;

    public String targetStr;

    // public String resultStrStart;
}
