package com.uxsino.reactorq.event;

/**
 * @ClassName RealTimeMonitorEvent
 * @Description TODO
 * @Author LXH
 * @Daate 2020/7/13 17:26
 **/
@UxEvent(name = "REAL_TIME_MONITOR")
public class RealTimeMonitorEvent extends Event{

    public String neId;

    public String sql;
}
