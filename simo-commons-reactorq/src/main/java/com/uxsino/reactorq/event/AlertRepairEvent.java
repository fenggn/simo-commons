package com.uxsino.reactorq.event;

@UxEvent(name = "ALERT_REPAIR")
public final class AlertRepairEvent extends Event {
    public String neId;

    public String indicatorId;

    public String alertOrigin;
}
