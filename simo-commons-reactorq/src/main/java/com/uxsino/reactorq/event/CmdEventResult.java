package com.uxsino.reactorq.event;

@UxEvent(name = "CMD_RESULT_EVENT")
public class CmdEventResult extends Event {
    public String sessionId;

    public CmdEventType eventType;

    public String result;

    public boolean succeed;
}
