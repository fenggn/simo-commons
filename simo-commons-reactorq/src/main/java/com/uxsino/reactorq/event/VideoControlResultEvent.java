package com.uxsino.reactorq.event;

@UxEvent(name = "VIDEO_CONTROL_RESULT")
public class VideoControlResultEvent extends Event {

    private boolean support = true;

    private boolean result = false;

    public boolean isSupport() {
        return support;
    }

    public void setSupport(boolean support) {
        this.support = support;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
