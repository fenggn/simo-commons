package com.uxsino.reactorq.event;

import com.alibaba.fastjson.JSONObject;

@UxEvent(name = "VIDEO_CONTROL")
public class VideoControlEvent extends Event {

    public String neId;

    public ControlType type;

    public JSONObject json;

    public enum ControlType {
        PTZ, EDIT, PREVIEW
    }
}
