package com.uxsino.reactorq.event;

@UxEvent(name = "SIMO_DIS_RESULT")
public class DisResultEvent extends Event {

    private MSG_TYPE type;

    private String msg;

    private boolean finished;

    private String batch;

    public DisResultEvent(String batch) {
        this.batch = batch;
        this.finished = false;
    }

    public DisResultEvent() {
        this.finished = false;
    }

    public DisResultEvent(String batch, MSG_TYPE type, String msg) {
        this.type = type;
        this.msg = msg;
        this.finished = false;
        this.batch = batch;
    }

    public DisResultEvent(String batch, MSG_TYPE type, String msg, boolean finished) {
        this.type = type;
        this.msg = msg;
        this.finished = finished;
        this.batch = batch;
    }

    public MSG_TYPE getType() {
        return type;
    }

    public void setType(MSG_TYPE type) {
        this.type = type;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public enum MSG_TYPE {
                          ne,
                          links,
                          log,
                          ind,
                          node,
                          process
    }
}
