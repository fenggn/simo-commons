package com.uxsino.reactorq.event;

import java.util.List;

import com.uxsino.commons.logicSelector.GROUPBY_TYPE;
import com.uxsino.reactorq.model.PolicyItem;
import com.uxsino.reactorq.model.SelectorConf;

@UxEvent(name = "PATROL_POLICY_UPDATE")
public class UpdatePatrolPolicyEvent extends Event {

    public String id;

    public GROUPBY_TYPE groupBy;

    public List<SelectorConf> selectorConf;

    public List<PolicyItem> items;
}
