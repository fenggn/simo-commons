package com.uxsino.reactorq.event;

import java.util.Date;

@UxEvent(name = "DATA_CLEANING")
public final class DataCleaningEvent extends Event {

    public String serviceId;

    public Date timePoint;

}
