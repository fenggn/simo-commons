package com.uxsino.reactorq.event;

import java.util.Random;

@UxEvent(name = "SIMO_DIS_REQUEST")
public class DisRequestEvent extends Event {

    private DIS_TYPE type;

    private String msg;

    private String batch;

    private int timeout;

    private boolean extend;

	private boolean usePing;

	private boolean quickDis;

    private String collectorId;

    public DisRequestEvent() {
		batch = System.currentTimeMillis() + "" + new Random().nextLong();
		this.extend = true;
    }

	public enum DIS_TYPE {
		one, all, subnet, more, netSegment, repatrol
	}

    public DIS_TYPE getType() {
        return type;
    }

    public void setType(DIS_TYPE type) {
        this.type = type;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public int getTimeout() {
        return timeout;
    }

	public void setTimeout(int timeout) {
		this.timeout = timeout;
    }

    public boolean isExtend() {
        return extend;
    }

    public void setExtend(boolean extend) {
        this.extend = extend;
    }

	public boolean isUsePing() {
		return usePing;
	}

	public void setUsePing(boolean usePing) {
		this.usePing = usePing;
	}

	public boolean isQuickDis() {
		return quickDis;
	}

	public void setQuickDis(boolean quickDis) {
		this.quickDis = quickDis;
	}

    public String getCollectorId() {
		return collectorId;
    }

    public void setCollectorId(String collectorId) {
        this.collectorId = collectorId;
    }

}
