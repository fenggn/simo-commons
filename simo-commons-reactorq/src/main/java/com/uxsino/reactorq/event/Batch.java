package com.uxsino.reactorq.event;

import java.util.List;

import com.uxsino.commons.logicSelector.GROUPBY_TYPE;
import com.uxsino.reactorq.model.SelectorConf;

@UxEvent(name = "BATCH_CMD")
public class Batch extends Event {
    public String batchId;

    public String execCmd;

    public String[] protocols;

    public GROUPBY_TYPE groupBy;

    public List<SelectorConf> selectorConf;
}
