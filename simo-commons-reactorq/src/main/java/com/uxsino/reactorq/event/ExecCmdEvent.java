package com.uxsino.reactorq.event;

@UxEvent(name = "EXEC_CMD_EVENT")
public class ExecCmdEvent extends Event {
    public String sessionId;

    public CmdEventType eventType;

    public String protocolName;

    public String protocolConf;

    public String neId;

    public String cmd;
}
