package com.uxsino.reactorq.event;

@UxEvent(name = "CONN_TEST_RESULT")
public class ConnectionTestResultEvent extends Event {
    public String id;

    public boolean result;
}
