package com.uxsino.reactorq.commons;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * marks an object type sent by {@link JMSSubscriber} that contains receiver id with it. 
 * subscriber will set the receiver id accordingly
 *
 */
public interface IMessageWithReceiver {

    /**
     * return the receiver id
     * @return receiver id
     */
    @JsonIgnore
    @JSONField(serialize = false)
    String getReceiverId();

}
