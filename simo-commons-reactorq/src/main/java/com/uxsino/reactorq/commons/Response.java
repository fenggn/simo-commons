package com.uxsino.reactorq.commons;

/***
 * 
 * response for a remote mq call
 *
 */
public class Response {
    public static final int STATUS_SUCCESS = 1;

    public static final int STATUS_ERROR = 0;

    public int status;

    // the returned value encoded in json
    public String j;
}
