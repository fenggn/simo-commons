package com.uxsino.reactorq.commons;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;

public interface IFlowItem {
    public enum STATUS {
                        START,
                        RUNNING,
                        END_SUCCESS,
                        END_ERROR,
                        END_TIMEOUT;
        public boolean isEnd() {
            return this == END_SUCCESS || this == END_ERROR || this == END_TIMEOUT;
        }
    };

    // reserved entity name for collector itself on which a task is running
    public static final String FLOW_CONTROL = "#flow_control";

    @JsonIgnore
    @JSONField(serialize = false)
    public boolean isInternal();

//    @JsonIgnore
//    @JSONField(serialize = false)
//    public boolean isEnded();

    @JsonIgnore
    @JSONField(serialize = false)
    public STATUS getStatus();
}
