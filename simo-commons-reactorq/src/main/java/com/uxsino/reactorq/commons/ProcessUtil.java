package com.uxsino.reactorq.commons;

import java.util.concurrent.ExecutorService;

import reactor.core.publisher.TopicProcessor;
import reactor.core.publisher.WorkQueueProcessor;

public class ProcessUtil<E> {
    public static <T> TopicProcessor<T> createTopicProcessor(String name) {
        return TopicProcessor.<T> builder().name(name).build();
    }

    public static <T> TopicProcessor<T> createTopicProcessor(String name, int bufferSize, boolean autoCancel) {
        return TopicProcessor.<T> builder().name(name).bufferSize(bufferSize).autoCancel(autoCancel).build();
    }

    public static <T> TopicProcessor<T> createTopicProcessor(String name, int bufferSize, boolean autoCancel,
        boolean share) {
        return TopicProcessor.<T> builder().name(name).bufferSize(bufferSize).autoCancel(autoCancel).share(share)
            .build();
    }

    public static <T> WorkQueueProcessor<T> createWorkQueueProcessor(String name, int bufferSize, boolean autoCancel) {
        return WorkQueueProcessor.<T> builder().name(name).bufferSize(bufferSize).autoCancel(autoCancel).build();
    }

    public static <T> WorkQueueProcessor<T> createWorkQueueProcessor(ExecutorService executor, int bufferSize,
        boolean autoCancel) {
        return WorkQueueProcessor.<T> builder().executor(executor).bufferSize(bufferSize).autoCancel(autoCancel)
            .build();
    }
}
