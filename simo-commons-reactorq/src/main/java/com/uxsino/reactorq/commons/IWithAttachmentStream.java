package com.uxsino.reactorq.commons;

import java.io.InputStream;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * used for messages with attachment
 * each consumer should close the stream
 */
public interface IWithAttachmentStream {

    /**
     * set the attachment stream
     * @param attachement stream
     */
    @JsonIgnore
    @JSONField(serialize = false)
    void setAttachement(InputStream attachement);

    /**
     * get the attachment stream
     * 
     * @return atachment stream or Null
     */
    @JsonIgnore
    @JSONField(serialize = false)
    InputStream getAttachement();

}
