package com.uxsino.reactorq;

import org.apache.activemq.ActiveMQConnection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.uxsino.reactorq.commons.ReactorQFactory;

@Configuration
public class QFactoryBeanConf {

    @Value("${simo.mq.broker.url:'tcp://127.0.0.1:61616'}")
    private String brokerUrl;

    @Bean(name = "default_reactorq_factory")
    @Scope("singleton")
    public ReactorQFactory getDefaultReactorQFactory() {

        return ReactorQFactory.setDefaultFactory(ActiveMQConnection.DEFAULT_USER, ActiveMQConnection.DEFAULT_PASSWORD,
            brokerUrl);
    }
}
