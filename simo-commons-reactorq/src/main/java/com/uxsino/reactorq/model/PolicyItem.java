package com.uxsino.reactorq.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Strings;

public class PolicyItem implements Serializable {

    // public String id;
    public String indicatorName;

    public SCHEDULE_TYPE scheduleType;

    @JsonProperty("interval")
    public long scheduleInterval = 0; // whether FIXED_DELAY or FIXED_RATE

    public String cronExpr = null; // CRON

    public PolicyItem() {
    }

    public PolicyItem(String indicatorName, SCHEDULE_TYPE scheduleType, long scheduleInterval, String cronExpr) {
        this.indicatorName = indicatorName;
        this.scheduleInterval = scheduleInterval;
        this.cronExpr = cronExpr;
        this.scheduleType = scheduleType;
    }

    @Override
    public int hashCode() {
        String codeExp = indicatorName + "[" + scheduleInterval + "]" + cronExpr + "[" + scheduleType.name() + "]";
        return codeExp.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof PolicyItem) {
            PolicyItem cp = (PolicyItem) obj;
            return Strings.nullToEmpty(this.indicatorName).equals(cp.indicatorName)
                    && (scheduleType == null ? cp.scheduleType == null : (scheduleType.equals(cp.scheduleType)))
                    && scheduleInterval == cp.scheduleInterval && Strings.nullToEmpty(cronExpr).equals(cp.cronExpr);
        } else {
            return false;
        }
    }
}