package com.uxsino.reactorq.model;

/**
 * 
 * TEMP: in memory only. will be removed after collector restarts
 * LOCAL: local ne, will not sync to the central db
 * PUBLIC: sync with central db
 *
 */
public enum SOURCE_TYPE {
                         TEMP,
                         LOCAL,
                         PUBLIC
}
