package com.uxsino.reactorq.model;

import org.apache.commons.lang3.StringUtils;

public enum FieldType {
                       UNKNOWN("unknown_field"),
                       STRING("str_field"),
                       NUMBER("num_field"),
                       PERCENT("per_field"),
                       DATETIME("date_field");
    private String name;

    public String getName() {
        return name;
    }

    private FieldType(String name) {
        this.name = name;
    }

    public static FieldType createByName(String name) {
        if (StringUtils.isBlank(name)) {
            return null;
        }
        for (FieldType type : FieldType.values()) {
            if (name.equals(type.getName())) {
                return type;
            }
        }
        return null;
    }

    public INDICATOR_TYPE caseIndType() {
        try {
            return INDICATOR_TYPE.valueOf(this.toString());
        } catch (Exception e) {
            return INDICATOR_TYPE.STRING;
        }

    }
}
