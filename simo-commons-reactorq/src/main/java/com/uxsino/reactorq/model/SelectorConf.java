package com.uxsino.reactorq.model;

public class SelectorConf {
    private String propExpr;

    private String opName;

    private String propValue;

    public SelectorConf() {
    }

    public String getPropExpr() {
        return propExpr;
    }

    public void setPropExpr(String propExpr) {
        this.propExpr = propExpr;
    }

    public String getOpName() {
        return opName;
    }

    public void setOpName(String opName) {
        this.opName = opName;
    }

    public String getPropValue() {
        return propValue;
    }

    public void setPropValue(String propValue) {
        this.propValue = propValue;
    }

    public SelectorConf(String propExpr, String opName, String propValue) {
        this.propExpr = propExpr;
        this.opName = opName;
        this.propValue = propValue;
    }

}
