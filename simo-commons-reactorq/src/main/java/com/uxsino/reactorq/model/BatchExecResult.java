package com.uxsino.reactorq.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.uxsino.reactorq.commons.IFlowItem;

/**
 * 
 * Result of batch execution on an entity
 *
 */
public class BatchExecResult implements IFlowItem {

    public static final String FAILED_BATCHES = "#batch_failed";

    public int exitCode;

    public BatchItemResult res = null;

    /**
     * the sequence id of execution instance
     */
    @JsonProperty("seq")
    @JSONField(name = "seq")
    public String seqId;

    public STATUS isStatus;

    public boolean isEnded;

    public boolean isInternal;

    @Override
    public boolean isInternal() {
        return this.isInternal;
    }

    /*@Override
    public boolean isEnded() {
        return this.isEnded;
    }*/

    @Override
    public STATUS getStatus() {
        return this.isStatus;
    }
}
