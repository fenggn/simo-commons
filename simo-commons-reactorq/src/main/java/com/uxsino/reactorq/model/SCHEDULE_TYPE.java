package com.uxsino.reactorq.model;

public enum SCHEDULE_TYPE {

                           // {@ link scheduleAtFixedDelay}
                           FIXED_DELAY,
                           // {@ link scheduleAtFixedRate}
                           FIXED_RATE,
                           // {@ link CronTrigger}
                           CRON

}
