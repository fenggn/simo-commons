package com.uxsino.reactorq.model;

import java.util.ArrayList;
import java.util.List;

/**
 * types of indicator
 * internal value types:
 * STRING: String
 * NUMBER: Double
 * PERCENT: Double
 * DATETIME: String (to be changed)
 * COMPOUND: Map<String,Object>
 * LIST: List<Map<String,Object>>
 * AGGREGATE: Map<String,Object>
 */
public enum INDICATOR_TYPE {
                            UNKNOWN("未知",false),
                            NONE("无",false),
                            STRING("字符串",true),
                            NUMBER("数字",true),
                            PERCENT("百分比",true),
                            BOOLEAN("布尔",false),
                            DATETIME("时间",false),
                            COMPOUND("键值对",false),
                            LIST("列表",true),
                            AGGREGATE("聚合",false);
    private String text; // 用于界面显示的中文名称

    private boolean allowCustom; // 是否允许自定义

    private INDICATOR_TYPE(String text, boolean allowCustom) {
        this.text = text;
        this.allowCustom = allowCustom;
    }

    public static INDICATOR_TYPE fromText(String text) {
        for (INDICATOR_TYPE indType : INDICATOR_TYPE.values()) {
            if (indType.text.equals(text)) {
                return indType;
            }
        }
        throw new IllegalArgumentException("无此枚举项:" + text);
    }

    public String getText() {
        return this.text;
    }

    public boolean allowCustom() {
        return allowCustom;
    }

    public List<INDICATOR_TYPE> getAllowCustomType() {
        List<INDICATOR_TYPE> list = new ArrayList<>();
        for (INDICATOR_TYPE indType : INDICATOR_TYPE.values()) {
            if (indType.allowCustom) {
                list.add(indType);
            }
        }
        return list;
    }
}
