package com.uxsino.reactorq.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BatchItemResult {
    /**
     * the entity on which the batch is executed
     */

    @JsonProperty("ne")
    @JSONField(name = "ne")
    public String entityId;

    /**
     * the result String
     */
    @JsonProperty("result")
    @JSONField(name = "result")
    public String value;

    /**
     * start time
     */
    public long start;

    /**
     * end time
     */
    public long end;

    public boolean success;

    public static BatchItemResult of(String entityId) {
        return new BatchItemResult(entityId);
    }

    public BatchItemResult() {
    }

    public BatchItemResult(String entityId) {
        this.entityId = entityId;
    }

    public BatchItemResult success() {
        this.success = true;
        return this;
    }

    public BatchItemResult error() {
        this.success = false;
        return this;
    }

    public BatchItemResult start() {
        this.start = System.currentTimeMillis();
        return this;
    }

    public BatchItemResult end() {
        this.end = System.currentTimeMillis();
        return this;
    }

    public BatchItemResult value(String value) {
        this.value = value;
        return this;
    }
}
